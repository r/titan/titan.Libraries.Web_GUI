// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
// Singleton list of applications inherited from WebAppBase.
// "WebApplications" object is intended to be the only global object.
// It is filled by each WebApplications/<WebAppDir>/Main.js, when the js is loaded.
var WebApplications = WebApplications || [];

if (window["$"] == undefined) {
    var browser = browserCheck();
    alert('This browser is not supported: ' + browser[0] + ' ' + browser[1] + '\nPlease use at least Firefox 38 or Chrome.');
}

$(document).ready(function() {
    "use strict";

    function receiveMessage(event) {
        if (event.source == parent && event.data.username != undefined && event.data.password != undefined) {
            localStorage.setItem('username', event.data.username);
            localStorage.setItem('password', event.data.password);
        }
    }

    window.addEventListener("message", receiveMessage, false);
    window.addEventListener("popstate", function(event) {
        if (event.state != undefined && event.state.index != undefined && WebApplications[event.state.index] != undefined) {
            webappBtnClicked(event.state.index, 'applications_header_appBtn_' + event.state.index);
        }
    });

    var m_this = this;
    var m_fileHandler = new FileHandler();
    var m_webAppModel = new CWebApp_Model(m_fileHandler);

    var m_loadedWebAppIndex;

    this.setAppParam = function(appName, key, value) {
        var index = getWebAppIndexFromName(appName);
        if (index != undefined) {
            var app = WebApplications[index];
            app.params[key] = value;
        }
    };

    this.loadApp = function(appName) {
        var index = getWebAppIndexFromName(appName);
        if (index != undefined) {
            webappBtnClicked(index, 'applications_header_appBtn_' + index, true);
        } else {
            alert('No application found with name ' + appName);
        }
    };

    function getWebAppIndexFromName(appName) {
        for (var i = 0; i < WebApplications.length; ++i) {
            if (WebApplications[i].name == appName) {
                return i;
            }
        }
    }

    function webappBtnClicked(index, buttonId, storeState) {
        var webApp = WebApplications[index].application;

        function webappUnloaded(aApplicationUnloaded) {
            if (aApplicationUnloaded) {
                if (storeState) {
                    window.history.pushState({'index': index}, '');
                }
                webApp.load(m_webAppModel, WebApplications[index].params, m_this); // calls WebApplications load fuction. WebApplications are derived from WebAppBase
                m_loadedWebAppIndex = index;

                $('#applications_header BUTTON').removeClass('SelectedWebApp');
                $("#" + buttonId).addClass('SelectedWebApp');
            }
            else {
                $('#applications_header BUTTON').attr('disabled', false);
            }
        }

        $('#applications_header BUTTON').attr('disabled', true);
        if (m_loadedWebAppIndex != undefined && WebApplications[m_loadedWebAppIndex].application != undefined && WebApplications[m_loadedWebAppIndex].application.unload != undefined) {
            WebApplications[m_loadedWebAppIndex].application.unload(webappUnloaded);
        } else {
            webappUnloaded(true);
        }
    }

    function webAppsDirLoaded(data) {
        // Runs after all WebApplications/<WebAppDir>/Main.js are parsed, so WebApplications is populated by now.
        var applications = data.availableApps;
        var defaultApp = data.defaultApp;

        function webAppsLoaded(ok, msg) {
            var app = getLocationParam('app');
            if (app != undefined) {
                var index = parseInt(app);
                if (isNaN(index)) {
                    m_this.loadApp(app);
                } else {
                    webappBtnClicked(index, 'applications_header_appBtn_' + index, true);
                }
            } else if (defaultApp != undefined) {
                m_this.loadApp(defaultApp);
            }
        }

        function ApplicationLoadTask(index, application) {
            var v_callback;
            var v_index = index;
            var v_directory = application.directory;
            var v_params = application.params;
            var v_name = application.name;
            var v_icon = application.icon;
            var v_color = application.color;
            var v_hidden = application.hidden;

            function mainJsFileImported(ok) {
                if (ok) {
                    var webApp = WebApplications[WebApplications.length - 1];
                    if (v_name == undefined) {
                        v_name = webApp.application.info().defaultName;
                    }
                    if (v_icon == undefined) {
                        v_icon = webApp.application.info().defaultIcon;
                    }
                    webApp.name = v_name;
                    if (v_params != undefined) {
                        webApp.params = v_params;
                    } else {
                        webApp.params = {};
                    }

                    var buttonId = 'applications_header_appBtn_' + v_index;
                    var buttonHtml = ''
                    if (v_color != undefined) {
                        buttonHtml = '<button index="' + v_index + '" id="' + buttonId + '" style="background-color:' + v_color + '; border-color:' + v_color + ';"><span><img src="' + v_icon + '"/>' + v_name + '</span></button>';
                    } else {
                        buttonHtml = '<button index="' + v_index + '" id="' + buttonId + '"><span><img src="' + v_icon + '"/>' + v_name + '</span></button>';
                    }
                    if (v_hidden && !(getLocationParam('developerMode') == "true")) {
                        //$('#hiddenWebapps').append(buttonHtml);
                    } else {
                        $('#webapps').append(buttonHtml);
                    }

                    $("#" + buttonId).on('click', function() {
                        webappBtnClicked(parseInt($(this).attr('index')), $(this).attr('id'), true);
                    }).on('mouseup', function(e) {
                        if (e.which == 2) {
                            var url = 'Main.html?app=' + $(this).attr('index');
                            var win = window.open(url, '_blank');
                        }
                    });
                } else {
                    alert('Failed to load application ' + v_name);
                }

                v_callback(true);
            }

            this.taskOperation = function(callback) {
                v_callback = callback;
                m_fileHandler.importJsFile(v_directory + '/Main.js', mainJsFileImported);
            };
        }

        var importTasks = [];
        for (var i = 0; i < applications.length; ++i) {
            importTasks[i] = new ApplicationLoadTask(i, applications[i]);
        }
        var taskList = new SyncTaskList(importTasks, webAppsLoaded);
        taskList.taskOperation();

        // $('#webappToggleButton').click(function () {
        //     $('#hiddenWebapps').toggleClass('hidden');
        // });
    }

    function start() {
        if (getLocationParam('hideRibbon') == 'true') {
            $('#applications_header_container').addClass('hidden');
        }

        // load framework javascript and css files
        new MultipleDirectoryListTask(
            [
                "WebApplicationFramework/Views",
                "WebApplicationFramework/ViewModels"
            ],
            m_fileHandler
        ).taskOperation(function(ok, resources) {
            m_fileHandler.loadCssFiles(resources.cssfiles, "TSGuiFrameworkMain");
            new JsImportTaskList(resources.jsfiles, m_fileHandler, function(ok) {
                m_webAppModel.loadMainConfig(webAppsDirLoaded);
            }).taskOperation();
        });
    }

    var browser = browserCheck();
    var not_first_run = localStorage.getItem('_not_first_run_');
    if ((not_first_run == 'true') || ((browser[0].toLowerCase() == "firefox" && parseInt(browser[1].split(".")[0]) >= 38) || browser[0].toLowerCase() == "chrome")) {
        start();
    } else {
        localStorage.setItem('_not_first_run_', 'true');
        var html = '' +
        '<div id="UnsupportedBrowser">' +
            '<p>This browser is not supported: ' + browser[0] + ' ' + browser[1] + '</p>' +
            '<p>Please use at least Firefox 38 or Chrome</p>' +
            '<button>Continue anyway...</button>' +
        '</div>'
        $("#TSGuiFrameworkMain").append(html);
        $("#UnsupportedBrowser > button").on("click", function() {
            $("#UnsupportedBrowser").remove();
            start();
        });
    }
});

function WebAppBase() {
    "use strict";
    var m_loaded = false;
    var m_imported = false;

    this.unload = function(destroy) {
        if (m_loaded) {
            destroy();
            m_loaded = false;
        }
    };

    this.load = function(jsfiles, alwasyImportableJsFiles, start, fileHandler) {
        if (m_imported) {
            jsfiles = alwasyImportableJsFiles
        } else {
            jsfiles = jsfiles.concat(alwasyImportableJsFiles)
        }

        var taskList = new JsImportTaskList(jsfiles, fileHandler, function(ok, msg) {
            if (ok) {
                start(function() {
                    $('#applications_header BUTTON').attr('disabled', false);
                });
                m_imported = true;
                m_loaded = true;
            } else {
                alert('WebAppBase::load says task was not OK:' + JSON.stringify(msg, null, 4));
            }
        });
        taskList.taskOperation();
    };
}
