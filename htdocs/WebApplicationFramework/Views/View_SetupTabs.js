// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_SetupTabs(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var m_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_SetupTabs)[0];
    var m_parentId = p_parentId;
    
    /** public functions */

    this.applicationCreated = function() {};

    this.refresh = function(p_fullRefresh) {
        if (p_fullRefresh) {
            $("#" + m_parentId).empty();
            createTabs();
        }
    };
    
    /** private functions */
    
    function createTabs() {
        function createTabRow(tabsVM, nesting, index, parentID) {
            var setupList = tabsVM.getList().values;
            var tabList = [];
            var tabidList = [];
            for (var i = 0; i < setupList[0].length; ++i) {
                tabList[i] = setupList[0][i][0];
                tabidList[i] = parentID + nesting + "_" + i;
            }
            var setupTabs = new CView_Tabs([tabsVM], "setupTabID_" + parentID, parentID, {idsCreating : tabidList, namesCreating : tabList, css: {height:"100px"}});
            setupTabs.applicationCreated();
            setupTabs.refresh(true);
            var subVMs = tabsVM.getSubViewModels();
            for (var i = 0; i < subVMs.length; ++i)
                if (subVMs[i])
                    createTabRow(subVMs[i], nesting + 1, i, tabidList[i]);
        }

        var tabsVM = m_viewmodel.getRecursiveSetups();
        createTabRow(tabsVM, 0, 0, m_parentId);
    }
}

CView_SetupTabs.getHelp = function() {
    return "A view that displays the setup hierarchy and can be used to switch setups.";
};

CView_SetupTabs.expectsInterface = function() {
    return [{
        "mandatory": ["getRecursiveSetups"]
    }];
}

CView_SetupTabs.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_SetupTabs",
        "type": "object",
        "properties": {},
        "additionalProperties": false
    };
    //$.extend(true, schema, ViewUtils.commonViewSchema);
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_SetupTabs.js