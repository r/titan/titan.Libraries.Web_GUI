// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_ElementTableForLargeData(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var base = new CView_ElementTable(p_viewmodels, p_mainId, p_parentId, p_data);
    var v_scrollId = base.base.mainId + "_Scroll";
    var scrollBar = new CView_Scroll(p_viewmodels, v_scrollId, base.base.tableWrapperId, p_data);
    
    var v_viewmodels = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_ElementTableForLargeData);
    var v_streamingViewmodel = v_viewmodels[0];
    var v_positionViewmodel = v_viewmodels[1];
    var v_conditionViewmodel = v_viewmodels[2];
    var v_scrollInitialised = false;
    
    /** public functions */

    this.applicationCreated = function() {
        base.applicationCreated();
        $('#' + base.base.tableWrapperId).css("position", "relative");
    };
    
    this.refresh = function(p_fullRefresh) {
        if (ViewUtils.checkVisibility(v_conditionViewmodel, base.base.mainId)) {
            var isStreaming = false;
            if (v_streamingViewmodel.isStreaming != undefined) {
                isStreaming = v_streamingViewmodel.isStreaming();
            }
            if (v_scrollInitialised && !isStreaming) {
                $("#" + v_scrollId).remove();
                v_scrollInitialised = false;
                $("#" + base.base.tableWrapperId).scrollTop(0).css("overflow-y", "auto");
            } else if (!v_scrollInitialised && isStreaming) {
                scrollBar.applicationCreated();
                v_scrollInitialised = true;
                $("#" + base.base.tableWrapperId).scrollTop(0).css("overflow-y", "hidden");
            }
            
            base.refresh(p_fullRefresh);
            if (isStreaming) {
                scrollBar.refresh(p_fullRefresh);
            }
            
            var oddRows = $("#" + base.base.mainId + "." + base.base.mainClass + " tr:nth-child(odd)");
            var evenRows = $("#" + base.base.mainId + "." + base.base.mainClass + " tr:nth-child(even)");
            
            if (isStreaming && v_positionViewmodel != undefined) {
                if (v_positionViewmodel.getPosition() % 2 == 0) {
                    oddRows.removeClass(base.base.mainClass + "_Even");
                    oddRows.addClass(base.base.mainClass + "_Odd");
                    evenRows.removeClass(base.base.mainClass + "_Odd");
                    evenRows.addClass(base.base.mainClass + "_Even");
                } else {
                    evenRows.removeClass(base.base.mainClass + "_Even");
                    evenRows.addClass(base.base.mainClass + "_Odd");
                    oddRows.removeClass(base.base.mainClass + "_Odd");
                    oddRows.addClass(base.base.mainClass + "_Even");
                }
            } else {
                oddRows.removeClass(base.base.mainClass + "_Even");
                oddRows.removeClass(base.base.mainClass + "_Odd");
                evenRows.removeClass(base.base.mainClass + "_Odd");
                evenRows.removeClass(base.base.mainClass + "_Even");
            }
        }
    };
}

CView_ElementTableForLargeData.getHelp = function() {
    return "A table view whose columns can contain single element subviews and can use viewmodels suited for handling larger datasets.";
};

CView_ElementTableForLargeData.expectsInterface = function() {
    var expectedInterface = CView_ElementTable.expectsInterface();
    var scrollExpectedInterface = CView_Scroll.expectsInterface();
    for (var i = 0; i < scrollExpectedInterface.length; ++i) {
        expectedInterface.push(scrollExpectedInterface[i]);
    }
    expectedInterface.unshift({
        "optional": ["getState"]
    });
    expectedInterface.unshift({
        "optional": ["getPosition"]
    });
    expectedInterface.unshift({
        "optional": ["isStreaming"]
    });
    
    return expectedInterface;
};

CView_ElementTableForLargeData.getCustomDataSchema = function() {
    var tableSchema = CView_ElementTable.getCustomDataSchema();
    var scrollSchema = CView_Scroll.getCustomDataSchema();
    
    for (var id in scrollSchema.properties) {
        tableSchema.properties[id] = scrollSchema.properties[id];
    }
    
    tableSchema.title = "Custom data for CView_ElementTableForLargeData";
    return tableSchema;
};

//# sourceURL=WebApplicationFramework\Views\View_ElementTableForLargeData.js