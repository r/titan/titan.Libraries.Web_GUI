// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_ComboBox(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";

    /** constructor */

    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_ComboBox)[0];
    var v_conditionViewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_ComboBox)[1];
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;
    var v_selected = false;
    var v_current = '';
    var v_target;

    var v_this = this;

    var LIST = 1,
    TARGET = 0;

    var v_label;
    var v_showLabel = v_customData.showLabel ? v_customData.showLabel : false;
    var v_defaultValue = v_customData.defaultValue;

    /** public functions */

    this.applicationCreated = function () {
        if (v_viewmodel != undefined) {
            $("#" + v_parentId).append(getHtml());
            if (v_customData.isElementLabelPresent) {
                v_label = ViewUtils.addLabel(v_mainId, v_customData.elementText);
            }
            $("#" + v_mainId).addClass("BasicComboBox");

            $("#" + v_mainId + "_cbValue").on({
                'focus' : function () {
                    v_selected = true;
                },
                'blur' : function (event) {
                    v_selected = false;
                },
                'change' : function (event) {
                    var obj = $(this);
                    var val = obj.val();
                    v_viewmodel.setValue(TARGET, obj.val());
                    obj.blur();
                }
            });
            
            ViewUtils.addTooltip(v_mainId, v_customData);
        }
    };

    this.refresh = function (p_fullRefresh) {
        if (v_viewmodel != undefined && ViewUtils.checkVisibility(v_conditionViewmodel, v_mainId)) {
            var dataObject = v_viewmodel.getListWithElementInfo();
            if (dataObject == undefined || dataObject.values[0] == undefined) {
                return;
            }
            
            if (p_fullRefresh && v_customData.isElementLabelPresent && v_customData.elementText == undefined) {
                v_label.text(dataObject.values[0].element);
            }
            
            if (!v_selected) {
                var element = "";
                var jQcb = $("#" + v_mainId + "_cbValue");
                
                jQcb.empty();

                v_current = dataObject.values[TARGET].val;
                element = dataObject.values[TARGET].element;
                
                if (v_current == undefined) {
                    $("#" + v_mainId + " > *").css("display", "none");
                    $("#" + v_mainId).addClass("NoData");
                } else {
                    $("#" + v_mainId + " > *").css("display", "");
                    $("#" + v_mainId).removeClass("NoData");
                }
                
                var selectedFound = false;
                if (v_customData.possibleValues == undefined && dataObject.values[LIST]) {
                    if (dataObject.values[LIST].val && dataObject.values[LIST].val.constructor == Array) {
                        for (var idx in dataObject.values[LIST].val) {
                            if (dataObject.values[LIST].val.hasOwnProperty(idx)) {
                                var vv = dataObject.values[LIST].val[idx];
                                jQcb.append('<option value="' + vv + '"' + (v_current === vv ? ' selected="selected"' : '') + '>' + vv + '</option>');
                                if (v_current === vv) {
                                    selectedFound = true;
                                }
                            }
                        }
                    }
                } else if (v_customData.possibleValues != undefined) {
                    for (var i = 0; i < v_customData.possibleValues.length; ++i) {
                        var vv = v_customData.possibleValues[i];
                        jQcb.append('<option value="' + vv + '"' + (v_current === vv ? ' selected="selected"' : '') + '>' + vv + '</option>');
                        if (v_current === vv) {
                            selectedFound = true;
                        }
                    }
                }
                
                if (v_defaultValue != undefined) {
                    jQcb.prepend('<option value="' + v_defaultValue + '"' + (selectedFound ? '' : ' selected="selected"') + '>' + v_defaultValue + '</option>');
                }
                    
                if (v_showLabel) {
                    document.getElementById(v_mainId + "_cbHeader").value = ((v_customData.text) ? v_customData.text : element);
                }
                
                if (v_customData.disabled !== true && dataObject.values[TARGET] != undefined && dataObject.values[TARGET].isWritable) {
                    $("#" + v_mainId + " > select").prop('disabled', false);
                } else {
                    $("#" + v_mainId + " > select").prop('disabled', true);
                }
            }
        }
    };

    /** private functions */

    function getHtml() {
        return '<div id="' + v_mainId + '"><div></div>' + (v_showLabel ? '<label id="' + v_mainId + '_cbHeader"></label>' : '') +
        '<select id="' + v_mainId + '_cbValue"' + ((v_customData.writable !== "false") ? '' : ' disabled') + '></select></div>';
    }
}

CView_ComboBox.getHelp = function() {
    return "A combo box view that needs an element to set and either a second data connection in the viewmodel or a static list of choices.";
}

CView_ComboBox.expectsInterface = function() {
    return [
        {
            "mandatory": ["setValue", "getListWithElementInfo"]
        },
        {
            "optional": ["getState"]
        }
    ];
};

CView_ComboBox.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_ComboBox",
        "type": "object",
        "properties": {
            "showLabel": {
                "description": "Show label next to the combo box (default false).",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            },
            "text": {
                "description": "The text of the label next to the combo box.",
                "type": "string"
            },
            "possibleValues": {
                "description": "A list of possible predefined values. If the list is undefined, the list from the viewmodel will be used.",
                "type": "array",
                "format": "table",
                "items": {
                    "type": "string",
                    "title": "value"
                }
            },
            "defaultValue": {
                "description": "A default value that appears as the first element and is selected when the response does not match any element from the list.",
                "type": "string"
            },
            "writable": {
                "type": "boolean",
                "format": "checkbox",
                "description": "Whether we allow the data to be changed (default true).",
                "default": false
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema, ViewUtils.commonElementSchema);
    return schema;
}

//# sourceURL=WebApplicationFramework\Views\View_ComboBox.js
