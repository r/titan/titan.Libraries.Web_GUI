// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_ElementTable(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    if (p_data.class == undefined) {
        p_data.class = "ElementTable";
    }
    
    var base = new CView_Table_Vertical(p_viewmodels, p_mainId, p_parentId, p_data);
    this.base = base;
    
    var v_flexViewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_ElementTable)[0];
    
    // temp variables for resizable behaviour
    var lastObj;
    var prevObjWidth;
    var prevWidth;
    var prevPosition;
    
    var v_this = this;

    /** public functions */

    this.applicationCreated = base.applicationCreated;
    this.refresh = base.refresh;

    /** overridden functions */

    base.setSelection = function(selection) {
        $("#" + base.tableId + " tr").removeClass(base.mainClass + "_Selected");
        if (selection != undefined) {
            for (var i = 0; i < selection.length; ++i) {
                $("#" + base.tableId + " *:nth-child(" + (selection[i] + 2) + ")").addClass(base.mainClass + "_Selected");
            }
        }
    };

    base.addDataToTable = function(p_numberOfRows, p_numberOfColumns) {
        var numberOfRows = $("#" + base.tableId + " tr").length;
        if (p_numberOfRows > 0) {
            // create the html for the row and add it to the DOM
            var html = '';
            for (var i = 0; i < p_numberOfRows; ++i) {
                html += '<tr>';
                for (var j = 0; j < p_numberOfColumns; ++j) {
                    html += '<td id="' + base.tableId + "_SubView_" + (numberOfRows + i - 1) + "_" + j + '"></td>';
                }
                html += '</tr>'
            }
            $("#" + base.tableId).append(html);
            
            // create the subviews where applicable
            for (var i = 0; i < p_numberOfRows; ++i) {
                var subViews = [];
                for (var j = 0; j < p_numberOfColumns; ++j) {
                    if (base.subViewDescriptors != undefined && base.subViewDescriptors[j] != undefined && base.subViewDescriptors[j].subView != undefined && window[base.subViewDescriptors[j].subView] != undefined) {
                        var parentId = base.tableId + "_SubView_" + (numberOfRows + i - 1) + "_" + j;
                        var viewId = base.tableId + "_SubView_" + (numberOfRows + i - 1) + "_" + j + "_View";
                        var viewmodel = new base.ViewmodelProxy(numberOfRows + i - 1, j);
                        
                        var subView = new window[base.subViewDescriptors[j].subView]([viewmodel], viewId, parentId, base.subViewDescriptors[j]);
                        subView.applicationCreated();
                        ViewUtils.applyCss(base.subViewDescriptors[j], viewId);
                        ViewUtils.processCss(base.subViewDescriptors[j], parentId);
                        subViews.push(subView);
                    } else {
                        // we must ensure that there are as many subviews as the number of cells in the table
                        subViews.push(null);
                    }
                }
                base.subViews.push(subViews);
            }
        } else {
            // we remove the correct number of rows from the DOM and the subviews
            $("#" + base.tableId + " tr").slice(numberOfRows + p_numberOfRows).remove();
            for (var i = 0; i > p_numberOfRows; --i) {
                base.subViews.pop();
            }
        }
    };

    base.createHeader = function(p_headerList) {
        var html = '<tr>'
        for (var i = 0; i < p_headerList.length; ++i) {
            html += '<th>' + p_headerList[i].heading + '</th>';
        }
        html += '</tr>'
        $("#" + base.tableId).append(html);
        var firstRow = $("#" + base.tableId + " tr");
        firstRow.addClass(base.mainClass + "_Header");
        calculateWidthsFromFlex();
        
        if (base.customData.displayHeader === false) {
            firstRow.css("display", "none");
        } else {
            if (base.customData.resizable === true) {
                // Resizing only works if we give the widths in pixels
                recalculateTableWidthPixels();
                
                var resizables = $("#" + base.tableId + " tr th");
                var lastObj = resizables.last();
                for (var i = 0; i < resizables.length - 1; ++i) {
                    $(resizables[i]).resizable({
                        handles: "e",
                        start: function(event, ui) {
                            prevObjWidth = lastObj.width();
                            prevWidth = ui.size.width;
                            prevPosition = ui.originalElement.offset().left;
                        },
                        resize: function(event, ui) {
                            // if our offset changes or the last column's width does not change any more
                            if (ui.originalElement.offset().left != prevPosition || lastObj.width() == prevObjWidth) {
                                ui.size.width = prevWidth; //do not resize
                            } else {
                                prevWidth = ui.size.width;
                                prevObjWidth = lastObj.width();
                            }
                        }
                    });
                }
            }
        }
        
        if (base.customData.showToolTip !== false) {
            $("#" + base.tableId).on("mouseenter", "td", function() {
                var header = $("#" + base.tableId + " tr th");
                $(this).prop("title", $(header[$(this).index()]).text());
            });
        }
    };
    
    base.createSeparators = function(separators) {
        var rows = $("#" + base.tableId + " tr");
        var numberOfColumns = $("#" + base.tableId + " tr th").length;
        for (var i = 0; i < separators.length; ++i) {
            if (separators[i].index + 1 < rows.length) {
                var rowAfterSeparator = rows[separators[i].index + 1];
                $(rowAfterSeparator).before('<div class="' + base.mainClass + '_Separator">' + separators[i].text + '</div>');
            }
        }
    }

    base.setupSelection = function() {
        $("#" + base.tableId).on("click", "tr", function() {
            var index = $(this).index() - 1;
            if (index >= 0) {
                base.dataViewmodel.select(base.getUnalteredIndex(index));
            }
        });
    };
    
    base.displayFilter = function() {
        return base.sortViewmodel != undefined && base.customData.filter !== false;
    };
    
    base.displaySort = function() {
        return base.sortViewmodel != undefined && base.customData.sort !== false;
    };
    
    base.getIndexOfTableHeaderBasedOnOrientation = function(obj) {
        return obj.index();
    };

    /** private functions */

    function calculateWidthsFromFlex() {
        if (v_flexViewmodel != undefined && base.subViewDescriptors != undefined) {
            var cols = $("#" + base.mainId + " tr th");
            var flexes = [];
            var total = 0;
            for (var i = 0; i < base.subViewDescriptors.length; ++i) {
                if (base.subViewDescriptors[i].flex != undefined) {
                    flexes.push(base.subViewDescriptors[i].flex);
                    total += base.subViewDescriptors[i].flex;
                } else {
                    flexes.push(1);
                    total += 1;
                }
            }
            v_flexViewmodel.getChildPercentagesFromFlex(flexes, total);
            var table = $("#" + base.tableId);
            for (var i = cols.length - 1; i >= 0; --i) {
                table.prepend('<col width=' + flexes[i] + '>');
            }
        }
    }

    function recalculateTableWidthPixels() {
        var cols = $("#" + base.tableId + " tr th");
        for (var i = 0; i < cols.length - 1; ++i) {
            $(cols[i]).width(cols[i].getBoundingClientRect().width);
        }
        $(cols[cols.length - 1]).removeAttr('style');
    }
}

CView_ElementTable.getHelp = function() {
    return "A table view whose columns can contain single element subviews.";
}

CView_ElementTable.expectsInterface = function() {
    var expectedInterface = CView_Table_Vertical.expectsInterface();
    expectedInterface.unshift({
        "optional": ["getChildPercentagesFromFlex"]
    });
    return expectedInterface;
}

CView_ElementTable.getCustomDataSchema = function() {
    var schema = CView_Table_Vertical.getCustomDataSchema();
    schema.title = "Custom data for CView_ElementTable";
    schema.properties.class.default = "ElementTable";
    schema.properties.sort.default = false;
    schema.properties.resizable = {
        "description": "Whether we want the table to be resizable (default false). It will only work when no subviews are specified.",
        "type": "boolean",
        "format": "checkbox",
        "default": true
    };
    schema.properties.showToolTip = {
        "description": "Whether the column name is shown when hovering over a cell.",
        "type": "boolean",
        "format": "checkbox",
        "default": false
    };
    return schema;
}

//# sourceURL=WebApplicationFramework\Views\View_ElementTable.js