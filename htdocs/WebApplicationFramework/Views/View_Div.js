// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_Div(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;
    if (v_customData.text == undefined) {
        v_customData.text = "";
    }
    
    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_Div)[0];
    var v_conditionViewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_Div)[1];
    
    var v_currentHtml;
    
    var v_this = this;
    
    /** public functions */

    this.applicationCreated = function() {
        $("#" + v_parentId).append(getHtml());
        if (v_customData.isElementLabelPresent) {
            ViewUtils.addLabel(v_mainId, v_customData.elementText);
        }
        if (v_customData.class != undefined) {
            $("#" + v_mainId).addClass(v_customData.class);
        }
        
        var v_currentHtml = v_customData.text;
    };

    this.refresh = function() {
        if (ViewUtils.checkVisibility(v_conditionViewmodel, v_mainId)) {
            if (v_viewmodel != undefined) {
                var html = v_viewmodel.getHtml();
                if (html != v_currentHtml) {
                    $("#" + v_mainId).html(html);
                    v_currentHtml = html;
                }
            }
        }
    };

    /** private functions */
    
    function getHtml() {
        return '<div id="' + v_mainId + '">' + v_customData.text + '</div>';
    }
}

CView_Div.getHelp = function() {
    return "A single div element. Any kind of html can be inserted by using the text member of the custom data.";
}

CView_Div.expectsInterface = function() {
    return [
        {
            "optional": ["getHtml"]
        },
        {
            "optional": ["getState"]
        }
    ];
};

CView_Div.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_Div",
        "type": "object",
        "properties": {
            "class": {
                "type": "string",
                "description": "the css class of the div"
            },
            "text": {
                "type": "string",
                "description": "the text of the div"
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema, ViewUtils.commonElementLabelSchema);
    return schema;
};
//# sourceURL=WebApplicationFramework\Views\View_Div.js