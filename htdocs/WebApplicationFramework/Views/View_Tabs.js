// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_Tabs(aViewModels, aID, aParentID, aCustomData)
{
    "use strict";
    
	/** private members and constructor */
    var mViewModel = ViewUtils.getViewmodelsFromExpectedInterface(aViewModels, CView_Tabs)[0];
    var mID = aID;
    var mParentID = aParentID;
    var mCustomData = aCustomData;
    var mThis = this;
    var mTabs;
    
    var mActivateCallback = true;

	/** public functions */
    this.applicationCreated = function() 
    {
        createHTML();
    };

    this.refresh = function(aFullRefresh) {
        if (mViewModel && mViewModel.getList)
        {
            var selections = mViewModel.getList().selections[0];
            mActivateCallback = false;
            mTabs.tabs('option', 'active', selections[0]);
            mActivateCallback = true;
        }
    };
    
    /** private functions */
    function createHTML()
    {
        var lHtml = '<div id="' + mID + '" class="TabsView"><ul>';

        for (var i = 0; i < mCustomData.idsCreating.length; ++i)
        {
            var lName = undefined; 
            if (mCustomData.namesCreating)
                lName = mCustomData.namesCreating[i];
            if (!lName)
                lName = mCustomData.idsCreating[i];
            lHtml += '<li><a href="#' + mCustomData.idsCreating[i] + '">' + lName + '</a></li>';
        }
        
        lHtml += '</ul>';
        
        for (var i = 0; i < mCustomData.idsCreating.length; ++i)
            lHtml += '<div id="' + mCustomData.idsCreating[i] + '"></div>';
        
        lHtml += '</div>';

        $("#" + mParentID).append(lHtml);
        mTabs = $('DIV#' + mID).tabs({
            activate: function(ev, ui) {
                if (mActivateCallback && mViewModel && mViewModel.select)
                    mViewModel.select(ui.newTab.index());
            }
        });
    }
}

CView_Tabs.getHelp = function() {
    return "A tab view that creates static tabs.\n" +
        "If a viewmodel is connected, its selection will be used to show the active tab and its select method will be used when switching tabs.\n" +
        "As many subviews can be connected to this view as the number of tabs that are created.";
};

CView_Tabs.expectsInterface = function() {
    return [{
        "optional": ["getList", "select"]
    }];
}

CView_Tabs.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_Tabs",
        "type": "object",
        "properties": {
            "namesCreating": {
                "description": "The name of the tabs that are created.",
                "type": "array",
                "format": "table",
                "items": {
                    "type": "string",
                    "title": "tab"
                },
                "minItems": 1
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema);
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_Tabs.js