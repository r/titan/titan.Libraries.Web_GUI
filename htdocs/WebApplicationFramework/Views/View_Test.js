// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_Test(aViewModels, aID, aParentID)
{
    "use strict";
    
	/** private members and constructor */
    var mViewModel = aViewModels[0];
    var mID = aID;
    var mDataTable = null;
    var mParentID = aParentID;
    var mThis = this;
    var mEditList = [];
    var lIDCounter;

	/** public functions */
    this.applicationCreated = function() {
       // if (mViewModel.getOptions) {
            var lOptions;// = mViewModel.getOptions();
            createHtml(lOptions);
            setupView(mID, lOptions);
        // }
        /*function htmlLoaded(aSucc, aData)
        {
            console.log("aData", aData);
        }
        mViewModel.loadFile("WebApplications/CustomizableApp/Views/View_Test.html", htmlLoaded);*/
    };

    this.refresh = function(aFullRefresh)
    {
        if (mViewModel.getListWithElementInfo) {
            var lValueList = mViewModel.getListWithElementInfo(); //console.log(lValueList);
            lIDCounter = 0;
            //console.log (lValueList);

            var lIsEqualStructure = true; // TODO NOWX - this is not working currently well
            
            if (aFullRefresh)
            {
                $('#' + mID).html('');
                $('#' + mID).addClass("alma");
                mEditList = []; 
            }
            for (var j = 0; j < lValueList.values.length; ++j)
            {
                var lObj = lValueList.values[j];
                if (lObj)
                {
                    if (aFullRefresh)
                    {
                        var i = 0;
                        var lContainer = document.createElement("DIV");
                        lContainer.className = "testContainer";
                        createColumn(lContainer, lObj, mID + "_" + j + "_" + (i++), true, j);
                        if (lObj.children) 
                        {
                            for (var k = 0; k < lObj.children.length; ++k)
                            {
                                createColumn(lContainer, lObj.children[k], mID + "_" + j + "_" + (i++), false, j);
                            }
                        }
                        $('#' + mID).append(lContainer);
                        $('#' + mID).append('<div class=line></div>');
                    }
                    else
                    { 
                        refreshColumn(lObj);
                        if (lObj.children) 
                        {
                            for (var k = 0; k < lObj.children.length; ++k)
                            {
                                refreshColumn(lObj.children[k]);
                            }
                        }
                    }
                }
            }
        }
    };

    /** private functions */
    function action(e)
    {
        var lCklickedIndexInList = e.target.indexInList;
        var lCklickedDataPathIndex = e.target.dataPathIndex;
        if (lCklickedDataPathIndex != null)
        {
            var newVal = prompt("Please enter new value", e.target.innerHTML);
            if (newVal != null)
            {
                mViewModel.setValue(lCklickedDataPathIndex, newVal, lCklickedIndexInList);
            }            
        }
    }
    function createHtml(aHeaderInfo)
    {
        var lDiv = document.createElement("DIV");
        lDiv.innerHTML = '<div id='+mID+'></div>';
        $("#" + mParentID).append(lDiv);
        $('#' + mID).on('click', action);
    }
    
    function createColumn(aParent, aObj, aId, aReadOnly, aDataPathIndex)
    {
        var lColumn = document.createElement("SPAN");
        lColumn.className = "testColumn";
        var lHeaderID = aId + '_header';
        createCell(lColumn, aObj.element, true, lHeaderID, true);
        if (Array.isArray(aObj.val)) {
            for (var i = 0; i < aObj.val.length; ++i)
            {
                var lRowID = aId + '_row' + (i+1);
                createCell(lColumn, aObj.val[i], aReadOnly, lRowID, true, aDataPathIndex, i);
            }
        } else {
            var lRowID = aId + '_row_1';
            createCell(lColumn, aObj.val, aReadOnly, lRowID, true, aDataPathIndex);
        }
        aParent.appendChild(lColumn);
    }
    
    function refreshColumn(aObj)
    {
        refreshCell(aObj.element);
        if (Array.isArray(aObj.val)) {
            for (var i = 0; i < aObj.val.length; ++i)
            {
                refreshCell(aObj.val[i]);
            }
        } else {
            refreshCell(aObj.val);
        }
    }
    
    function createCell(aParent, aArray, aReadOnly, aId, aBreak, aDataPathIndex, aIndexInList)
    {
        var lCell = document.createElement("SPAN");
        if (aDataPathIndex != undefined) { lCell.dataPathIndex = aDataPathIndex; }
        if (Array.isArray(aArray)) {
            for (var l = 0; l < aArray.length; ++l) 
            {
                createCell(lCell, aArray[l], false, aId + "_" + l, false);
            }
            aParent.appendChild(lCell);
            if (aBreak) { aParent.appendChild(document.createElement("br")); }
        } else {
            lCell.className = "testCell";
            lCell.innerHTML = aArray;
            lCell.readOnly = aReadOnly;
            lCell.id = aId;
            mEditList.push(lCell.id);
            if (aIndexInList != undefined) { lCell.indexInList = aIndexInList; }
            aParent.appendChild(lCell);
            if (aBreak) { aParent.appendChild(document.createElement("br")); } 
        }
    }
    
    function refreshCell(aArray)
    {
        if (Array.isArray(aArray)) {
            for (var l = 0; l < aArray.length; ++l) 
            {
                refreshCell(aArray[l]);
            }
        } else {
            $('#' + mEditList[lIDCounter++]).html(aArray); 
        }
    }
    
    function setupView(aTableID, aOptions)
    {
    }
    
    function setupCallbacks(aTableID)
    {
    }
}

CView_Test.getHelp = function() {
    return "A basic view that can show data using the cnnected viewmodel's getListWithElementInfo.";
}

//# sourceURL=WebApplicationFramework\Views\View_Test.js