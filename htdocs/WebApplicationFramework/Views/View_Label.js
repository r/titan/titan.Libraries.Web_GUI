// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_Label(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var v_viewmodels = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_Label);
    var v_viewmodel = v_viewmodels[0];
    var v_conditionViewmodel = v_viewmodels[1];
    var v_writableViewmodel = v_viewmodels[2];
    
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;
    
    var v_label;
    var v_selected = false;
    var v_currentValue = "";
    
    var v_this = this;
    
    /** public functions */

    this.applicationCreated = function() {
        $("#" + v_parentId).append(getHtml());
        if (v_customData.isElementLabelPresent) {
            v_label = ViewUtils.addLabel(v_mainId, v_customData.elementText);
        }
        if (v_customData.class != undefined) {
            $("#" + v_mainId).addClass(v_customData.class);
        } else {
            $("#" + v_mainId).addClass("BasicLabel");
        }
        
        var input = $("#" + v_mainId + " input");
        
        if (v_customData.disabled !== true && v_customData.text == undefined && v_viewmodel != undefined) {
            input.on({
                focus: function() {
                    v_selected = true;
                },
                blur: function() {
                    var newValue = $(this).val();
                    if (newValue != v_currentValue) {
                        v_viewmodel.setValue(0, newValue);
                    }
                    v_selected = false;
                },
                keydown: function(event) {
                    if (event.keyCode == 13) {
                        $(this).blur();
                    }
                }
            });
        }
        
        if (v_viewmodel == undefined || v_customData.text != undefined) {
            $("#" + v_mainId + " > input").prop('disabled', true);
        }
        
        ViewUtils.addTooltip(v_mainId, v_customData);
    };

    this.refresh = function(p_fullRefresh) {
        if (ViewUtils.checkVisibility(v_conditionViewmodel, v_mainId) && v_viewmodel != undefined) {
            var dataObject = v_viewmodel.getListWithElementInfo();
            if (v_customData.text != undefined || dataObject == undefined || dataObject.values[0] == undefined) {
                return;
            }
            
            var input = $("#" + v_mainId + " input");
            
            if (p_fullRefresh && v_customData.isElementLabelPresent && v_customData.elementText == undefined) {
                v_label.text(dataObject.values[0].element);
            }
            
            if (!v_selected && v_customData.text == undefined) {
                var text = "";
                if(dataObject.values[0].val != undefined) {
                    $("#" + v_mainId + " > *").css("display", "");
                    $("#" + v_mainId).removeClass("NoData");
                    text = dataObject.values[0].val;
                } else {
                    $("#" + v_mainId + " > *").css("display", "none");
                    $("#" + v_mainId).addClass("NoData");
                }
                // we need both...
                input.val(text);
                input[0].setAttribute('value', text);
                v_currentValue = text;
            }
            
            if (v_customData.disabled !== true && dataObject.values[0] != undefined && dataObject.values[0].isWritable && (v_writableViewmodel == undefined || v_writableViewmodel.isWritable())) {
                $("#" + v_mainId + " > input").prop('disabled', false);
            } else {
                $("#" + v_mainId + " > input").prop('disabled', true);
            }
        }
    };

    /** private functions */
    
    function getHtml() {
        return '<div id="' + v_mainId + '">' +
            '<div></div><input value="' + (v_customData.text != undefined ? v_customData.text : '') + '">' + 
        '</div>';
    }
}

CView_Label.getHelp = function() {
    return "A single label that can also be used for input.";
}

CView_Label.expectsInterface = function() {
    return [
        {
            "optional": ["setValue", "getListWithElementInfo"]
        },
        {
            "optional": ["getState"]
        },
        {
            "optional": ["isWritable"]
        }
    ];
};

CView_Label.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_Label",
        "type": "object",
        "properties": {
            "class": {
                "type": "string",
                "description": "the css class of the view",
                "default": "BasicLabel"
            },
            "text": {
                "type": "string",
                "description": "the label text"
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema, ViewUtils.commonElementSchema);
    return schema;
};
//# sourceURL=WebApplicationFramework\Views\View_Label.js