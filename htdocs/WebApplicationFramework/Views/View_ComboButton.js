// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_ComboButton(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";

    /** constructor */

    var v_id = p_mainId;
    var v_parentId = p_parentId;
    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_ComboButton)[0];
    var v_data = p_data;
    if (v_data["class"] == undefined) {
        v_data["class"] = "ComboButton";
    }

    var v_counter = 0;

    var base = new CView_BasicButton(p_viewmodels, p_mainId, p_parentId, p_data);

    this.applicationCreated = function() {
        base.applicationCreated();
        if (v_viewmodel != undefined) {
            $("#" + v_id).append('<div><img src="WebApplicationFramework/Res/arrowDown.png"></div>');
        }

        $("#" + v_id + " > div").click(function(event) {
            event.stopPropagation();
            event.preventDefault();

            var data = v_viewmodel.getList().values[1];
            if (data == undefined || data.length == 0) {
                return;
            }

            var customDataForMenu = {
                "offset": {top: event.clientY - 10, left: event.clientX - 290}
            };
            var id = v_id + "_ContextMenu";
            var contextMenu = new CView_ContextMenu([{
                "getMenuElements": function() {
                    var toReturn = [];
                    var data = v_viewmodel.getList().values[1];
                    if (data != undefined) {
                        for (var i = 0; i < data.length; ++i) {
                            var clicker = new ContextMenuItemClicker(i);
                            toReturn.push({
                                "text": data[i][0],
                                "callback": clicker.click
                            })
                        }
                    }
                    return toReturn;
                }
            }], id, "TSGuiFrameworkMain", customDataForMenu);
            contextMenu.applicationCreated();
            ViewUtils.applyCss(customDataForMenu, id);
        });

        $("#" + v_id).hover(function() {
            var data = v_viewmodel.getList();
            $("#" + v_id).prop("title", data.values[1][data.selections[0][0]][0]);
        });
    };

    this.refresh = function(fullRefresh) {
        base.refresh(fullRefresh);
        if (v_counter > 0) {
            --v_counter;
            if (v_counter == 0) {
                base.onClick();
            }
        }
    }

    function ContextMenuItemClicker(p_index) {
        var index = p_index;

        this.click = function() {
            v_viewmodel.select(index);
            v_counter = 2;
        }
    }
}

CView_ComboButton.getHelp = function() {
    return "A button view combined with a combo box.";
};

CView_ComboButton.expectsInterface = function() {
    return [
        {
            "mandatory": ["setValue", "getListWithElementInfo", "select"]
        },
        {
            "optional": ["getState"]
        }
    ];
};

CView_ComboButton.getCustomDataSchema = function() {
    return CView_BasicButton.getCustomDataSchema();
};

//# sourceURL=WebApplicationFramework\Views\View_ComboButton.js