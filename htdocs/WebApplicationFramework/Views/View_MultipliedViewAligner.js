// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_MultipliedViewAligner(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;
    
    var v_viewmodels = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_MultipliedViewAligner);
    var v_dataViewmodel = v_viewmodels[0];
    var v_flexViewmodel = v_viewmodels[1];
    var v_conditionViewmodel = v_viewmodels[2];
    
    var v_multipliedSubviews = [];
    var v_alignerSize = 0;

    var v_last_viewmodelBundle;

    var v_this = this;

    /** public functions */

    this.applicationCreated = function() {
        preprocessSubviewDescriptors(v_customData, 0);
    };

    this.setSelection = function(selection) {
        $("#" + v_mainId + " .Aligner").removeClass("Selected");
        if (selection != undefined)
            for (var i = 0; i < selection.length; ++i)
                $("#" + v_mainId + "_" + selection[i] + " .Aligner").addClass("Selected");
    };

    this.refresh = function(p_fullRefresh) {
        if (p_fullRefresh) {
            var viewmodelBundle = v_dataViewmodel.getViewmodelBundle();
            if (v_last_viewmodelBundle != viewmodelBundle)
            {
                v_last_viewmodelBundle = viewmodelBundle;
                $("#" + v_parentId).empty();
                v_multipliedSubviews = [];
                fullRefresh();
                v_this.setSelection(v_dataViewmodel.getSelectionIndx());
            }
        } else if (ViewUtils.checkVisibility(v_conditionViewmodel, v_mainId)) {
            for (var i = 0; i < v_multipliedSubviews.length; ++i) {
                v_multipliedSubviews[i].refresh();
                v_this.setSelection(v_dataViewmodel.getSelectionIndx());
            }
        }
    };
    
    /** private functions */
    
    function preprocessSubviewDescriptors(descriptor, counter) {
        if (descriptor.multipliedSubviews != undefined) {
            descriptor.idsCreating = [];
            for (var i = 0; i < descriptor.multipliedSubviews.length; ++i) {
                descriptor.idsCreating[i] = "" + counter;
                counter = preprocessSubviewDescriptors(descriptor.multipliedSubviews[i], counter + 1);
            }
        }
        
        return counter;
    }
    
    function fullRefresh() {
        v_alignerSize = v_last_viewmodelBundle.length;
        v_customData.idsCreating = [];
        for (var i = 0; i < v_last_viewmodelBundle.length; ++i) {
            v_customData.idsCreating.push(v_mainId + "_" + i);
        }
        createAligner();
        for (var i = 0; i < v_last_viewmodelBundle.length; ++i) {
            createSubviews(v_last_viewmodelBundle[i], v_customData.multipliedSubviews, [v_customData.idsCreating[i]], v_mainId + "_" + i + "_");
        }
    }
    
    function createAligner() {
        var aligner = new CView_Aligner(p_viewmodels, v_mainId, v_parentId, v_customData);
        if (v_customData.flexCalculation || v_customData.resizable) {
            aligner.getLenghtsFromViewmodel = getLenghtsFromViewmodel;
        }
        aligner.applicationCreated();
        ViewUtils.applyCss(v_customData, v_mainId);
        ViewUtils.processCss(v_customData, v_parentId);
    }
    
    function getLenghtsFromViewmodel() {
        var sizes = [];
        for (var i = 0; i < v_alignerSize; ++i) {
            sizes[i] = ((1 / v_alignerSize) * 100) + "%";
        }
        return sizes;
    }
    
    function createSubviews(viewmodelBundle, subviewDescriptors, parentIds, idPrefix) {
        if (subviewDescriptors != undefined) {
            for (var i = 0; i < subviewDescriptors.length; ++i) {
                var viewmodels = getViewmodels(viewmodelBundle, subviewDescriptors[i].viewmodelIdexes);
                var parentId = parentIds[i];
                var id = parentId + "_subview";
                var customData = mcopy(subviewDescriptors[i]);
                if (customData.idsCreating != undefined) {
                    for (var j = 0; j < customData.idsCreating.length; ++j) {
                        customData.idsCreating[j] = idPrefix + customData.idsCreating[j];
                    }
                }
                
                if (subviewDescriptors[i].subView != undefined && window[subviewDescriptors[i].subView] != undefined) {
                    var subview = new window[subviewDescriptors[i].subView](viewmodels, id, parentId, customData);
                    subview.applicationCreated();
                    ViewUtils.applyCss(customData, id);
                    ViewUtils.processCss(customData, parentId);
                    $("#" + parentId).on("click", function(a) {
                        
                        var index = $(this).index();
                        if (index >= 0) {
                            v_dataViewmodel.select(index);
                        }
                    });
                    
                    v_multipliedSubviews.push(subview);
                    subview.refresh(true);
                    
                    if (subviewDescriptors[i].subView != "CView_MultipliedViewAligner") {
                        createSubviews(viewmodelBundle, customData.multipliedSubviews, customData.idsCreating, idPrefix);
                    }
                } else {
                    alert("Could not create subview " + subviewDescriptors[i].subView);
                }
            }
        }
    }
    
    function getViewmodels(bundle, indexes) {
        var viewmodels = [];
        if (indexes != undefined) {
            for (var i = 0; i < indexes.length; ++i) {
                viewmodels.push(bundle[indexes[i]]);
            }
        }
        return viewmodels;
    }
}

CView_MultipliedViewAligner.getHelp = function() {
    return "An aligner that creates its subviews specified in the custom data using a viewmodel bundle. See CView_Aligner.";
};

CView_MultipliedViewAligner.expectsInterface = function() {
    var expectedInterface = CView_Aligner.expectsInterface();
    expectedInterface.unshift({
        "optional": ["getState"]
    });
    expectedInterface.unshift({
        "optional": ["getChildPercentagesFromFlex"]
    });
    expectedInterface.unshift({
        "mandatory": ["getViewmodelBundle"]
    });
    return expectedInterface;
};

CView_MultipliedViewAligner.getCustomDataSchema = function() {
    var schema = CView_Aligner.getCustomDataSchema();
    schema.title = "Custom data for CView_MultipliedViewAligner";
    schema.properties.multipliedSubviews = {
        "type": "array",
        "format": "tabs",
        "description": "The subviews and their custom data.",
        "items": {
            "type": "object",
            "title": "subview",
            "properties": {
                "subView" : {
                    "type" : "string",
                    "description": "The class name of the subview."
                },
                "viewmodelIdexes": {
                    "type": "array",
                    "format": "table",
                    "description": "The index of the connected viewmodels in a bundle.",
                    "items": {
                        "type": "integer",
                        "min": 0
                    }
                }
            },
            "required": ["subView"],
            "additionalProperties": true
        },
        "minItems": 1
    }
    schema.required = ["multipliedSubviews"];
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_MultipliedViewAligner.js