// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_ElementAligner(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";

    /** constructor */

    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;

    var v_viewmodels = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_ElementAligner);
    var v_dataViewmodel = v_viewmodels[0];
    var v_flexViewmodel = v_viewmodels[1];
    var v_conditionViewmodel = v_viewmodels[2];

    var v_aligner;
    var v_subViews = [];
    var v_currentListWithElementInfo;

    var v_this = this;

    /** public functions */

    this.applicationCreated = function() {
        createSubViews();
    };

    this.refresh = function(p_fullRefresh) {
        if (ViewUtils.checkVisibility(v_conditionViewmodel, v_mainId)) {
            if (v_dataViewmodel != undefined) {
                v_currentListWithElementInfo = v_dataViewmodel.getListWithElementInfo();
            }
            for (var i = 0; i < v_subViews.length; ++i) {
                if (v_subViews[i] != undefined && v_subViews[i].refresh != undefined) {
                    v_subViews[i].refresh(p_fullRefresh);
                }
            }
        }
    };

    /** private functions */

    function getAligner() {
        var aligner = new CView_Aligner(p_viewmodels, v_mainId, v_parentId, v_customData);
        if (v_customData.flexCalculation || v_customData.resizable) {
            aligner.getLenghtsFromViewmodel = getLenghtsFromViewmodel;
        }
        return aligner;
    }

    function getLenghtsFromViewmodel() {
        if (v_flexViewmodel != undefined && v_flexViewmodel.getChildPercentagesFromFlex != undefined) {
            var flexes = [];
            var total = 0;
            var subviews = v_customData.subviews;
            for (var i = 0; i < subviews.length; ++i) {
                if (subviews[i].flex != undefined) {
                    flexes.push(subviews[i].flex);
                    total += subviews[i].flex;
                } else {
                    flexes.push(1);
                    total += 1;
                }
            }
            v_flexViewmodel.getChildPercentagesFromFlex(flexes, total);
            return flexes;
        } else {
            return undefined;
        }
    }

    function createSubViews() {

        v_customData.idsCreating = [];
        var viewData = [];

        var subViewDescriptors = v_customData.subviews;
        for (var i = 0; i < subViewDescriptors.length; ++i) {
            if (subViewDescriptors[i].subView != undefined && window[subViewDescriptors[i].subView] != undefined) {
                var parentId = v_mainId + "_SubView_" + i;
                var viewId = v_mainId + "_SubView_" + i + "_View";
                v_customData.idsCreating.push(parentId);
                viewData[i] = {
                    "parentId": parentId,
                    "viewId": v_mainId + "_SubView_" + i + "_View",
                    "customData": subViewDescriptors[i]
                };
            } else {
                alert(subViewDescriptors[i].subView + " is not a valid view");
            }
        }

        v_aligner = getAligner();
        v_aligner.applicationCreated();

        for (var i = 0; i < viewData.length; ++i) {
            if (viewData[i] != undefined) {
                var viewmodels = [];
                if (v_dataViewmodel != undefined) {
                    viewmodels = [new ViewmodelProxy(i)];
                }
                var subView = new window[viewData[i].customData.subView](viewmodels, viewData[i].viewId, viewData[i].parentId, viewData[i].customData);
                subView.applicationCreated();
                ViewUtils.applyCss(viewData[i].customData, viewData[i].viewId);
                ViewUtils.processCss(viewData[i].customData, viewData[i].parentId);

                v_subViews[i] = subView;
            }
        }
    }

    function ViewmodelProxy(p_index) {

        var v_index = p_index;

        this.select = v_dataViewmodel.select;

        this.getList = function() {
            return {
                "selections": v_currentListWithElementInfo.selections,
                "values": [[v_currentListWithElementInfo.values[v_index].val, undefined, v_currentListWithElementInfo.values[v_index].isWritable]]
            };
        };

        this.getListWithElementInfo = function() {
            return {
                "selections": v_currentListWithElementInfo.selections,
                "values": [v_currentListWithElementInfo.values[v_index]]
            };
        };

        this.setValue = function(aDataPathIndex, aValue, aIndexInList, p_lastSelectionIndexes, p_callback, p_additionalData) {
            v_dataViewmodel.setValue(v_index, aValue, aIndexInList, p_callback, p_additionalData);
        };

        this.getDataList = v_dataViewmodel.getDataList;
    }
}

CView_ElementAligner.getHelp = function() {
    return "An aligner that creates its subviews specified in the custom data. See CView_Aligner.";
};

CView_ElementAligner.expectsInterface = function() {
    var expectedInterface = CView_Aligner.expectsInterface();
    expectedInterface.unshift({
        "optional": ["getState"]
    });
    expectedInterface.unshift({
        "optional": ["getChildPercentagesFromFlex"]
    });
    expectedInterface.unshift({
        "optional": ["getListWithElementInfo", "setValue", "select"]
    });
    return expectedInterface;
};

CView_ElementAligner.getCustomDataSchema = function() {
    var schema = CView_Aligner.getCustomDataSchema();
    schema.title = "Custom data for CView_ElementAligner";
    schema.properties.subviews = {
        "type": "array",
        "format": "tabs",
        "description": "The subviews and their custom data",
        "items": {
            "type": "object",
            "title": "subview",
            "properties": {
                "subView" : {
                    "type" : "string",
                    "description": "the class name of the subview"
                }
            },
            "required": ["subView"],
            "additionalProperties": true
        },
        "minItems": 1
    }
    schema.required = ["subviews"];
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_ElementAligner.js