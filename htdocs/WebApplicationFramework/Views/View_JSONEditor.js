// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_JSONEditor(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";

    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_JSONEditor)[0];
    var v_viewId = p_mainId;
    var v_parentId = p_parentId;

    var v_editorId = v_viewId + "_Editor";
    var v_editor;
    var v_editorDiv;

    var v_customData = p_data;
    if (v_customData["class"] == undefined) {
        v_customData["class"] = "JSONEditor";
    }

    var v_name = "";
    if (v_customData.headerText != undefined) {
        v_name = v_customData.headerText;
    }

    var v_this = this;

    /** public functions */

    this.applicationCreated = function() {
        JSONEditor.defaults.options.theme = 'jqueryui';
        JSONEditor.defaults.editors.integer = JSONEditor.defaults.editors.number.extend({
            sanitize: function(value) {
                return value + "";
            }
        });

        $("#" + v_viewId).remove();
        $("#" + v_parentId).append('<div id="' + v_viewId + '" class="' + v_customData["class"] + '"></div>');
        v_editorDiv = $("#" + v_viewId);

        v_editorDiv.append('<div id="' + v_viewId + '_Header" class="' + v_customData["class"] + '_Header"><h3>' + v_name + '</h3></div>');

        $("#" + v_viewId + "_Header").append('<button id="' + v_viewId + '_Save" class="JSONEditor_Save">Save</button>');
        if (!v_customData.manualSaveRequired) {
            $('#' + v_viewId + '_Save').hide();
        } else {
            $('#' + v_viewId + '_Save').on("click", submit);
        }

        if (v_customData.closeable) {
            $("#" + v_viewId + "_Header").append('<button id="' + v_viewId + '_Close" class="JSONEditor_Close">X</button>');
            $("#" + v_viewId + "_Close").on("click", close);
        }
        v_editorDiv.append('<div id="' + v_viewId + '_Editor"></div>');

        v_editorDiv.on("keydown", function(event) {
            event.stopPropagation();
        });

        v_editorDiv.on('input propertychange', 'input', function() {
            this.dispatchEvent(createEvent('change'));
        });

        if (v_customData.editorOptions == undefined) {
            v_customData.editorOptions = {
                "disable_edit_json": true,
                "keep_oneof_values": false,
                "display_required_only": true
            }
        }

        v_customData.editorOptions.schema = v_viewmodel.getSchema();
        v_editor = new JSONEditor(document.getElementById(v_viewId + "_Editor"), v_customData.editorOptions);

        if (!v_customData.manualSaveRequired) {
            v_editor.on("change", submit);
        }

        if (v_customData.draggable) {
            var handle = $("#" + v_viewId + "_Header > H3");
            handle.css("cursor", "move");
            v_editorDiv.css("position", "absolute");
            v_editorDiv.draggable({
                containment: [$("#" + v_parentId).offset().left, $("#" + v_parentId).offset().top, 20000, 20000],
                scroll: true,
                handle: handle
            });
        }

        if (v_customData.draggable && v_customData.offset != undefined) {
            v_editorDiv.offset(v_customData.offset);
            // in IE and chrome we need to set the offset twice so it gets calculated correctly
            v_editorDiv.offset(v_customData.offset);
        }
    };

    this.refresh = function(p_fullRefresh) {
        function dataArrived(data) {
            v_editor.setValue(data);
        }

        if (p_fullRefresh) {
            if (v_customData.initOnRefresh) {
                v_this.applicationCreated();
            }
            v_viewmodel.getJSONData(dataArrived);
        }
    };

    /** private functions */

    function submit() {
        var errors = v_editor.validate(v_editor.getValue());
        if (errors.length === 0) {
            v_viewmodel.setJSONData(v_editor.getValue());
        }
    }

    function close() {
        v_editorDiv.remove();
    }
}

CView_JSONEditor.getHelp = function() {
    return "A JSON schema editor view.";
};

CView_JSONEditor.expectsInterface = function() {
    return [
        {
            "mandatory": ["getSchema", "setJSONData", "getJSONData"]
        }
    ];
};

CView_JSONEditor.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_JSONEditor",
        "type": "object",
        "properties": {
            "class": {
                "description": "The css class of the JSON Editor",
                "type": "string",
                "default": "JSONEditor"
            },
            "headerText": {
                "description": "The title of the editor.",
                "type": "string"
            },
            "manualSaveRequired": {
                "description": "Whether we must press the save button to save the data.",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            },
            "initOnRefresh": {
                "description": "Whether we initialize the editor on every full refresh. Useful if the same editor is used with different schemas.",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            },
            "closeable": {
                "description": "Whether we want to allow closing the editor.",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            },
            "draggable": {
                "description": "Whether we want the editor to be draggable.",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            },
            "offset": {
                "description": "The offset of the editor. Only works with draggable.",
                "type": "object",
                "properties": {
                    "top": {
                        "type": "integer"
                    },
                    "left": {
                        "type": "integer"
                    }
                }
            },
            "editorOptions": {
                "type": "object",
                "description": 'The JSON Editor options, see https://github.com/jdorn/json-editor for more details.',
                "additionalProperties": true
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema);
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_JSONEditor.js