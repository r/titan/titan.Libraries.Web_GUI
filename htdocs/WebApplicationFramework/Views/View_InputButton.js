// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_InputButton(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";

    /** constructor */

    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_InputButton)[0];
    var v_data = p_data;
    if (v_data.prompt == undefined) {
        v_data.prompt = "";
    }
    var base = new CView_BasicButton(p_viewmodels, p_mainId, p_parentId, p_data);

    this.applicationCreated = base.applicationCreated;
    this.refresh = base.refresh;

    base.onClick = function() {
        var inputValue = prompt(v_data.prompt);
        if (inputValue != undefined) {
            v_viewmodel.setValue(0, inputValue);
        }
    }
}

CView_InputButton.getHelp = function() {
    return "A button view that can be used to input data.";
};

CView_InputButton.expectsInterface = function() {
    return CView_BasicButton.expectsInterface();
};

CView_InputButton.getCustomDataSchema = function() {
    var baseSchema = CView_BasicButton.getCustomDataSchema();
    var schema = {
        "properties": {
            "prompt": {
                "description": "The promt text.",
                "type": "string"
            }
        }
    };
    $.extend(true, baseSchema, schema);
    return baseSchema;
};

//# sourceURL=WebApplicationFramework\Views\View_ButtonForDownload.js