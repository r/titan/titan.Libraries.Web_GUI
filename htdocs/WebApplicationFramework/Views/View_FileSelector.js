// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_FileSelector(p_viewmodels, p_mainId, p_parentId, p_options) {
    "use strict";
    
    var HTML = "WebApplicationFramework/Views/View_FileSelector.html";
    var v_parentId = p_parentId;
    var v_mainId = p_mainId;
    
    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_FileSelector)[0];
    var v_customData = p_options;
    
    var v_this = this;
    
    this.applicationCreated = function() {
        var mainDiv = document.createElement("div");
        mainDiv.setAttribute("id", v_mainId);
        $("#" + v_parentId).append(mainDiv);
        
        function htmlLoaded(ok, data) {
            if (ok) {
                $('#' + v_mainId).append(data);
                
                var select = $('#' + v_mainId + " .FileSelector_Filters");
                var options = v_viewmodel.getFileTypeList();
                for (var i = 0; i < options.length; ++i) {
                    select.append(new Option(options[i].text, options[i].value));
                }
                select.change(function() {
                    v_viewmodel.setFilter($('#' + v_mainId + " .FileSelector_Filters option:selected").text(), refresh);
                });
                
                setupCallbacks();
                v_viewmodel.home(refresh);
                
            } else {
                alert("Error loading html file: " + HTML);
            }
        }
        
        v_viewmodel.loadFile(HTML, htmlLoaded);
    };
    
    function refresh(fullRefreshView) {
        if (fullRefreshView) {
            fullRefresh();
        } else {
            selectionRefresh();
        }
    }
    
    function fullRefresh() {
        var path = v_viewmodel.getCurrentDir();
        $('#' + v_mainId).find(".FileSelector_Bread").text(path);
        
        var files = v_viewmodel.getCurrentDirElements();
        files = files.sort(sortFiles);
        
        var ul = $('#' + v_mainId).find("ul");
        ul.children().remove();
        for (var i = 0; i < files.length; ++i) {
            
            var type = files[i]["type"];
            var path = files[i]["name"];
            var displayName = files[i]["displayName"];
            var size = files[i]["size"];
            var timestamp = files[i]["timestamp"];
            var id = files[i]["id"];
            
            var li = document.createElement('li');
            li.setAttribute("filetype", type);
            li.setAttribute("path", path);
            li.setAttribute("pathId", id);
            
            if (type.endsWith("d")) {
                li.innerHTML = "<span class='FileSelector_Name'>" + displayName + "</span>";
            } else if (type.endsWith("f")) {
                li.innerHTML = "<span class='FileSelector_Name'>" + displayName + "</span><span class='FileSelector_Size'>" + size + "</span><span class='FileSelector_Time'>" + (new Date(timestamp * 1000)).toISOString() + "</span>";
            } else {
                li.innerHTML = type + ": <span class='FileSelector_Name'>" + displayName + "</span><span class='FileSelector_Size'>" + size + "</span><span class='FileSelector_Time'>" + (new Date(timestamp * 1000)).toISOString() + "</span>";
            }
            ul.append(li);
            
            $(li).click(onClick);
            $(li).dblclick(onDoubleClick);
        }
        
        $("#" + v_mainId + " .FileSelector_Back").prop("disabled", !v_viewmodel.allowBack());
        $("#" + v_mainId + " .FileSelector_Forward").prop("disabled", !v_viewmodel.allowForward());
        
        selectionRefresh();
    }
    
    function selectionRefresh() {
        var selection = v_viewmodel.getSelection();
        
        $('#' + v_mainId).find("li").each(function() {
            var isSelected = false;
            for (var i = 0; i < selection.length; ++i) {
                if ($(this).attr("path") == selection[i].value) {
                    isSelected = true;
                    break;
                }
            }
            if (isSelected) {
                $(this).addClass("FileSelector_Selected");
            } else {
                $(this).removeClass("FileSelector_Selected");
            }
        });
        
        var selectionString = "";
        for (var i = 0; i < selection.length; ++i) {
            selectionString += selection[i].text + "; ";
        }
        $("#" + v_mainId + " .FileSelector_Selection").val(selectionString);
    }
    
    function onClick(event) {
        event.preventDefault();
        event.stopPropagation();
        
        var obj = $(event.target).closest("li");
        var path = obj.attr("path");
        var id = obj.attr("pathid");
                
        if (event.ctrlKey) {
            v_viewmodel.action(id, path, "ctrlclick", refresh);
        } else {
            v_viewmodel.action(id, path, "click", refresh);
        }
    }
    
    function onDoubleClick(event) {
        event.preventDefault();
        event.stopPropagation();
        
        var obj = $(event.target).closest("li");
        var path = obj.attr("path");
        var id = obj.attr("pathid");
                
        v_viewmodel.action(id, path, "doubleclick", refresh);
    }
    
    function sortFiles(a, b) {
        var isDirA = (a.type.endsWith("d")), isDirB = (b.type.endsWith("d"));
        // Directories first.
        if (isDirA && !isDirB) {
            return -1;
        } else if (isDirB && !isDirA) {
            return 1;
        } else {
            return a.displayName.localeCompare(b.displayName);
        }
    }
    
    function setupCallbacks() {
        $("#" + v_mainId + " .FileSelector_CreateDir").click(function() {
            var name = prompt("Please enter the name of the new directory", "New directory");
            if (name != undefined) {
                v_viewmodel.createDirectory(name, refresh);
            }
        });
        
        $("#" + v_mainId + " .FileSelector_Back").click(function() {
            v_viewmodel.back(refresh);
        });
        
        $("#" + v_mainId + " .FileSelector_Forward").click(function() {
            v_viewmodel.forward(refresh);
        });
        
        $("#" + v_mainId + " .FileSelector_Home").click(function() {
            v_viewmodel.home(refresh);
        });
        
        $("#" + v_mainId + " .FileSelector_Selection").click(function() {
            $(this).val("");
        });
        
        $("#" + v_mainId + " .FileSelector_Selection").keyup(function() {
            v_viewmodel.setSelectionManually($(this).val());
        });
        
        $("#" + v_mainId + " .FileSelector_OkButton").click(function() {
            v_viewmodel.filesSelected();
        });
        
        $("#" + v_mainId + " .FileSelector_CancelButton").click(function() {
            v_viewmodel.fileSelectionCanceled();
        });
    }
}

CView_FileSelector.getHelp = function() {
    return "A file selector view.";
};

CView_FileSelector.expectsInterface = function() {
    return [
        {
            "mandatory": ["filesSelected", "fileSelectionCanceled", "getCurrentDirElements", "getCurrentDir", "action",
                "back", "forward", "allowBack", "allowForward", "home",
                "createDirectory", "getSelection", "setSelectionManually", "getFileTypeList", "setFilter", "exists"]
        }
    ];
};

CView_FileSelector.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_FileSelector",
        "type": "object",
        "properties": {},
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema);
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_FileSelector.js