// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_TabsWithData(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_TabsWithData)[0];
    var v_conditionViewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_TabsWithData)[1];
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;
    if (v_customData.class == undefined) {
        v_customData.class = "TabsWithData";
    }
    
    var v_this = this;
    
    /** public functions */

    this.applicationCreated = function() {
        if (v_viewmodel != undefined) {
            $("#" + v_parentId).append(getHtml(getOrientation()));
            setupCallbacks();
        }
    };

    this.refresh = function(p_fullRefresh) {
        if (v_viewmodel != undefined && ViewUtils.checkVisibility(v_conditionViewmodel, v_mainId)) {
            var dataObject = v_viewmodel.getList();
            if (p_fullRefresh) {
                fullRefresh(dataObject, getOrientation());
            } else {
                setSelection(dataObject);
                setText(dataObject);
            }
        }
    };

    /** private functions */
    
    function fullRefresh(dataObject, orientation) {
        var data = dataObject.values[0];
        var ul = $("#" + v_mainId + "_ul");
        ul.empty();
        
        for (var i = 0; i < data.length; ++i) {
            ul.append('<li class="' + v_customData.class + '_' + orientation + '_Li">' + data[i][0] + '</li>');
        }
        
        setSelection(dataObject);
    }

    function setSelection(dataObject) {
        if (dataObject.selections[0] != undefined) {
            var selected = dataObject.selections[0][0];
            if (selected != undefined) {
                $("#" + v_mainId + "_ul li").removeClass(v_customData.class + "_Selected");
                $('#' + v_mainId + "_ul li:nth-child(" + (selected + 1) + ")").addClass(v_customData.class + "_Selected");
            }
        } else {
            $("#" + v_mainId + "_ul li").removeClass(v_customData.class + "_Selected");
        }

    }
    
    function setText(dataObject) {
        var list = $("#" + v_mainId + "_ul li");
        for (var i = 0; i < Math.min(list.length, dataObject.values[0].length); ++i) {
            $(list[i]).text(dataObject.values[0][i][0]);
        }
    }
    
    function getOrientation() {
        if (v_customData.orientation != undefined && v_customData.orientation.toLowerCase() == "vertical") {
            return "Vertical";
        } else {
            return "Horizontal";
        }
    }
    
    function getHtml(orientation) {
        return '' +
            '<div class="' + v_customData.class + '" id="' + v_mainId + '">' +
                '<div class="' + v_customData.class + '_' + orientation + '_Tabs">' +
                    '<ul id="' + v_mainId + "_ul" + '" class="' + v_customData.class + '_' + orientation + '_Ul"></ul>' +
                '</div>' +
                '<div id="' + v_customData.idsCreating[0] + '" class="' + v_customData.class + '_' + orientation + '_Panel">' + 
            '</div>';
    }

    function setupCallbacks() {
        $('#' + v_mainId + "_ul").on('click', 'li', function() {
            v_viewmodel.select($(this).index());
        });
    }
}

CView_TabsWithData.getHelp = function() {
    return "A tab view that can have one connected subview and it can either be horizontally or vertically alligned.\n" +
        "It will create as many tabs as returned by a connected viewmodel's getList function.";
};

CView_TabsWithData.expectsInterface = function() {
    return [{
        "mandatory": ["getList", "select"]
    }, {
        "optional": ["getState"]
    }];
}

CView_TabsWithData.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_TabsWithData",
        "type": "object",
        "properties": {
            "orientation": {
                "description": "The orientation of the tabs (default horizontal)",
                "type": "string",
                "enum": ["horizontal", "vertical"],
                "default": "vertical"
            },
            "class": {
                "description": "The css class of the tabs",
                "type": "string",
                "default": "TabsWithData"
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema);
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_TabsWithData.js
