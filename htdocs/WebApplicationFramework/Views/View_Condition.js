// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_Condition(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_Condition)[0];
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;
    
    var v_elements;
    var v_prevState = 0;
    
    var v_this = this;
    
    /** public functions */

    this.applicationCreated = function() {
        $("#" + v_parentId).append(getHtml());
        v_elements = $('#' + v_mainId + ' > div');
        v_elements.css("display", "none");
        v_prevState = -1;
        $(v_elements[0]).css("display", "block");
    };

    this.refresh = function(p_fullRefresh) {
        var state = v_viewmodel.getState();
        if (state != v_prevState) {
            v_elements.css("display", "none");
            if (state) {
                $(v_elements[0]).css("display", "block");
            } else {
                $(v_elements[1]).css("display", "block");
            }
            v_prevState = state;
        }
    };

    /** private functions */
    
    function getHtml() {
        var html = '<div id="' + v_mainId + '">';
        for (var i = 0; i < v_customData.idsCreating.length; ++i) {
            html += '<div id="' + v_customData.idsCreating[i] + '"></div>'
        }
        html += '</div>';
        return html;
    }
}

CView_Condition.getHelp = function() {
    return "A view that only shows one of its connected child views based on the state of the connected viewmodel.";
}

CView_Condition.expectsInterface = function() {
    return [
        {
            "mandatory": ["getState"]
        }
    ];
}

CView_Condition.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_Condition",
        "type": "object",
        "properties": {},
        "additionalProperties": false
    };
    
    $.extend(true, schema, ViewUtils.commonViewSchema);
    return schema;
}

//# sourceURL=WebApplicationFramework\Views\View_Condition.js