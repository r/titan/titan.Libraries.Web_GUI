// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * printf() clone - String.prototype.format(formatStr, args...)
 * @param {string} format - format string
 * @param {string} args... - arguments to inject
 * @example
 * // 'zero 1 blabla lastone'
 * '{0} {1} blabla {2}'.format("zero", 1, "lastone");
 * @returns {string} Returns a new string where the arguments are injected into the format string.
 * You can escape curly beraces '{' by doubling it: '{{0}}: {0}'.format("theargument");
 */
String.prototype.format = function() {
    "use strict";
    var args = arguments;
    return this.replace(/\{\{|\}\}|\{(\d+)\}/g, function (m, n) {
        if (m === "{{") { return "{"; }
        if (m === "}}") { return "}"; }
        return args[n];
    });
};

function uniq(list) {
    var seen = {};
    return list.filter(function(item) {
        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}

window.jsErrors = [];

/*if (!window.onerror) {
    window.onerror = function(error, url, line, ch, ex) {
        "use strict";
        var errorMessage = 'Error caught in global context: "{0}", URL: {1}, Line {2}, char index {3} (w/o indent, usually).'.format(error, url, line, ch);
        var stack = "*** Stack *** : " + (ex ? ex.stack : '(No stack available)');
        if (window.console) { 
            console.error(errorMessage);
            console.error(stack);
        }
        window.jsErrors[window.jsErrors.length] = errorMessage;
        window.jsErrors[window.jsErrors.length] = stack;
        return true; // handled.
    };
}*/

/** mpad - converts a number to string and pads it with leading zeroes */
function mpad(num, size) {
    "use strict";
    return ('000000000' + num).substr(-size);
}

/** mlog - logs values to the debug console. Freezez objects to the current vlaue, unlike the native console. */
function mlog() {
    "use strict";
    var obj=[];
    for (var i = 0; i < arguments.length; i++) {
        if (arguments[i] == undefined) {
            obj[i] = undefined;
        } else {
            obj[i] = mcopy(arguments[i]);
        }
    }
    if (window.console) console.log(obj);
}

/** mlog - logs values to the debug console. Freezez objects to the current vlaue, unlike the native console. Also prints call stack */
function mlogc() {
    "use strict";
    var obj=[];
    for (var i = 0; i < arguments.length; i++) {
        if (arguments[i] == undefined) {
            obj[i] = undefined;
        } else {
            obj[i] = mcopy(arguments[i]);
        }
    }
    if (window.console) console.log("CALL STACK :" + new Error().stack, obj);
}

/** mcopy - copies an object through serialization and deserializaton. */
function mcopy(data) {
    "use strict";
    return JSON.parse(JSON.stringify(data));
}
/*
// ONLY enable these if you need them in multiple places.
Array.prototype.mshift = function(times) {
    for (var i = 0; i < times; i++) {
        this.shift();
    }
};

Array.prototype.mpop = function(times) {
    for (var i = 0; i < times; i++) {
        this.pop();
    }
};
*/

/**
 * Replace all occurences of str1 to str2, inside str.
 * @param {string} str - source
 * @param {string} str1 - from
 * @param {string} str2 - to
 * @returns {string} Returns with the replaced string.
 */
function replaceAll(str, str1, str2) {
   "use strict";
   var index = str.indexOf(str1);     
   while (index !== -1) {
      str = str.replace(str1, str2);
      index = str.indexOf(str1);
   }
   return str;
}

/**
 * Pretty prints a JSON serialized string. Contains workarounds for oldff.
 * @param {string} text - JSON text
 * @param {number} indent - Number of spaces to indent
 * @returns {string} Returns with the JSON string with the proper indent, or null if input is not a valid JSON textual representation.
 */
function indentJSON(text, indent) {
    "use strict";
    var anObj;
    var indentStep = indent || 4;
    try {
        anObj = JSON.parse(text);
        var str = JSON.stringify(anObj, undefined, indentStep);
        if (oldff) // There is a strange formatting issue: "[{" is the sequence, and is not newline+indented.
            str = str.replace(/^(\s*).*?(?:\[\{)/gm,
                function(match, p1) {
                    var indent = "\r\n";
                    for(var i = 0; i < indentStep; i++)
                        indent += ' ';
                    return match.replace("[{", "[" + indent + p1 + "{");
                });
        return str;
    } catch (e) {
        return null;
    }
}

/**
 * Shallow Copy of an object.
 * @param {object} obj - object to clone
 * @returns {object} Returns with a new object where the field have their values cloned.
 */
function shallowcopy(obj) {
    "use strict";
    var target = {};
    for (var i in obj) {
        if (obj.hasOwnProperty(i)) {
            target[i] = obj[i];
        }
    }
    return target;
}

/**
 * Create an Event object.
 * @param {string} p_name - Name of the event
 * @returns {Event} Returns with a new, initialized Event.
 */
function createEvent(p_name) {
    "use strict";
    if (oldff) {
        var v_event = document.createEvent('Event');
        v_event.initEvent(p_name, true, true);
        return v_event;
    } else
        return new Event(p_name);
}

/**
 * Generates a sufficiently unique ID to be used in the DOM.
 * @returns {string} Returns with a new, random ID.
 */
function generateId() {
    "use strict";
    return String.fromCharCode(65 + Math.floor(Math.random() * 26) + (Math.round(Math.random()) ? 32 : 0)) + Date.now();
}

/**
 * Deep Copy of an object.
 * @param {object} obj - object to clone
 * @returns {object} Returns with a new object where everything is fully cloned.
 */
function instanceToObject(obj) {
    "use strict";
    var o = {};
    for (var property in obj) {
        if (obj.hasOwnProperty(property)) {
            if (typeof obj[property] === "object") {
                o[property] = instanceToObject(obj[property]);
            } else {
                o[property] = obj[property];
            }
        }
    }
    return o;
}

/** isvalue returns true only if the parameter is not undefined and is not null. */
function isvalue(aSomething) {
    "use strict";
    return !((typeof aSomething === 'undefined') || (aSomething === null));
}

String.prototype.splice = function(index, count, add) {
    return this.slice(0, index) + (add || "") + this.slice(index + count);
}

function hasPrefix(list, prefix) {
    for (var i = 0; i < prefix.length; ++i) {
        if (list[i] == undefined || list[i] != prefix[i]) {
            return false;
        }
    }
    return true;
}

function transpose(matrix) {
    if (matrix.length == 0) return matrix;
    return matrix[0].map(function(col, i) { 
        return matrix.map(function(row) { 
            return row[i];
        })
    });
}

function getLocationParam(p_key) {
    var pairs = location.search.substring(1).split('&');
    for (var i = 0; i < pairs.length; ++i) {
        var keyValue = pairs[i].split('=');
        if (keyValue[0] == p_key) {
            return keyValue[1].replace('/', '');
        }
    }
    return undefined;
}

// No modification if ip has http prefix and no port is given.
// Adds http prefix if there isn't one.
// Joins ip and port if both are given.
// If there is an url with a trailing slash and a port is given, the trailing slash will be removed and the port will be added on.
// If relativeURL is true, current hostname and port is used as the base address
function urlifier(ip, port, relativeURL) {
    var httpPrefix = 'http://';
    if (ip == undefined) {
    	ip = '';
    }
    if (ip != '' && relativeURL != undefined && relativeURL == true) {
      currentAddress=window.location.hostname;
      if (window.location.port != undefined && window.location.port != '') {
        currentAddress = currentAddress + ":" + window.location.port;
      }
      ip = currentAddress + '/' + ip;
      port=undefined;
    }
    if (ip != '' && ip.substr(0, httpPrefix.length).toLowerCase() !== httpPrefix) {
        ip = httpPrefix + ip;
    }
    if (port != undefined) {
        port = ':' + port;
        if (ip.substr(ip.length - 1) === '/') {
            ip = ip.substring(0, ip.length - 1);
        }
    } else {
        port = '';
    }
    return ip + port;
}

// string.encode("hex")
function hex2a(hex) {
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

// string.decode("hex")
function a2hex(str) {
    var arr = [];
    for (var i = 0, l = str.length; i < l; i++) {
        var hex = Number(str.charCodeAt(i)).toString(16);
        arr.push(hex.length > 1 && hex || "0" + hex);
    }
    return arr.join('');
}

function replaceAt(str, index, replacement) {
    return str.substr(0, index) + replacement+ str.substr(index + replacement.length);
}

//# sourceURL=Utils\Utilities.js
