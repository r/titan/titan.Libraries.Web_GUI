// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function BaseDialog(p_parentId, p_id, p_options) {
    "use strict";

    var v_id = p_id;
    var v_parentId = p_parentId;
    var v_options = p_options;

    var v_this = this;
    var v_defaultButtonId;

    $("#" + v_parentId).append('<div id="' + v_id + '"></div>');
    var dialog = $("#" + v_id);

    this.createDialogContent = function() {};

    this.setDefaultButton = function(id) {
        v_defaultButtonId = id;
    };

    this.createButtons = function() {
        return [];
    };

    this.onClosing = function() {};

    this.close = function() {
        dialog.dialog("close");
    };

    function createDialogWindow() {
        if (v_options.height == undefined) {
            v_options.height = "auto";
        }

        if (v_options.width == undefined) {
            v_options.width = "auto";
        }

        dialog.dialog({
            autoOpen: false,
            height: v_options.height,
            width: v_options.width,
            modal: true,
            title: v_options.header,
            buttons: v_this.createButtons(),

            close: function(event, ui) {
                v_this.onClosing();
                $("#" + v_id).remove();
            }
        });
    }

    this.contentCreated = function() {};

    this.open = function() {
        this.createDialogContent();
        createDialogWindow();
        this.contentCreated();

        if (v_defaultButtonId != undefined) {
            dialog.on('keydown', function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code === $.ui.keyCode.ENTER) {
                    e.stopPropagation();
                    $('#' + v_defaultButtonId).click();
                    return false;
                }
            });
        }

        dialog.dialog("open");
    };
}

function ExitDialog(p_parentId, p_id, p_options) {
    "use strict";
    var dialog = new BaseDialog(p_parentId, p_id, p_options);

    var v_id = p_id;
    var v_options = p_options;
    var v_callback = p_options.callback;
    var v_exit = false;
    var v_save = false;

    dialog.createDialogContent = function() {
        $("#" + v_id).append('<p class="validateTips">' + v_options.text + '</p>');
    };

    dialog.onClosing = function() {
        v_callback(v_exit, v_save);
    };

    dialog.createButtons = function() {
        dialog.setDefaultButton(v_id + "_YesButton");
        return [
            {
                text: "Yes",
                id: v_id + "_YesButton",
                click:  function () {
                    v_exit = true;
                    v_save = true;
                    dialog.close();
                }
            },
            {
                text: "No",
                id: v_id + "_NoButton",
                click:  function () {
                    v_exit = true;
                    v_save = false;
                    dialog.close();
                }
            },
            {
                text: "Cancel",
                id: v_id + "_CancelButton",
                click:  function () {
                    v_exit = false;
                    v_save = false;
                    dialog.close();
                }
            }
        ];
    };

    return dialog;
}

function ConfirmationDialog(p_parentId, p_id, p_options) {
    "use strict";
    var dialog = new BaseDialog(p_parentId, p_id, p_options);

    var v_id = p_id;
    var v_options = p_options;
    var v_callback = p_options.callback;

    dialog.createDialogContent = function() {
        $("#" + v_id).append('<p class="validateTips">' + v_options.text + '</p>');
    };

    dialog.createButtons = function() {
        dialog.setDefaultButton(v_id + "_OkButton");
        return [
            {
                text: "OK",
                id: v_id + "_OkButton",
                click:  function () {
                    $("#" + v_id).dialog("close");
                    v_callback();
                }
            }
        ];
    };

    return dialog;
}

function InputDialog(p_parentId, p_id, p_options) {
    "use strict";
    var dialog = new BaseDialog(p_parentId, p_id, p_options);

    var v_id = p_id;
    var v_options = p_options;
    var v_validator = v_options.validator;
    var v_callback = v_options.callback;

    var v_formId = v_id + "_Form";
    var v_fieldId = v_id + "_Field";
    var v_default = p_options.defaultValue;
    var v_validationId = v_id + '_Validation';

    dialog.createDialogContent = function() {
        var htmlContent = '<p class="validateTips">' + v_options.text + '</p>';
        htmlContent += '<form name="' + v_formId + '" id="' + v_formId + '"><input type="text" size="32" id="' + v_fieldId + '" value="' + v_default + '"/><br /><p id="' + v_validationId + '" class="ui-inputdialog-validate"></p><input type="submit" tabindex="-1" style="position:absolute; top:-100px"></form>';
        $("#" + v_id).append(htmlContent);

        if (v_validator != undefined) {
            $("#" + v_fieldId).on('keyup', function() {
                $('#' + v_validationId).html(v_validator($("#" + v_fieldId).val()));
            });
        }
    };

    dialog.createButtons = function() {
        dialog.setDefaultButton(v_id + "_OkButton");
        return [
            {
                text: "OK",
                id: v_id + "_OkButton",
                click:  function () {
                    var value = $("#" + v_fieldId).val();
                    if (v_validator == undefined || v_validator(value) === "") {
                        $("#" + v_id).dialog("close");
                        v_callback(value);
                    }
                }
            }
        ];
    };

    return dialog;
}

function ChoiceDialog(p_parentId, p_id, p_options) {
    "use strict";
    var dialog = new BaseDialog(p_parentId, p_id, p_options);

    var v_id = p_id;
    var v_options = p_options;
    var v_choices = v_options.choices;
    var v_callback = v_options.callback;

    var v_okButtonId = v_id + "_OkButton";
    var v_tableId = v_id + "_Table";

    function search(input, choices, callback) {
        var indexes = [];
        for (var i = 0; i < choices.length; ++i) {
            if (choices[i].text.indexOf(input) != -1) {
                indexes.push(i)
            }
        }

        callback(indexes);
    }

    function searchComplete(indexes) {
        $(".dialog-table tr").addClass("hidden");
        for (var i = 0; i < indexes.length; ++i) {
            $(".dialog-table tr:nth-child(" + (indexes[i] + 1) + ")").removeClass("hidden");
        }
        $(".validateTips").text(v_options.text);
    }

    dialog.getTableId = function() {
        return v_tableId;
    };

    dialog.getRows = function() {
        var rows = [];
        for (var i = 0; i < v_choices.length; ++i) {
            var radioId = v_id + '_RadioButton_' + v_choices[i].text;
            rows.push([
                '<input type="radio" name="class" id="' + radioId + '" value="' + v_choices[i].value + '"/>',
                '<label for="' + radioId + '">' + v_choices[i].text + '</label>'
            ]);
        }
        return rows;
    };

    dialog.createDialogContent = function() {
        var htmlContent = '<p class="validateTips">' + v_options.text + '</p>';
        htmlContent += '<input class="validateSearch" type="text"><button class="validateSearchButton">Search</button><button class="validateSearchClear">Show All</button>';
        htmlContent += '<table class="dialog-table" id="' + v_tableId + '">';

        var rows = dialog.getRows();
        for (var i = 0; i < rows.length; ++i) {
            htmlContent += '<tr>';
            for (var j = 0; j < rows[i].length; ++j) {
                htmlContent += '<td>' + rows[i][j] + '</td>';
            }
            htmlContent += '</tr>';
        }

        htmlContent += '</table>';
        $("#" + v_id).append(htmlContent);


        $(".dialog-table tr").dblclick(function() {
            $('#' + v_okButtonId).click();
        });

        $(".validateSearchButton").click(function() {
            $(".validateTips").text("Searching...");
            if (v_options.searchFunction != undefined) {
                v_options.searchFunction($(".validateSearch").val(), v_choices, searchComplete);
            } else {
                search($(".validateSearch").val(), v_choices, searchComplete);
            }
        });

        $(".validateSearchClear").click(function() {
            $(".dialog-table tr").removeClass("hidden");
        });

        $(".validateSearch").on('keydown', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code === $.ui.keyCode.ENTER) {
                e.stopPropagation();
                $('.validateSearchButton').click();
                return false;
            }
        });
    };

    dialog.createButtons = function() {
        dialog.setDefaultButton(v_okButtonId);
        return [
            {
                text: "OK",
                id: v_okButtonId,
                click:  function () {
                    var selection = $("#" + v_tableId + " input[type='radio']:checked").val();
                    if (selection) {
                        $("#" + v_id).dialog("close");
                        $("#" + v_id).remove();
                        v_callback(selection);
                    }
                }
            }
        ];
    };

    return dialog;
}

function ChoiceDialogWithButton(p_parentId, p_id, p_options) {
    "use strict";
    var dialog = new ChoiceDialog(p_parentId, p_id, p_options);

    var v_id = p_id;
    var v_options = p_options;
    if (v_options.buttonStyle == undefined) {
        v_options.buttonStyle = "";
    }
    if (v_options.buttonText == undefined) {
        v_options.buttonText = "?";
    }
    var v_choices = v_options.choices;

    dialog.getRows = function() {
        var rows = [];
        for (var i = 0; i < v_choices.length; ++i) {
            var radioId = v_id + '_RadioButton_' + v_choices[i].text;
            rows.push([
                '<input type="radio" name="class" id="' + radioId + '" value="' + v_choices[i].value + '"/>',
                '<label for="' + radioId + '">' + v_choices[i].text + '</label>',
                '<button style="' + v_options.buttonStyle + '" id="setupDeleteButton_' + i + '" type="button" idx="' + i + '">' + v_options.buttonText + '</button>'
            ]);
        }
        return rows;
    };

    dialog.contentCreated = function() {
        $("#" + dialog.getTableId() ).on("click", "button", function() {
            v_options.buttonHandler(v_choices[parseInt($(this).attr("idx"), 10)].value);
            if (v_options.closeOnButtonPress) {
                $("#" + v_id).dialog("close");
                $("#" + v_id).remove();
            }
        });
    };

    return dialog;
}

function FileDialog(p_fileHandler, p_parentId, p_id, p_options) {

    var v_id = p_id;
    var v_options = p_options;
    var v_fileHandler = p_fileHandler;
    var v_fileHandlerViewmodel = {"getFileHandler": function() {return v_fileHandler;}};
    var v_callback = v_options.callback;

    var v_viewmodel = new CViewModel_FileSelector(v_fileHandlerViewmodel, v_options);
    var v_view = new CView_FileSelector([v_viewmodel], v_id + "_FileSelectorContent", v_id, {});

    if (v_options.height == undefined) {
        v_options.height = $(window).height() * 0.8;
    }

    if (v_options.width == undefined) {
        v_options.width = $(window).width() * 0.8;
    }

    var dialog = new BaseDialog(p_parentId, p_id, v_options);

    dialog.createDialogContent = function() {
        $("#" + v_id).append('<p class="validateTips">' + v_options.text + '</p>');
        v_view.applicationCreated();
    };

    dialog.createButtons = function() {
        v_viewmodel.filesSelected = function() {
            var selection = v_viewmodel.getSelection();
            if (selection.length > 0) {
                if (v_options["dialogType"] == "save") {
                    v_viewmodel.exists(selection[0].value, ifExists)
                } else {
                    toReturn();
                }
            }
        }

        v_viewmodel.fileSelectionCanceled = function() {
            $("#" + v_id).dialog("close");
        }
    };

    function ifExists(exists) {
        if (exists) {
            if (confirm("File already exists, overwrite it?")) {
                toReturn();
            }
        } else {
            toReturn();
        }
    }

    function toReturn() {
        $("#" + v_id).dialog("close");
        v_callback(v_viewmodel.getSelection());
    }

    return dialog;
}

var ViewUtils = {};

ViewUtils.jumpToEditor = function(id) {
    $("#" + id)[0].scrollIntoView({block: "end", behavior: "smooth"});
};

ViewUtils.applyCss = function(a_customData, a_id) {
    if (a_customData.css != undefined) {
        $("#" + a_id).css(a_customData.css);
    }
};

ViewUtils.processCss = function(a_customData, a_parentId) {
    if (a_customData.processCss != undefined) {
        var cssString = '';
        for (var selector in a_customData.processCss) {
            cssString += "#" + a_parentId + " " + selector + " {";
            for (var rule in a_customData.processCss[selector]) {
                cssString += rule + ": " + a_customData.processCss[selector][rule] + ";";
            }
            cssString += "}\n";
        }
        $("#" + a_parentId).prepend("<style type='text/css'>" + cssString + "</style>");
    }
};

ViewUtils.getViewmodelsFromExpectedInterface = function(a_viewmodels, a_class) {
    var viewmodels = [];
    var expectedInterface = a_class.expectsInterface();

    for (var i = 0; i < expectedInterface.length; ++i) {
        var interfaceList;
        if (expectedInterface[i].mandatory != undefined) {
            interfaceList = expectedInterface[i].mandatory;
        } else if (expectedInterface[i].optional != undefined) {
            interfaceList = expectedInterface[i].optional;
        } else {
            continue;
        }

        for (var j = 0; j < a_viewmodels.length; ++j) {
            var ok = true;
            for (var k = 0; ok && k < interfaceList.length; ++k) {
                if (a_viewmodels[j][interfaceList[k]] == undefined) {
                    ok = false;
                }
            }
            if (ok) {
                viewmodels[i] = a_viewmodels[j];
                break;
            }
        }
    }

    return viewmodels;
};

ViewUtils.addLabel = function(a_id, a_text, a_class) {

    if (a_text == undefined) {
        a_text = "";
    }

    if (a_class == undefined) {
        a_class = "CustomizableApp_LabelAtTop";
    }

    var obj = $("#" + a_id);
    var html = '<label class="' + a_class + '">' + a_text + '</label><br/>';
    obj.prepend(html);

    return $("#" + a_id + " > *").first();
};

ViewUtils.checkVisibility = function(p_conditionViewmodel, p_id) {
    var isVisible = p_conditionViewmodel == undefined || p_conditionViewmodel.getState();
    if (isVisible) {
        $("#" + p_id).removeClass("hidden");
        return true;
    } else {
        $("#" + p_id).addClass("hidden");
        return false;
    }
};

ViewUtils.getSuggestedHeight = function(id) {
    var windowHeight = window.innerHeight;
    var top = 0;
    var element = document.getElementById(id);
    while (element != undefined) {
        top += element.offsetTop || 0;
        element = element.offsetParent;
    }
    return windowHeight - top - 10;
}

ViewUtils.addTooltip = function(id, customData) {
    if (customData.tooltip != undefined) {
        $("#" + id).prop("title", customData.tooltip);
    }
}

ViewUtils.commonViewSchema = {
    "properties": {
        "flex": {
            "oneOf": [
                {
                    "description": "A number that is used to determine the view's width / height in aligners as a proportion",
                    "type": "number"
                },
                {
                    "description": "A number that is used to determine the view's width / height in aligners in pixels",
                    "type": "string",
                    "pattern": "[0-9]+px"
                }
            ]
        },
        "css": {
            "description": 'Css rules for the current div. Example: {"height": "500px"}',
            "type": "object",
            "properties": {},
            "additionalProperties": true
        },
        "processCss": {
            "description": 'Apply the css rules to the css selectors given in the object\'s ids. Handle with care, as the given css is applied to all matched elements below the current element (even in the subviews). Example: {"DIV > LABEL": {"color": "red"}}',
            "type": "object",
            "properties": {},
            "additionalProperties": true
        }
    }
};

ViewUtils.commonElementSchema = {
    "properties": {
        "isElementLabelPresent": {
            "type": "boolean",
            "format": "checkbox",
            "default": true,
            "description": "show label above the element"
        },
        "elementText": {
            "type": "string",
            "description": "the text of the label above the element (if undefined, the request element will be used)"
        },
        "tooltip": {
            "type": "string",
            "description": "the tooltip text shown when hovering over the element"
        },
        "disabled": {
            "type": "boolean",
            "format": "checkbox",
            "description": "whether the view element is disabled, i.e. not writable",
            "default": true
        }
    }
};
