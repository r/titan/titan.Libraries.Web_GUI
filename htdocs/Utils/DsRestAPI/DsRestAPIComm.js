// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CDsRestAPIComm(p_extension) {
    "use strict";
    
    var mThis = this;
    var mRequestsDisabled = false;
    var mTimeLastRequestFailed = 0;
    var mNoAnswer = [{"node":{"val":"No answer", "tp":3}}];
    
    // disable cache (mainly for IE, it works all right with firefox and chrome)
    $.ajaxSetup({cache: false});
    
    /* This gives a more sane initial value than being zero. */
    var start = Date.now();
    var ajaxCallURL = "api.dsapi";
    if (p_extension != undefined) {
        ajaxCallURL = "api." + p_extension;
    }
    
    /** public functions */
    this.ajaxCall = function(aData, aHandler) {
        if (mRequestsDisabled) {
            aData = {"requests": [], "timeOut": 5.0};
        }
        
        var end = Date.now();
        $('#cll_DsRestAPI_FPS').html((1000 / (end - start)).toFixed(2)); // diff to previous ajaxCall's end.
        start = Date.now();
        $.ajax({
            url: ajaxCallURL,
            type: 'POST',
            data: JSON.stringify(aData),
            //contentType: 'application/json',
            //accepts: 'application/json',
            dataType: 'text',
            cache: false,
            success: function(data, textStatus, jqXHR) {
                var end = Date.now();
                if (mRequestsDisabled) {
                    mRequestsDisabled = false;
                    // if we were disconnected for more than 15 secs, reload the page
                    if (end - mTimeLastRequestFailed > 15000) {
                        location.reload();
                    } else {
                        aHandler(mNoAnswer);
                    }
                } else {
                    $('#cll_DsRestAPI_serverTime').html(parseFloat(jqXHR.getResponseHeader("X-EPTF-CLL-ServerTime")).toFixed(2));
                    $('#cll_DsRestAPI_roundtrip').html(end - start);
                    $('#cll_DsRestAPI_dataSize').html(data.length);
                    $('#cll_DsRestAPI_error').html("");
                    $('#cll_DsRestAPI_error').addClass("hidden");
                    if (data && data !== "" && data !== " ")
                    {
                        var contentlist;
                        try {
                            contentlist = JSON.parse(data).contentList;
                        } catch (e) {
                            mThis.handleError("Badly encoded response received from server", e.message, aHandler, end, start);
                            return;
                        }
                        if (contentlist.length == 1 && contentlist[0].node != undefined && contentlist[0].node.val == "timeout")
                        {
                            mThis.handleError("Timeout received from server", "Overload", aHandler, end, start);
                        }
                        else
                            aHandler(contentlist);
                    }
                    else
                        aHandler(mNoAnswer);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                mThis.handleError("UI has been disconnected", textStatus, aHandler, end, start);
            },
            timeout: 10000
        });
    };

    this.handleError = function(aState, aError, aHandler, aEnd, aStart)
    {
        if (!mRequestsDisabled) {
            mRequestsDisabled = true;
            mTimeLastRequestFailed = Date.now();
        }
        $('#cll_DsRestAPI_roundtrip').html(aEnd - aStart);
        $('#cll_DsRestAPI_error').html("State: " + aState + " <br/>Error: " + aError + "<br/>Action: reconnecting");
        $('#cll_DsRestAPI_error').removeClass("hidden");
        aHandler(mNoAnswer);
    };
}
