// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function FileLoader(p_url, p_fileHandler, p_data) {
    "use strict";
    var url = p_url;
    var data = "";
    if (p_data != undefined) {
        data = p_data;
    }
    
    var v_fileHandler = p_fileHandler;
    
    this.taskOperation = function(callback) {
        function dataArrived(ok, p_data) {
            if (ok) {
                data = p_data;
            }
            callback(ok, data);
        }
        
        v_fileHandler.loadFile(url, dataArrived);
    };
    
    this.save = function(p_content, p_url, p_callback) {
        if (p_content != undefined) {
            data = p_content;
        }
        
        var oldUrl = url;
        if (p_url != undefined) {
            oldUrl = url;
            url = p_url;
        }
        
        function callback(ok) {
            if (!ok) {
                url = oldUrl;
            }
            if (p_callback != undefined) {
                p_callback(ok);
            }
        }
        
        if (url != undefined) {
            v_fileHandler.putFile(url, data, callback);
        } else {
            callback(false);
        }
        
    };
    
    this.getData = function() {
        return data;
    };
    
    this.setData = function(p_data) {
        data = p_data;
    };
    
    this.getUrl = function() {
        return url;
    };
}