// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function JsonLoader(jsonFile, p_fileHandler, defaultValue) {
    "use strict";
    var url = jsonFile;
    
    var data = defaultValue;
    var v_fileHandler = p_fileHandler;

    this.taskOperation = function(callback) {
        function dataArrived(ok, p_data) {
            var error = false;
            if (ok) {
                try {
                    data = JSON.parse(p_data);
                } catch (e) {
                    error = "Failed to parse JSON data: " + p_data;
                }
            } else {
                error = "Failed to load " + url;
            }
            if (!error)
                callback(true, data);
            else
                callback(false, error);
        }
        
        v_fileHandler.loadFile(url, dataArrived);
    };
    
    this.save = function(p_url, p_callback) {
        var oldUrl = url;
        if (p_url != undefined) {
            oldUrl = url;
            url = p_url;
        }
        
        function callback(ok) {
            if (!ok) {
                url = oldUrl;
            }
            if (p_callback != undefined) {
                p_callback(ok);
            }
        }
        
        if (url) {
            v_fileHandler.putFile(url, JSON.stringify(data, null, 4), callback);
        } else {
            callback(false);
        }
        
    };
    
    this.getData = function() {
        return data;
    };
    
    this.setData = function(p_data) {
        data = p_data;
    };
    
    this.getUrl = function() {
        return url;
    };
}