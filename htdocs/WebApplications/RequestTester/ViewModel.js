// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function RequestTester_ViewModel(p_fileHandler, p_DsRestAPI) {
    "use strict";

    var v_DsRestAPI = p_DsRestAPI;
    var v_fileHandler = p_fileHandler;
    var v_help;
    
    var v_request;
    var v_response;
    var v_time = 0;
    
    var dataSourceUtils = new DataSourceUtils();
    
    var v_autogui_ViewModel = new CViewModel_AutoGUI({
        "getResponseElement" : function() {
            return dataSourceUtils.expandResponse(v_response, 0, [], [], v_request);
        },
        "getDsRestAPI": function() {
            return v_DsRestAPI;
        }
    });
    
    var v_this = this;
    
    this.init = function(p_callback) {
        function helpArrived(ok, help) {
            if (ok) {
                v_help = new HelpTreeBuilder(help).getHelpTree(true);
                p_callback(true);
            } else {
                p_callback(false, "Error getting help");
            }
        }
        
        v_DsRestAPI.getHelp(helpArrived);
    };
    
    this.setBinder = function(p_binder) {
        v_autogui_ViewModel.setBinder(p_binder);
    }
    
    this.getAutoGuiViewModel = function() {
        return v_autogui_ViewModel;
    }
    
    this.sendRequest = function(refresh, request) {
        if (request) {
            v_request = request;
        }
        
        function responseArrived(response) {
            v_time = Date.now() - v_time;
            v_response = response;
            refresh();
        }
        
        v_time = Date.now();
        v_DsRestAPI.getList(v_request, responseArrived);
    };

    this.getHelpTree = function() {
        return dataSourceUtils.convertHelpToTreeDataArray(v_help.sources);
    };
    
    this.getHelp = function() {
        return v_help;
    }
    
    this.loadFile = v_fileHandler.loadFile;
    this.loadCss = v_fileHandler.loadCss;
    
    this.getRequest = function() {
        return v_request;
    };
    
    this.getResponse = function() {
        return v_response;
    };
    
    this.getTime = function() {
        return "Time: " + v_time + " ms.";
    };
}
//# sourceURL=RequestTester\ViewModel.js
