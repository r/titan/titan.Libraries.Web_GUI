// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_Model(p_webAppModel, p_framework, p_extension) {
    "use strict";

    var v_baseModel = p_webAppModel;
    var v_setupModel = v_baseModel.getSetupModel();
    var v_framework = p_framework;
    var v_dsRestAPI;
    var v_MetaSchema;

    if (v_baseModel.getAppConfig().lastEditedApp != undefined && window["DsRestAPI"] != undefined) {
        v_dsRestAPI = new DsRestAPI(p_extension);
    } else {
        v_dsRestAPI = {"getHelp": function(callback) {callback(true, {"sources" : []});}};
    }

    v_baseModel.getFileHandler().loadFile('Libs/AJV/json-schema-draft-04.json', function(ok, data) {
        v_MetaSchema = JSON.parse(data);
    });

    ///////////////////// SETUP HANDLING /////////////////////

    this.newSetup = function() {
        v_setupModel.newSetup();
        changeLastEdited(undefined);
        var setup = v_setupModel.getSetup();
        fillDesktopSettings(setup);
        return setup;
    };

    this.deleteSetup = function(name, callback) {
        function setupDeleted(ok) {
            var config = v_baseModel.getAppConfig();
            if (ok && config.lastEditedSetup == name) {
                changeLastEdited(undefined);
            }
            callback(ok)
        }
        v_setupModel.deleteSetup(name, setupDeleted);
    };

    this.switchSetup = function(p_setupName, p_setupLoaded) {
        function setupLoaded(ok, setup, setupName) {
            fillDesktopSettings(setup);
            if (ok) {
                changeLastEdited(setupName);
            }
            p_setupLoaded(ok, setup, setupName);
        }
        v_setupModel.loadSetup(p_setupName, setupLoaded, true);
    };

    this.saveSetup = v_setupModel.saveSetup;

    this.saveSetupAs = function(setupName, callback) {
        function setupSaved(ok) {
            if (ok) {
                changeLastEdited(setupName);
            }
            callback(ok);
        }

        v_setupModel.saveSetupAs(setupName, setupSaved);
    };

    this.listSetups = v_setupModel.listSetups;

    this.setupExists = function(setupName, callback) {
        v_setupModel.setupExists(setupName, callback);
    };

    function fillDesktopSettings(setup) {
        var desktopData = setup.desktop.getData();
        if (desktopData["ViewmodelEditors"] == undefined) {
            desktopData["ViewmodelEditors"] = [];
        }
        if (desktopData["ViewEditors"] == undefined) {
            desktopData["ViewEditors"] = [];
        }
        if (desktopData["Imports"] == undefined) {
            desktopData["Imports"] = [];
        }
        if (desktopData["HtmlEditor"] == undefined) {
            desktopData["HtmlEditor"] = {};
        }
        if (desktopData["RequestEditor"] == undefined) {
            desktopData["RequestEditor"] = {};
        }
    }

    function changeLastEdited(lastEdited) {
        var config = v_baseModel.getAppConfig();
        config.lastEditedSetup = lastEdited;
        v_baseModel.saveAppConfig(config);

        if (lastEdited != undefined) {
            v_framework.setAppParam("DsRestAPI Console", "customization", config.lastEditedApp);
            v_framework.setAppParam("DsRestAPI Console", "setup", config.lastEditedSetup);

            var mainConfig = v_baseModel.getMainConfig();
            for (var i = 0; i < mainConfig.availableApps.length; ++i) {
                if (mainConfig.availableApps[i].directory == "WebApplications/CustomizableApp" && mainConfig.availableApps[i].params.customization == config.lastEditedApp) {
                    v_framework.setAppParam(mainConfig.availableApps[i].name, "setup", config.lastEditedSetup);
                }
            }
        }
    }

    this.globalSetupSearch = v_setupModel.globalSetupSearch;

    this.exportChartRequest = function(callback) {
        var exporter = new ChartRequestCreator(v_baseModel.getFileHandler(), v_baseModel.getAppConfig().lastEditedApp, callback);
        exporter.run();
    };

    this.getSetup = v_setupModel.getSetup;

    ///////////////////// EDITOR MODEL HANDLING /////////////////////

    this.createViewModelEditor_Model = function(className, modelCreated, customData) {
        if (customData == undefined) {
            customData = {};
        } else {
            customData = mcopy(customData);
        }
        var data = {
            "class": className,
            "customData": customData,
            "dataPathStrList": [],
            "dataPathList": [],
            "selectionToControlPathStrList": [],
            "selectionToControlPathList": []
        };
        var setup = v_setupModel.getSetup();
        setup.viewmodels.getData().push(data);
        var dekstopData = {};
        setup.desktop.getData()["ViewmodelEditors"].push(dekstopData);
        modelCreated(new GuiEditor_ViewModelEditor_Model(data), dekstopData);
    };

    this.deleteViewModelEditor_Model = function(index) {
        var setup = v_setupModel.getSetup();
        setup.viewmodels.getData().splice(index, 1);
        setup.desktop.getData()["ViewmodelEditors"].splice(index, 1);
    };

    this.createViewEditor_Model = function(className, modelCreated, customData) {
        if (customData == undefined) {
            customData = {};
        } else {
            customData = mcopy(customData);
        }
        var data = {
            "class": className,
            "customData": customData,
            "viewModelIndexes": []
        };
        var setup = v_setupModel.getSetup();
        setup.views.getData().push(data);
        var dekstopData = {};
        setup.desktop.getData()["ViewEditors"].push(dekstopData);
        modelCreated(new GuiEditor_ViewEditor_Model(data), dekstopData);
    };

    this.deleteViewEditor_Model = function(index) {
        var setup = v_setupModel.getSetup();
        v_setupModel.getSetup().views.getData().splice(index, 1);
        setup.desktop.getData()["ViewEditors"].splice(index, 1);
    };

    this.createImport_Model = function(setupName, modelCreated) {
        var data = {
            "setupName": setupName
        };
        var setup = v_setupModel.getSetup();
        setup.imports.getData().push(data);
        var dekstopData = {};
        setup.desktop.getData()["Imports"].push(dekstopData);
        modelCreated(new GuiEditor_Imports_Model(data), dekstopData);
    };

    this.deleteImport_Model = function(index) {
        var setup = v_setupModel.getSetup();
        v_setupModel.getSetup().imports.getData().splice(index, 1);
        setup.desktop.getData()["Imports"].splice(index, 1);
    };

    this.getEditorModels = function(modelsCreated) {
        var setup = v_setupModel.getSetup();
        var data = {
            "viewmodel": {
                "models": [],
                "desktopData": []
            },
            "view": {
                "models": [],
                "desktopData": []
            },
            "imports": {
                "models": [],
                "desktopData": []
            }
        }

        var viewmodelInstances = setup.viewmodels.getData();
        if (setup.desktop.getData()["ViewmodelEditors"] == undefined) {
            setup.desktop.getData()["ViewmodelEditors"] = [];
        }
        data.viewmodel.desktopData = setup.desktop.getData()["ViewmodelEditors"];
        for (var i = 0; i < viewmodelInstances.length; ++i) {
            data.viewmodel.models.push(new GuiEditor_ViewModelEditor_Model(viewmodelInstances[i]));
            if (data.viewmodel.desktopData[i] == undefined) {
                data.viewmodel.desktopData[i] = {};
            }
        }

        var viewInstances = setup.views.getData();
        if (setup.desktop.getData()["ViewEditors"] == undefined) {
            setup.desktop.getData()["ViewEditors"] = [];
        }
        data.view.desktopData = setup.desktop.getData()["ViewEditors"];
        for (var j = 0; j < viewInstances.length; ++j) {
            data.view.models.push(new GuiEditor_ViewEditor_Model(viewInstances[j]));
            if (data.view.desktopData[j] == undefined) {
                data.view.desktopData[j] = {};
            }
        }

        var imports = setup.imports.getData();
        if (setup.desktop.getData()["Imports"] == undefined) {
            setup.desktop.getData()["Imports"] = [];
        }
        data.imports.desktopData = setup.desktop.getData()["Imports"];
        for (var j = 0; j < imports.length; ++j) {
            data.imports.models.push(new GuiEditor_Imports_Model(imports[j]));
            if (data.imports.desktopData[j] == undefined) {
                data.imports.desktopData[j] = {};
            }
        }

        modelsCreated(data);
    };

    ///////////////////// RESOURCE HANDLING /////////////////////

    this.viewExists = function(viewName, callback) {
        function filesArrived(views) {
            callback(views.indexOf(viewName) != -1);
        }

        this.listCustomViews(filesArrived);
    };

    this.viewmodelExists = function(viewmodelName, callback) {
        function filesArrived(viewmodels) {
            callback(viewmodels.indexOf(viewmodelName) != -1);
        }

        this.listCustomViewmodels(filesArrived);
    };

    function listJavascriptResources(locations, callback) {
        new MultipleDirectoryListTask(locations, v_baseModel.getFileHandler()).taskOperation(function(ok, resources) {
            callback(resources.jsfiles);
        });
    }

    this.listViews = function(callback) {
        listJavascriptResources([
                "WebApplicationFramework/Views",
                "WebApplications/CustomizableApp/Views",
                v_baseModel.getAppConfig().lastEditedApp + "/Views"
            ], callback
        );
    };

    this.listCustomViews = function(callback) {
        function filesArrived(views) {
            var result = [];
            for (var i = 0; i < views.length; ++i) {
                var fileName = views[i].fileName;
                if (fileName.endsWith(".js") || fileName.endsWith(".css") || fileName.endsWith(".html")) {
                    result.push(fileName.substring(fileName.lastIndexOf("/") + 1, fileName.lastIndexOf(".")));
                }
            }
            result = uniq(result);
            result.sort();
            callback(result);
        }
        v_baseModel.getFileHandler().getDirectory(v_baseModel.getAppConfig().lastEditedApp + "/Views", filesArrived);
    };

    this.listViewmodels = function(callback) {
        listJavascriptResources([
                "WebApplicationFramework/ViewModels",
                "WebApplications/CustomizableApp/ViewModels",
                v_baseModel.getAppConfig().lastEditedApp + "/ViewModels"
            ], callback
        );
    };

    this.listCustomViewmodels = function(callback) {
        function filesArrived(viewmodels) {
            var result = [];
            for (var i = 0; i < viewmodels.length; ++i) {
                var fileName = viewmodels[i].fileName;
                if (fileName.endsWith(".js")) {
                    result.push(fileName.substring(fileName.lastIndexOf("/") + 1));
                }
            }
            result.sort();
            callback(result);
        }
        v_baseModel.getFileHandler().getDirectory(v_baseModel.getAppConfig().lastEditedApp + "/ViewModels", filesArrived);
    };

    this.getViewUrl = function(name) {
        return v_baseModel.getAppConfig().lastEditedApp + "/Views/" + name;
    };

    this.getViewmodelUrl = function(name) {
        return v_baseModel.getAppConfig().lastEditedApp + "/ViewModels/" + name;
    };

    this.deleteFile = function(file, callback) {
        v_baseModel.getFileHandler().delDirectory(file, callback);
    };

    ///////////////////// CONFIG HANDLING /////////////////////

    this.getAppConfig = v_baseModel.getAppConfig;

    this.setEditedApp = function(app, callback) {
        var config = v_baseModel.getAppConfig();
        config.lastEditedApp = app;
        v_setupModel.setSetupDirectory(app + "/Setups");
        v_baseModel.saveAppConfig();
        new JsImportFromConfigTask(app + '/AppConfig.json', v_baseModel.getFileHandler()).taskOperation(function(ok, extension) {
            if (window["DsRestAPI"] != undefined) {
                v_dsRestAPI = new DsRestAPI(extension);
            }
            callback(true);
        });
    };

    this.listEditableApps = function(callback) {
        var result = [];
        var mainConfig = v_baseModel.getMainConfig();
        for (var i = 0; i < mainConfig.availableApps.length; ++i) {
            if (mainConfig.availableApps[i].directory == "WebApplications/CustomizableApp" && mainConfig.availableApps[i].params.customization != undefined) {
                result.push(mainConfig.availableApps[i].params.customization);
            }
        }
        callback(result);
    };

    this.listEditableConfigs = function(callback) {
        var result = [{
            "config": "CustomizableContent/MainConfig.json",
            "schema": "CustomizableContent/MainConfigSchema.json"
        }];
        function filesArrived(data) {
            for (var i = 0; i < data.length; ++i) {
                if (data[i].contentType.endsWith("d")) {
                    result.push({
                        "config": data[i].fileName + "/AppConfig.json",
                        "schema": data[i].fileName + "/AppConfigSchema.json"
                    });
                }
            }
            callback(result);
        }
        v_baseModel.getFileHandler().getDirectory("CustomizableContent", filesArrived);
    };

    ///////////////////// USEFUL FUNCTIONS FOR VIEWMODELS /////////////////////

    this.getDesktopDataForRequestEditor = function() {
        return v_setupModel.getSetup().desktop.getData()["RequestEditor"];
    };

    function compareOptions(option1, option2) {
        if (option1.text < option2.text) {
            return -1;
        } else if (option1.text > option2.text) {
            return 1;
        } else {
            return 0;
        }
    }

    this.sortOptions = function(options) {
        options.sort(compareOptions);
    };

    this.getDsRestAPI = function() {
        return v_dsRestAPI;
    };

    this.getMetaSchema = function() {
        return v_MetaSchema;
    }

    this.getFileHandler = v_baseModel.getFileHandler;
}

function ChartRequestCreator(p_fileHandler, p_appDirectory, p_callback) {
    var v_fileHandler = p_fileHandler;
    var v_callback = p_callback;

    // setup name -> setup params or null
    var v_setupsToCheckForCharts = {};
    var v_setupsImported = {};
    var v_showOtherSetups = false;
    var v_setupList;

    var v_chartRequest = [];

    var v_setupLoader = new CSetup_Model(v_fileHandler);
    v_setupLoader.setSetupDirectory(p_appDirectory + "/Setups");
    var v_chartSaver = new JsonLoader(p_appDirectory + "/TimelineRequest.json", v_fileHandler, v_chartRequest);

    this.run = function() {
        function setupsListed(setups) {
            v_setupList = setups;
            var tasks = [];
            for (var i = 0; i < setups.length; ++i) {
                tasks.push(new CheckSetupForSetupTabsTask(setups[i]));
            }
            var taskList = new SyncTaskList(tasks, setupsCollected);
            taskList.taskOperation();
        }

        v_setupLoader.listSetups(setupsListed);
    };

    function CheckSetupForSetupTabsTask(p_setupName) {
        var v_setupName = p_setupName;
        this.taskOperation = function(callback) {
            function setupLoaded(ok, setup, setupName) {
                if (ok) {
                    checkSetupForSetupTabs(setup.viewmodels.getData(), setup.imports.getData());
                    callback(true);
                } else {
                    callback(false, "Could not load setup: " + setupName);
                }
            }

            v_setupLoader.loadSetup(v_setupName, setupLoaded, true);
        }
    }

    function checkSetupForSetupTabs(viewmodels, imports) {
        for (var i = 0; i < viewmodels.length; ++i) {
            if (viewmodels[i].class == "CViewModel_SetupTabs" && viewmodels[i].customData.hierarchy != undefined) {
                collectSetups(viewmodels[i].customData.hierarchy.menuElements);
            }
            if (viewmodels[i].class == "CViewModel_SetupTabs" && viewmodels[i].customData.displayOtherSetups == true) {
                v_showOtherSetups = true;
            }
        }

        for (var i = 0; i < imports.length; ++i) {
            v_setupsImported[imports[i].setupName] = true;
        }
    }

    function collectSetups(menuElements) {
        if (menuElements != undefined) {
            for (var i = 0; i < menuElements.length; ++i) {
                if (menuElements[i].setupName != undefined) {
                    if (menuElements[i].setupParams != undefined) {
                        v_setupsToCheckForCharts[menuElements[i].setupName] = menuElements[i].setupParams;
                    } else {
                        v_setupsToCheckForCharts[menuElements[i].setupName] = null;
                    }
                }
                collectSetups(menuElements[i].menuElements);
            }
        }
    }

    function setupsCollected() {
        if (v_showOtherSetups) {
            for (var i = 0; i < v_setupList.length; ++i) {
                if (!v_setupsImported.hasOwnProperty(v_setupList[i]) && !v_setupsToCheckForCharts.hasOwnProperty(v_setupList[i])) {
                    v_setupsToCheckForCharts[v_setupList[i]] = null;
                }
            }
        }

        var tasks = [];
        for (var setup in v_setupsToCheckForCharts) {
            tasks.push(new FindChartsTask(setup, v_setupsToCheckForCharts[setup]));
        }
        var taskList = new SyncTaskList(tasks, chartsFound);
        taskList.taskOperation();
    }

    function FindChartsTask(p_setupName, p_setupParams) {
        var v_setupName = p_setupName;
        var v_setupParams = p_setupParams;
        this.taskOperation = function(callback) {
            function setupLoaded(ok, setup, setupName) {
                if (ok) {
                    findCharts(setup.viewmodels.getData(), setup.request.getData(), setup.request.getData(), []);
                    callback(true);
                } else {
                    callback(false, "Could not load setup: " + setupName);
                }
            }

            v_setupLoader.loadSetup(v_setupName, setupLoaded, false, true);
        }
    }

    function findCharts(viewmodels, request, requests, path) {
        if (requests != undefined) {
            for (var i = 0; i < requests.length; ++i) {
                path.push(i);
                cleanRequestUp(requests[i], viewmodels, path);
                if (requests[i].getData.timeline != undefined) {
                    addChartRequest(mcopy(request), path);
                }

                findCharts(viewmodels, request, requests[i].getData.children, path);
                path.pop();
            }
        }
    }

    function cleanRequestUp(request, viewmodels, path) {
        request.getData.filter = undefined;
        request.getData.rangeFilter = undefined;

        var selectionIsChanged = false;
        for (var i = 0; i < viewmodels.length; ++i) {
            for (var j = 0; j < viewmodels[i].selectionToControlPathList.length; ++j) {
                if (pathEquals(path, viewmodels[i].selectionToControlPathList[j])) {
                    selectionIsChanged = true;
                }
            }
        }

        if (selectionIsChanged) {
            request.getData.selection = undefined;
        }
    }

    function pathEquals(path1, path2) {
        if (path1.length == path2.length) {
            for (var i = 0; i < path1.length; ++i) {
                if (path1[i] != path2[i]) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    function addChartRequest(request, path) {
        var requestList = [request.splice(path[0], 1)[0]];
        var currentRequest = requestList[0];
        for (var i = 1; i < path.length; ++i) {
            requestList[i] = currentRequest.getData.children.splice(path[i], 1)[0];
            currentRequest = requestList[i];
        }
        for (var i = 0; i < requestList.length - 1; ++i) {
            requestList[i].getData.children = [];
        }

        v_chartRequest.push(createRequestFromList(requestList));
    }

    function createRequestFromList(requestList) {
        var PATTERN = /%Parent(\d+)%/g;

        var used = [];
        var toCheck = [requestList.length - 1];

        while (toCheck.length > 0) {
            var index = toCheck.shift();
            used[index] = true;

            var pattern = new RegExp(PATTERN);
            var str = JSON.stringify(requestList[index]);
            var match = pattern.exec(str);
            while (match != undefined) {
                var parentid = parseInt(match[1], 10);
                if (parentid < index) {
                    toCheck.push(parentid);
                }
                match = pattern.exec(str);
            }
        }

        var newRequest;
        var prev;
        var k = 0;
        for (var i = 0; i < used.length; ++i) {
            if (used[i] != undefined) {
                for (var j = i + 1; j < requestList.length; ++j) {
                    var string = JSON.stringify(requestList[j]);
                    string = string.replace("%Parent" + i + "::idx%", "%Parent" + k + "::idx%");
                    string = string.replace("%Parent" + i + "%", "%Parent" + k + "%");
                    requestList[j] = JSON.parse(string);
                }

                if (prev == undefined) {
                    newRequest = requestList[i];
                    prev = newRequest;
                } else {
                    prev.getData.children = [requestList[i]];
                }
                prev = requestList[i];
                ++k;
            }
        }

        return newRequest;
    }

    function chartsFound() {
        v_chartSaver.save();
        v_callback();
    }
}

//# sourceURL=GuiEditor\Models\Model.js
