// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_ViewEditor_Model(parsedData) {
    "use strict";

    var v_viewInstance = parsedData;

    if (v_viewInstance.viewModelIndexes == undefined) {
        v_viewInstance.viewModelIndexes = [];
    }

    if (v_viewInstance.idsCreating == undefined) {
        v_viewInstance.idsCreating = [];
    }

    this.addViewModelIndex = function(p_viewModelIndex, p_index) {
        v_viewInstance.viewModelIndexes.splice(p_index, 0, p_viewModelIndex);
    };

    this.deleteViewModelIndexFromPosition = function(p_index) {
        v_viewInstance.viewModelIndexes.splice(p_index, 1);
    };

    this.viewModelDeleted = function(p_viewModelIndex) {
        var i = 0;
        while (i < v_viewInstance.viewModelIndexes.length) {
            if (v_viewInstance.viewModelIndexes[i] === p_viewModelIndex) {
                this.deleteViewModelIndexFromPosition(i);
            } else if (v_viewInstance.viewModelIndexes[i] > p_viewModelIndex) {
                --v_viewInstance.viewModelIndexes[i];
                ++i;
            } else {
                ++i;
            }
        }
    };

    this.connectionOrderChanged = function(fromIndex, toIndex) {
        var index = v_viewInstance.viewModelIndexes.splice(fromIndex, 1)[0];
        v_viewInstance.viewModelIndexes.splice(toIndex, 0, index);
    };

    this.getViewModelIndexes = function() {
        return v_viewInstance.viewModelIndexes;
    };

    this.getCustomData = function() {
        return v_viewInstance.customData;
    };

    this.setCustomData = function(data) {
        v_viewInstance.customData = data;
    };

    this.getClassName = function() {
        return v_viewInstance["class"];
    };

    this.setClass = function(p_class) {
        v_viewInstance["class"] = p_class;
    };

    this.getParentId = function() {
        return v_viewInstance.parentID;
    }

    this.setParentId = function(p_id) {
        v_viewInstance.parentID = p_id;
    };

    this.addChildView = function(p_id) {
        v_viewInstance.idsCreating.push(p_id);
    };

    this.removeChildView = function() {
        v_viewInstance.idsCreating.pop();
    };

    this.renameChildId = function(p_index, p_to) {
        v_viewInstance.idsCreating[p_index] = p_to;
    };

    this.getChildIds = function() {
        return v_viewInstance.idsCreating;
    };

    this.getDescriptorCopy = function() {
        return mcopy(v_viewInstance);
    };
}
//# sourceURL=GuiEditor\Models\Model_ViewEditor.js
