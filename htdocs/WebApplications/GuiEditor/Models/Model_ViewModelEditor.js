// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_ViewModelEditor_Model(parsedData) {
    "use strict";

    var v_viewModelInstance = parsedData;
    var v_this = this;

    this.addDataPath = function(strpath, path, index) {
        v_viewModelInstance.dataPathStrList.splice(index, 0, strpath);
        v_viewModelInstance.dataPathList.splice(index, 0, path);
    };

    this.deleteDataPath = function(index) {
        v_viewModelInstance.dataPathStrList.splice(index, 1);
        v_viewModelInstance.dataPathList.splice(index, 1);
    };

    this.moveDataPath = function(fromIndex, toIndex) {
        var path = v_viewModelInstance.dataPathList.splice(fromIndex, 1)[0];
        v_viewModelInstance.dataPathList.splice(toIndex, 0, path);
        var strpath = v_viewModelInstance.dataPathStrList.splice(fromIndex, 1)[0];
        v_viewModelInstance.dataPathStrList.splice(toIndex, 0, strpath);
    };

    this.addSelectionPath = function(strpath, path, index) {
        v_viewModelInstance.selectionToControlPathStrList.splice(index, 0, strpath);
        v_viewModelInstance.selectionToControlPathList.splice(index, 0, path);
    };

    this.deleteSelectionPath = function(index) {
        v_viewModelInstance.selectionToControlPathStrList.splice(index, 1);
        v_viewModelInstance.selectionToControlPathList.splice(index, 1);
    };

    this.moveSelectionPath = function(fromIndex, toIndex) {
        var path = v_viewModelInstance.selectionToControlPathList.splice(fromIndex, 1)[0];
        v_viewModelInstance.selectionToControlPathList.splice(toIndex, 0, path);
        var strpath = v_viewModelInstance.selectionToControlPathStrList.splice(fromIndex, 1)[0];
        v_viewModelInstance.selectionToControlPathStrList.splice(toIndex, 0, strpath);
    };

    this.deleteConnectionsWithPrefix = function(prefix) {
        var deleted = [];
        deleteConnectionsWithPrefixFromList(v_viewModelInstance.dataPathList, prefix, this.deleteDataPath, deleted, "data");
        deleteConnectionsWithPrefixFromList(v_viewModelInstance.selectionToControlPathList, prefix, this.deleteSelectionPath, deleted, "selection");
        return deleted;
    };

    function deleteConnectionsWithPrefixFromList(list, prefix, deleteFunction, listOfDeleted, type) {
        var i = 0;
        while (i < list.length) {
            if (hasPrefix(list[i], prefix)) {
                deleteFunction(i);
                listOfDeleted.unshift({
                    "type": type,
                    "index": i
                });
            } else {
                ++i;
            }
        }
    }

    this.updateConnections = function(prefix, amount) {
        var pos = prefix.pop();
        updateConnectionsInList(v_viewModelInstance.dataPathList, prefix, pos, amount);
        updateConnectionsInList(v_viewModelInstance.selectionToControlPathList, prefix, pos, amount);
    };

    function updateConnectionsInList(list, prefix, pos, amount) {
        for (var i = 0; i < list.length; ++i) {
            updateConnection(list[i], prefix, pos, amount);
        }
    }

    function updateConnection(connection, prefix, pos, amount) {
        if (hasPrefix(connection, prefix) && connection[prefix.length] != undefined && connection[prefix.length] >= pos) {
            connection[prefix.length] += amount;
        }
    }

    this.prefixChanged = function(fromPrefix, fromPrefixStr, toPrefix, toPrefixStr) {
        var origSiblingPrefix = mcopy(fromPrefix);
        var origPosition = origSiblingPrefix.pop();

        // This is the trick... we have to update the toPrefix, since we removed the node in fromPrefix
        updateConnection(toPrefix, origSiblingPrefix, origPosition, -1);

        var newSiblingPrefix = mcopy(toPrefix);
        var newPosition = newSiblingPrefix.pop();

        prefixChangedInList(v_viewModelInstance.dataPathList, v_viewModelInstance.dataPathStrList, fromPrefix, fromPrefixStr, toPrefix, toPrefixStr, origSiblingPrefix, origPosition, newSiblingPrefix, newPosition);
        prefixChangedInList(v_viewModelInstance.selectionToControlPathList, v_viewModelInstance.selectionToControlPathStrList, fromPrefix, fromPrefixStr, toPrefix, toPrefixStr, origSiblingPrefix, origPosition, newSiblingPrefix, newPosition);
    };

    function prefixChangedInList(list, strlist, fromPrefix, fromPrefixStr, toPrefix, toPrefixStr, origSiblingPrefix, origPosition, newSiblingPrefix, newPosition) {
        for (var i = 0; i < list.length; ++i) {
            var connection = list[i];
            // This is simple: we change the fromPrefix to toPrefix
            // or update the prefix: a node was deleted and a new node was added, we already know how to update it...
            if (hasPrefix(connection, fromPrefix)) {
                replacePrefix(list, strlist, i, fromPrefix, fromPrefixStr, toPrefix, toPrefixStr);
            } else {
                updateConnection(connection, origSiblingPrefix, origPosition, -1);
                updateConnection(connection, newSiblingPrefix, newPosition, 1);
            }
        }
    }

    function replacePrefix(list, strlist, index, fromPrefix, fromPrefixStr, toPrefix, toPrefixStr) {
        var connection = list[index];
        connection.splice(0, fromPrefix.length);
        for (var i = toPrefix.length - 1; i >= 0; --i) {
            connection.unshift(toPrefix[i]);
        }

        var connectionStrSplit = strlist[index].split(".");
        var fromPrefixStrSplit = fromPrefixStr.split(".");
        fromPrefixStrSplit.pop();
        var toPrefixStrSplit = toPrefixStr.split(".");
        connectionStrSplit.splice(0, fromPrefixStrSplit.length);
        for (var i = toPrefixStrSplit.length - 1; i >= 0; --i) {
            connectionStrSplit.unshift(toPrefixStrSplit[i]);
        }
        var newRepresentation = joinStringAtDot(connectionStrSplit);
        if (newRepresentation[0] === ".") {
            newRepresentation = newRepresentation.substring(1);
        }
        strlist[index] = newRepresentation;
    }

    function joinStringAtDot(splitString) {
        if (splitString.length === 0) return "";
        var joined = splitString[0];
        for (var i = 1; i < splitString.length; ++i) {
            joined += "." + splitString[i];
        }
        return joined;
    }

    this.getDataConnections = function() {
        return v_viewModelInstance.dataPathList;
    };

    this.getDataConnectionsStr = function() {
        return v_viewModelInstance.dataPathStrList;
    };

    this.getSelectionConnections = function() {
        return v_viewModelInstance.selectionToControlPathList;
    };

    this.getSelectionConnectionsStr = function() {
        return v_viewModelInstance.selectionToControlPathStrList;
    };

    this.getCustomData = function() {
        return v_viewModelInstance.customData;
    };

    this.setCustomData = function(data) {
        v_viewModelInstance.customData = data;
    };

    this.getClassName = function() {
        return v_viewModelInstance["class"];
    };

    this.setClass = function(p_class, p_callback) {
        v_viewModelInstance["class"] = p_class;
    };

    this.requestRenamed = function(p_path, name) {
        var changedData = [];
        for (var i = 0; i < v_viewModelInstance.dataPathList.length; ++i) {
            if (hasPrefix(v_viewModelInstance.dataPathList[i], p_path)) {
                renameStringRepresentation(v_viewModelInstance.dataPathStrList, i, p_path.length - 1, name);
                if (v_viewModelInstance.dataPathList[i].length === p_path.length) {
                    changedData.push(i);
                }
            }
        }

        var changedSelection = [];
        for (var i = 0; i < v_viewModelInstance.selectionToControlPathList.length; ++i) {
            if (hasPrefix(v_viewModelInstance.selectionToControlPathList[i], p_path)) {
                renameStringRepresentation(v_viewModelInstance.selectionToControlPathStrList, i, p_path.length - 1, name);
                if (v_viewModelInstance.selectionToControlPathList[i].length === p_path.length) {
                    changedSelection.push(i);
                }
            }
        }

        return {"data": changedData, "selection": changedSelection};
    };

    this.getDescriptorCopy = function() {
        return mcopy(v_viewModelInstance);
    };

    function renameStringRepresentation(list, index, toChangeIndex, name) {
        var splitString = list[index].split(".");
        splitString[toChangeIndex] = name;
        var newRepresentation = joinStringAtDot(splitString);
        list[index] = newRepresentation;
    }
}
//# sourceURL=GuiEditor\Models\Model_ViewModelEditor.js
