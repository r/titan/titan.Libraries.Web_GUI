// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_Connections_View(p_id, p_parent) {
    "use strict";

    var v_id = p_id;
    var v_parent = p_parent;

    var v_connections = [];
    var v_counter = 0;

    var v_this = this;

    this.destroy = function() {
        for (var i = 0; i < v_connections.length; ++i) {
            if (v_connections[i].connection != undefined) {
                v_connections[i].connection.remove();
            }
        }

        v_connections = [];
        v_counter = 0;
    };

    this.fullRebuild = function() {
        v_this.destroy();
        var objects = v_parent.getObjectsToConnect();
        for (var i = 0; i < objects.length; ++i) {
            var endpoints = objects[i].getEndpoints();
            for (var j = 0; j < endpoints.length; ++j) {
                var endpoint = endpoints[j];
                v_this.addEndpoint(endpoint);
            }
        }
    };

    this.addEndpoint = function(endpoint) {
        var obj = {};
        v_connections.push(obj);

        var endpointType = endpoint.endpointType;
        obj[endpointType] = endpoint;
        var connectionType = endpoint.connectionType;
        obj["connectionType"] = connectionType;

        if (endpointType == "source") {
            obj["target"] = v_parent.getEndpoint(connectionType, endpoint.identifier);
        } else {
            obj["source"] = v_parent.getEndpoint(connectionType, endpoint.identifier);
        }

        obj["options"] = endpoint["options"];
    };

    this.deleteEndpoint = function(endpoint) {
        var i = 0;
        while (i < v_connections.length) {
            if (v_connections[i]["source"] == endpoint || v_connections[i]["target"] == endpoint) {
                var connection = v_connections.splice(i, 1)[0];
                if (connection.connection != undefined) {
                    connection.connection.remove();
                }
            } else {
                ++i;
            }
        }
    };

    this.deleteConnectionsByObject = function(obj) {
        var i = 0;
        while (i < v_connections.length) {
            var found = false;
            if (v_connections[i]["source"] != undefined && v_connections[i]["source"]["object"] == obj) {
                found = true;
            } else if (v_connections[i]["target"] != undefined && v_connections[i]["target"]["object"] == obj) {
                found = true;
            }

            if (found) {
                var connection = v_connections.splice(i, 1)[0];
                if (connection.connection != undefined) {
                    connection.connection.remove();
                }
            } else {
                ++i;
            }
        }
    };

    this.refreshConnections = function(obj) {
        for (var i = 0; i < v_connections.length; ++i) {
            if (obj != undefined && v_connections[i]["source"] != undefined && v_connections[i]["source"]["object"] != obj && v_connections[i]["target"] != undefined && v_connections[i]["target"]["object"] != obj) {
                continue;
            }

            if (v_connections[i].connection != undefined) {
                v_connections[i].connection.update();
            } else {
                v_connections[i].connection = new LineDrawer(
                    v_id,
                    v_id + "_svg_" + v_counter,
                    v_connections[i]["source"],
                    v_connections[i]["target"],
                    v_connections[i]["options"]
                );
                v_connections[i].connection.init();
                ++v_counter;
            }
        }
    };

    this.showOnlyThis = function(object) {
        v_parent.allDisabled(true);
        object.disabled(false);
        enable(object, "target", "source");
        enable(object, "source", "target");
        v_this.refreshConnections();
    };

    function enable(p_object, p_endpointTypeOfObject, p_endpointTypeToFind) {
        for (var i = 0; i < v_connections.length; ++i) {
            if (v_connections[i][p_endpointTypeOfObject] != undefined && v_connections[i][p_endpointTypeOfObject].object == p_object && v_connections[i][p_endpointTypeToFind] != undefined) {
                var object = v_connections[i][p_endpointTypeToFind].object;
                if (object.disabled(false)) {
                    // we found a parallel path (or very unlikely a cycle)
                } else {
                    enable(object, p_endpointTypeOfObject, p_endpointTypeToFind);
                }
            }
        }
    }
}
//# sourceURL=GuiEditor\Views\View_Connections.js