// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_FilterElementEditor_View(p_viewmodel, p_parentId, p_viewId, p_parent) {
    "use strict";

    var v_id = p_viewId;
    var v_parentId = p_parentId;
    var v_editorDiv;
    var v_open = false;

    var v_parentView = p_parent;
    var v_viewmodel = p_viewmodel;

    var v_requestPath;
    var v_filterPath;
    var v_filter;
    var v_refresh;

    var v_customDataForEditor = {
        "editorOptions": {
            "disable_array_reorder": true,
            "disable_edit_json": true,
            "disable_collapse": true,
            "no_additional_properties": true
        },
        "css": {
            "width": "600px",
            "height": "506px",
            "z-index": 2000
        },
        "closeable": true,
        "draggable": true,
        "headerText": "Edit filter"
    }

    var v_viewmodelForJSONEditor;
    var editor;

    var v_this = this;

    function init() {
        v_viewmodelForJSONEditor = new FilterElementEditorViewmodel(v_viewmodel.getFilterPartCopy(v_requestPath, v_filterPath), save);
        editor = new CView_JSONEditor([v_viewmodelForJSONEditor], v_id, v_parentId, v_customDataForEditor);

        editor.applicationCreated();
        ViewUtils.applyCss(v_customDataForEditor, v_id);

        v_editorDiv = $("#" + v_id);
        v_editorDiv.on('click', function(){
            v_parentView.setFocusedObj(v_this);
        });
        v_editorDiv.on('dragstart', function(){
            v_parentView.setFocusedObj(v_this);
        });
        v_editorDiv.on('remove', closed);
        editor.refresh(true);

        ViewUtils.jumpToEditor(v_id);
    };

    function closed() {
        if (v_open) {
            v_open = false;
            v_refresh();
        }
    }

    this.setDefaultZidx = function() {
        v_editorDiv.css("z-index" , 2500);
    };

    this.setZidx = function() {
        v_editorDiv.css("z-index" , 3500);
    };

    this.open = function(p_requestPath, p_filterPath, p_offset, p_refresh) {
        if (v_open) {
            v_this.close();
        }
        v_requestPath = p_requestPath;
        v_filterPath = p_filterPath;
        v_filter = v_viewmodel.getFilterPart(v_requestPath, v_filterPath);
        v_refresh = p_refresh;
        v_customDataForEditor.offset = p_offset;
        init();
        v_open = true;
    };

    this.close = function() {
        if (v_open) {
            $("#" + v_id).remove();
        }
    };

    function save(newFilter) {
        if (v_open) {
            if (newFilter.dataValue != undefined) {
                v_filter.dataValue = newFilter.dataValue;
                v_filter.request = undefined;
            } else if (newFilter.request != undefined) {
                // preserve params
                if (v_filter.request != undefined) {
                    v_filter.request.source = newFilter.request.source;
                    v_filter.request.element = newFilter.request.element;
                    v_filter.request.ptcname = newFilter.request.ptcname;
                } else {
                    v_filter.request = newFilter.request;
                }
                v_filter.dataValue = undefined;
            }
        }
    }
}

function FilterElementEditorViewmodel(p_data, p_save) {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Edit filter",
        "type": "object",
        "oneOf": [
            {
                "title": "data value",
                "type": "object",
                "properties": {
                    "dataValue": {
                        "description": "A value",
                        "type": "string",
                        "default": "true"
                    }
                },
                "required": ["dataValue"],
                "additionalProperties": false
            },
            {
                "title": "request",
                "type": "object",
                "properties": {
                    "request": {
                        "description": "A request that will be converted to a value during the filter evaluation",
                        "type": "object",
                        "properties": {
                            "source": {
                                "description": "The source",
                                "type": "string"
                            },
                            "element": {
                                "description": "The element",
                                "type": "string"
                            },
                            "ptcname": {
                                "description": "The ptc name",
                                "type": "string"
                            }
                        },
                        "required": ["source", "element"],
                        "additionalProperties": false
                    }
                },
                "required": ["request"],
                "additionalProperties": false
            }
        ]
    };

    var data = p_data;

    this.getJSONData = function(callback) {
        callback(data);
    };

    this.setJSONData = p_save;

    this.getSchema = function() {
        return schema;
    };
}
//# sourceURL=GuiEditor\Views\View_FilterElementEditor.js
