// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_RequestEditor_View(p_viewModel, p_parent) {
    "use strict";

    var v_viewModel = p_viewModel.getRequestEditorViewModel();
    var v_parent = p_parent;

    var v_desktopData;
    var v_helpTree;
    var v_requestTree;
    var v_elementEditor;
    var v_filterEditor;
    var v_editorContainer = new GuiEditor_EditorContainer_View(p_viewModel, "GuiEditor_Playground", this);
    var v_checkDisabled = false;
    var v_this = this;

    var jsTreeUtils = new JsTreeUtils();

    ///////////////////// GETTER FOR SUBVIEWS //////////////////////////////

    this.getEditorContainerView = function() {
        return v_editorContainer;
    };

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.applicationCreated = function() {
        var aligner = new CView_Aligner([], "GuiEditor_RequestEditorSplit", undefined, {
            "existing": true,
            "resizable": true
        });
        aligner.applicationCreated();

        v_desktopData = v_viewModel.getDesktopData();
        v_helpTree = $("#GuiEditor_HelpTree");
        v_requestTree = $("#GuiEditor_RequestTree");
        v_editorContainer.applicationCreated();

        v_elementEditor = new GuiEditor_ElementEditor_View(p_viewModel, "GuiEditor_Playground", "GuiEditor_ElementEditor", v_this);
        v_filterEditor = new GuiEditor_FilterEditor_View(p_viewModel, "GuiEditor_Playground", "GuiEditor_FilterEditor", v_this, v_requestTree, v_helpTree);

        setupCallbacks();
    };

    this.setFocusedObj = function(p_object) {
        v_parent.setFocusedObj(p_object);
    };

    this.setDefaultZidx = function() {};
    this.setZidx = function() {};
    this.deletePressed = function() {
        var selected = v_requestTree.jstree("get_selected")[0];
        if (selected != undefined) {
            deleteNode(selected);
        }
    };

    this.fullRefresh = function() {
        v_desktopData = v_viewModel.getDesktopData();
        function viewsInitialized() {
            v_editorContainer.fullRebuildConnections();
            v_editorContainer.refreshConnections();
            v_parent.toggleButtons(true);

            $(document).on("keydown", toggleValidation);
            $(document).on("keyup", toggleValidation);
        }

        var taskList = new TaskList([new GenericTask(createViewElements), new GenericTask(v_editorContainer.createViewsFromExistingData)], viewsInitialized);
        taskList.taskOperation();
    };

    this.destroy = function() {
        closeEditors();
        $(document).off('dnd_move.vakata', helpToRequestDrag);
        $(document).off('dnd_stop.vakata', helpToRequestDrop);
        $(document).off('dnd_stop.vakata', requestToHelpDrop);
        $(document).off("keydown", toggleValidation);
        $(document).off("keyup", toggleValidation);
        v_editorContainer.destroy();
        v_requestTree.jstree("destroy");
    };

    this.search = function(string) {
        var nodes = $("#GuiEditor_RequestTree li");
        for (var i = 0; i < nodes.length; ++i) {
            var path = jsTreeUtils.getPath(v_requestTree.jstree("get_node", $(nodes[i]))).path;
            if (v_viewModel.matches(string, path)) {
                $(nodes[i]).addClass("found");
            }
        }
    };

    ///////////////////// INTERACTION WITH OUTHER VIEWS //////////////////////////////

    this.getRequestTree = function() {
        return v_requestTree;
    };

    this.requestRenamed = function(p_path, name) {
        v_requestTree.jstree("rename_node", jsTreeUtils.getLastNodeIdFromPath(v_requestTree, p_path), name);
        v_editorContainer.requestRenamed(p_path, name);
    };

    this.selectionAdded = function(path) {
        v_viewModel.selectionAdded(path);
    };

    this.selectionOrFilterChanged = function(p_path, p_selection, p_filter, p_rangeFilter, p_writableInfo) {
        var id = jsTreeUtils.getNodeIdFromPath(v_requestTree, mcopy(p_path));
        if (id !== false) {
            $("#" + id).removeClass("GuiEditor_NodeWithData GuiEditor_NodeWithSelection GuiEditor_NodeWithFilter GuiEditor_NodeWithRangeFilter GuiEditor_NodeWithWritableInfo");
            var hasData = false
            if (p_selection) {
                $("#" + id).addClass("GuiEditor_NodeWithSelection");
                hasData = true;
            }
            if (p_filter) {
                $("#" + id).addClass("GuiEditor_NodeWithFilter");
                hasData = true;
            }
            if (p_rangeFilter) {
                $("#" + id).addClass("GuiEditor_NodeWithRangeFilter");
                hasData = true;
            }
            if (p_writableInfo) {
                $("#" + id).addClass("GuiEditor_NodeWithWritableInfo");
                hasData = true;
            }
            if (hasData) {
                $("#" + id).addClass("GuiEditor_NodeWithData");
            }
        }
    };

    ///////////////////// CREATING VIEW ELEMENTS AND INTERACTIONS //////////////////////////////

    function createViewElements(callback) {
        v_viewModel.getHelp(createHelpJSTree, v_desktopData["sortHelp"]);
        v_viewModel.getRequestTree(function(data) {
            createRequestJSTree(data, callback);
        });
    }

    function createHelpJSTree(p_data) {
        v_helpTree.jstree("destroy");
        v_helpTree = v_helpTree.jstree({
            "core": {
                "data": p_data,
                "check_callback" : function(operation, node, node_parent, node_position, more) {
                     if (operation === "copy_node" || operation === "move_node") {
                         return false;
                     } else {
                         return true;
                     }
                },
                "multiple" : false,
                "animation": false,
                "worker": false
            },
            "plugins" : ["search", "dnd"],
            "search" : {
                "show_only_matches": true
            },
            "dnd": {
                "always_copy": true
            }
        });

        $(document).off('dnd_stop.vakata', requestToHelpDrop);
        $(document).on('dnd_stop.vakata', requestToHelpDrop);

        v_helpTree.bind("hover_node.jstree", function(e, data) {
            $("#"+data.node.id).prop("title", data.node.data);
        });
    }

    function createRequestJSTree(p_data, p_callback) {
        v_requestTree.jstree("destroy");
        v_requestTree = v_requestTree.jstree({
            "core": {
                "data": p_data,
                "check_callback": function(operation, node, node_parent, node_position, more) {
                    if (operation === "copy_node" && v_checkDisabled && jsTreeUtils.isNodeFromTree(node, v_helpTree) && jsTreeUtils.getDepth(node, v_helpTree) > 1 && $("#GuiEditor_RequestTree > ul").children().length > 0) {
                        return true;
                    } else if (operation === "copy_node" && jsTreeUtils.isNodeFromTree(node, v_helpTree) && $("#GuiEditor_RequestTree > ul").children().length > 0) {
                        return dragFromHelpValidate(node.id, node_parent.id);
                    } else if (operation === "copy_node" && jsTreeUtils.isNodeFromTree(node, v_requestTree)) {
                        return dragFromRequestValidate(node.id, node_parent.id);
                    } else if (operation === "copy_node" && node.text === "Selection connections" && node_parent.id != "#") {
                        return true;
                    } else if (operation === "copy_node" && node.text === "Request connection" && node_parent.id != "#") {
                        return true;
                    } else if (operation === "copy_node") {
                        return false;
                    } else {
                        return true;
                    }
                },
                "multiple": false,
                "animation": false,
                "worker": false
            },
            "plugins": ["contextmenu", "dnd"],
            "dnd": {
                "always_copy": true
            },
            "contextmenu": {
                "items": function($node) {
                    return {
                        "Edit": {
                            "label": "Edit request",
                            "action": function(data) {
                                var offset = data.reference.offset();
                                offset.left += data.reference.width();

                                v_elementEditor.open(jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.reference)).path, offset);
                                v_parent.setFocusedObj(v_elementEditor);
                            }
                        },
                        "EditFilter": {
                            "label": "Edit filter",
                            "action": function(data) {
                                var offset = data.reference.offset();
                                offset.left += data.reference.width();

                                var node = v_requestTree.jstree("get_node", data.reference);
                                v_filterEditor.open(jsTreeUtils.getPath(node).path, offset);
                                v_parent.setFocusedObj(v_filterEditor);
                            },
                            "separator_after": true
                        },
                        "Add": {
                            "label": "Add child request",
                            "action": function(data) {
                                var path = jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.reference)).path;
                                v_viewModel.addEmptyChildRequest(mcopy(path), 0, partialRefresh);
                                path.push(0);
                                var added = jsTreeUtils.getLastNodeIdFromPath(v_requestTree, path);
                                v_requestTree.jstree("deselect_node", data.reference)
                                v_requestTree.jstree("select_node", added);

                                var offset = $("#" + added + "_anchor").offset();
                                offset.left += $("#" + added + "_anchor").width();

                                v_elementEditor.open(path, offset);
                                v_parent.setFocusedObj(v_elementEditor);
                            }
                        },
                        "Copy": {
                            "label": "Copy",
                            "action": function(data) {
                                var path = jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.reference)).path;
                                v_viewModel.copyRequest(mcopy(path), partialRefresh);
                                path[path.length - 1] += 1;
                                var added = jsTreeUtils.getLastNodeIdFromPath(v_requestTree, path);
                                v_requestTree.jstree("deselect_node", data.reference)
                                v_requestTree.jstree("select_node", added);
                            }
                        },
                        "Delete": {
                            "label": "Delete",
                            "action": function(data) {
                                deleteNode(data.reference);
                            },
                            "separator_after": true
                        },
                        "SizeOf": {
                            "label": "Convert to sizeOf",
                            "action": function(data) {
                                var path = jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.reference)).path;
                                v_viewModel.convertToSizeOf(path, v_this.requestRenamed);
                            }
                        },
                        "ElementPresent": {
                            "label": "Convert to dataElementPresent",
                            "action": function(data) {
                                var path = jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.reference)).path;
                                v_viewModel.convertToDataElementPresent(path, v_this.requestRenamed);
                            },
                            "separator_after": true
                        },
                        "ExpandAll": {
                            "label": "Expand all",
                            "action": function(data) {
                                v_requestTree.jstree("open_all");
                            }
                        },
                        "CollapseAll": {
                            "label": "Collapse all",
                            "action": function(data) {
                                v_requestTree.jstree("close_all");
                            },
                            "separator_after": true
                        },
                        "CopyRequestAsText": {
                            "label": "Copy Request as Text",
                            "action": function(data) {
                                var text = JSON.stringify(v_viewModel.getRequest(), null, 4);

                                var textArea = document.createElement("textarea");
                                textArea.style.position = 'fixed';
                                textArea.style.top = 0;
                                textArea.style.left = 0;
                                textArea.style.width = '2em';
                                textArea.style.height = '2em';
                                textArea.style.padding = 0;
                                textArea.style.border = 'none';
                                textArea.style.outline = 'none';
                                textArea.style.boxShadow = 'none';
                                textArea.style.background = 'transparent';
                                textArea.value = text;

                                document.body.appendChild(textArea);
                                textArea.select();
                                document.execCommand('copy');
                                document.body.removeChild(textArea);
                            }
                        }
                    };
                },
                "select_node": true
            }
        });

        v_requestTree.bind("hover_node.jstree", function(e, data) {
            var string = JSON.stringify(v_viewModel.getRequestCopy(jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.node.id)).path), null, 4);
            $("#"+data.node.id).prop("title", string);
        });

        v_requestTree.bind("select_node.jstree", function (e, data) {
            closeEditors();
            v_parent.setFocusedObj(v_this);
        });

        v_requestTree.bind("copy_node.jstree", function(e, data) {
            if (jsTreeUtils.isNodeFromTree(data.original, v_helpTree)) {
                helpNodeCopied(data);
            } else if (jsTreeUtils.isNodeFromTree(data.original, v_requestTree)) {
                requestNodeCopied(data);
            } else if (data.original.text === "Selection connections") {
                v_requestTree.jstree("delete_node", data.node.id);
                v_editorContainer.selectionAddedInRequestTree(data.parent, data.original.id);
            } else if (data.original.text === "Request connection") {
                v_requestTree.jstree("delete_node", data.node.id);
                v_editorContainer.importAddedInRequestTree(data.parent, data.original.id);
            }
        });

        jsTreeUtils.openNodes(v_requestTree, v_desktopData.openRequests);

        v_requestTree.bind("after_open.jstree after_close.jstree", function(e, data) {
            v_editorContainer.refreshConnections(v_this);
            v_desktopData.openRequests = jsTreeUtils.findOpenNodes(v_requestTree);
        });

        v_requestTree.bind("after_open.jstree delete_node.jstree ready.jstree", function(e, data) {
            // some operations are async and even their callback gets called too soon... so we have to use a timeout
            // after_open, when we open a node, or when we insert one, delete_node, when we delete a node, and ready when the tree gets drawn in the beginning
            setTimeout(function() {
                v_viewModel.findSelectionsAndFilters();
            }, 100);
        });

        v_requestTree.bind("ready.jstree", function(e, data) {
            p_callback(true);
        });

        $(document).off('dnd_move.vakata', helpToRequestDrag);
        $(document).on('dnd_move.vakata', helpToRequestDrag);

        $(document).off('dnd_stop.vakata', helpToRequestDrop);
        $(document).on('dnd_stop.vakata', helpToRequestDrop);
    }

    function helpToRequestDrag(event, data) {
        var node = {
            "id": data.data.nodes[0]
        }
        if ($(data.event.target).attr("id") == "GuiEditor_RequestTree" && jsTreeUtils.isNodeFromTree(node, v_helpTree) && (v_checkDisabled || dragFromHelpValidate(data.data.nodes[0], "#"))) {
            data.helper.find('.jstree-icon').removeClass('jstree-er').addClass('jstree-ok');
        }
    }

    function helpToRequestDrop(event, data) {
        var node = {
            "id": data.data.nodes[0]
        }
        if ($(data.event.target).attr("id") == "GuiEditor_RequestTree" && jsTreeUtils.isNodeFromTree(node, v_helpTree) && (v_checkDisabled || dragFromHelpValidate(data.data.nodes[0], "#"))) {
            var helpPath = jsTreeUtils.getPath(v_helpTree.jstree("get_node", node.id)).path;
            var position = $("#GuiEditor_RequestTree > ul").children().length;
            v_viewModel.createRequest(helpPath, position, partialRefresh);
            v_viewModel.findSelectionsAndFilters();
        }
    }

    function requestToHelpDrop(event, data) {
        var node = {
            "id": data.data.nodes[0]
        }
        if (($(data.event.target).attr("id") == "GuiEditor_HelpTree" || jsTreeUtils.isNodeFromTree({"id": $(data.event.target).attr("id")}, v_helpTree)) && jsTreeUtils.isNodeFromTree({"id": data.data.nodes[0]}, v_requestTree)) {
            deleteNode(v_requestTree.jstree("get_node", node.id));
        }
    }

    function setupCallbacks() {
        $("#GuiEditor_HelpSearch").on("input", function() {
            v_helpTree.jstree("search", $(this).val());
        });
        $("#GuiEditor_Button_AddRequest").on("click", addEmptyRequest);

        $("#GuiEditor_SortHelpTree").button().click(function(event, ui) {
            if($(this).prop('checked')) {
                v_viewModel.getHelp(createHelpJSTree, true);
                v_desktopData["sortHelp"] = true;
            } else {
                v_viewModel.getHelp(createHelpJSTree, false);
                v_desktopData["sortHelp"] = false;
            }
        });

        if (v_desktopData["sortHelp"]) {
            $("#GuiEditor_SortHelpTree").prev().addClass('ui-state-active');
            $("#GuiEditor_SortHelpTree").prop('checked', true);
        }
    }

    function partialRefresh(p_path, p_data) {
        var parentId = getParentNodeIdFromPath(p_path);
        v_requestTree.jstree("create_node", parentId, p_data.text, p_path[p_path.length - 1]);
        v_requestTree.jstree("open_node", parentId);

        v_editorContainer.updatePaths(p_path, 1);
        v_editorContainer.refreshConnections(v_this);
    }

    function closeEditors() {
        v_elementEditor.close();
        v_filterEditor.close();
        v_viewModel.findSelectionsAndFilters();
    }

    ///////////////////// HANDLING EVENTS //////////////////////////////

    function helpNodeCopied(data) {
        closeEditors();
        v_requestTree.jstree("delete_node", data.node.id);
        var helpPath = jsTreeUtils.getPath(v_helpTree.jstree("get_node", data.original.id)).path;
        if (data.parent === "#") {
            v_viewModel.createRequest(helpPath, data.position, partialRefresh);
        } else {
            var requestPath = jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.parent)).path;
            v_viewModel.addChildRequest(helpPath, requestPath, data.position, partialRefresh);
        }
    }

    function requestNodeCopied(data) {
        closeEditors();
        v_requestTree.jstree("delete_node", data.node.id);
        var fromPathObj = jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.original.id));
        var fromPath = fromPathObj.path;
        var fromPathStr = fromPathObj.strpath;
        var toPath = [];
        var toPathStr = "";
        if (data.parent != "#") {
            var toPathObj = jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.parent));
            toPath = toPathObj.path;
            toPathStr = toPathObj.strpath;
        }
        v_viewModel.moveRequest(fromPath, toPath, data.position);

        function moveComplete(node, parent, pos) {
            toPath.push(data.position);
            v_editorContainer.pathsMoved(fromPath, fromPathStr, toPath, toPathStr);
            v_requestTree.jstree("open_node", parent);
            v_editorContainer.refreshConnections(v_this);
        }

        v_requestTree.jstree("move_node", v_requestTree.jstree("get_node", data.original.id), v_requestTree.jstree("get_node", data.parent), data.position, moveComplete);
    }

    function dragFromHelpValidate(p_helpId, p_requestParentId) {
        var helpPath = jsTreeUtils.getPath(v_helpTree.jstree("get_node", p_helpId)).path;
        if (p_requestParentId === "#") {
            return v_viewModel.isValidToCreateRequest(helpPath);
        } else {
            var requestPath = jsTreeUtils.getPath(v_requestTree.jstree("get_node", p_requestParentId)).path;
            return v_viewModel.isValidToAddRequest(helpPath, requestPath);
        }
    }

    function dragFromRequestValidate(p_fromId, p_toId) {
        var fromPath = jsTreeUtils.getPath(v_requestTree.jstree("get_node", p_fromId)).path;
        var toPath;
        if (p_toId != "#") {
            toPath = jsTreeUtils.getPath(v_requestTree.jstree("get_node", p_toId)).path;
        } else {
            toPath = [];
        }

        return v_viewModel.isValidToMoveRequest(fromPath, toPath);
    }

    function getParentNodeIdFromPath(p_path) {
        if (p_path.length === 1) {
            return "#";
        } else {
            var id = "#";
            for (var i = 0; i < p_path.length - 1; ++i) {
                id = v_requestTree.jstree("get_node", id).children[p_path[i]];
            }
            return id;
        }
    }

    function addEmptyRequest() {
        closeEditors();

        v_viewModel.createEmptyRequest(0, partialRefresh);
        v_requestTree.jstree("deselect_node", v_requestTree.jstree("get_selected"));
        var newNodeId = jsTreeUtils.getLastNodeIdFromPath(v_requestTree, [0]);
        v_requestTree.jstree("select_node", newNodeId);

        var offset = $("#" + newNodeId + "_anchor").offset();
        offset.left += $("#" + newNodeId + "_anchor").width();

        v_elementEditor.open([0], offset);
        v_parent.setFocusedObj(v_elementEditor);
    }

    function deleteNode(p_data) {
        var path = jsTreeUtils.getPath(v_requestTree.jstree("get_node", p_data)).path;
        v_viewModel.deleteRequest(mcopy(path));

        v_requestTree.jstree("delete_node", p_data);
        closeEditors();

        v_editorContainer.updatePaths(mcopy(path), -1);
        v_editorContainer.refreshConnections(v_this);

        v_desktopData.openRequests = jsTreeUtils.findOpenNodes(v_requestTree);
    }

    function toggleValidation(event) {
        if (event.keyCode == 17) {
            if (event.type == "keydown") {
                v_checkDisabled = true;
            } else {
                v_checkDisabled = false;
            }
        }
    }

    ///////////////////// CONNECTION RELATED /////////////////////

    function RequestEndpoint(p_identifier) {
        var v_identifier = p_identifier;

        this.getOffset = function() {

            var id = jsTreeUtils.getLastNodeIdFromPath(v_requestTree, v_identifier());
            var htmlObj = $("#" + id + "_anchor");

            var offset = htmlObj.offset();
            offset.top += htmlObj.height() / 2;
            offset.left += htmlObj.width()
            return offset;
        };

        this.object = v_this;
    }

    this.getEndpoint = function(identifier) {
        return new RequestEndpoint(identifier);
    };

    this.getConnectionInformation = function(identifier) {
        return jsTreeUtils.getPath(v_requestTree.jstree("get_node", identifier));
    };

    this.disabled = function() {};
}

//# sourceURL=GuiEditor\Views\View_RequestEditor.js
