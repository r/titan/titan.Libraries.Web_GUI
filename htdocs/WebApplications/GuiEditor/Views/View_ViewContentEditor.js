// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_ViewContentEditor_View(p_viewmodel, p_id) {

    var base = new GuiEditor_BaseContentEditor_View(p_viewmodel, p_id);

    var HTML_OF_TAB = '' +
        '<li>' +
            '<a href="#{0}">{1}</a>' +
            '<span class="ui-icon ui-icon-close">Remove Tab</span>' +
        '</li>';

    var HTML_OF_FILETABS = '' +
        '<div id="{0}">' +
            '<div id="{0}_Tabs">' +
                '<ul>' +
                    '<li><a {1}href="#{0}_Html">HTML</a></li>' +
                    '<li><a {1}href="#{0}_Css">CSS</a></li>' +
                    '<li><a {1}href="#{0}_Js">JavaScript</a></li>' +
                '</ul>' +
                '<div id="{0}_Html"></div>' +
                '<div id="{0}_Css"></div>' +
                '<div id="{0}_Js"></div>' +
            '</div>' +
        '</div>';

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.applicationCreated = function() {
        base.applicationCreated();
        setupCallbacks();
    };

    function setupCallbacks() {
        $("#" + base.viewId + "_Button_New").on("click", newTab);
        $("#" + base.viewId + "_Button_Load").on("click", load);
        $("#" + base.viewId + "_Button_LoadTemplate").on("click", loadTemplate);
        $("#" + base.viewId + "_Button_Save").on("click", save);
        $("#" + base.viewId + "_Button_SaveAs").on("click", saveAs);
    }

    ///////////////////// HANDLING TABS AND PANELS //////////////////////////////

    function getCurrentlyOpenFilePanel(activePanel) {
        if (activePanel == undefined) {
            activePanel = base.getCurrentlyOpenPanel();
        }
        return $("#" + activePanel.attr("id") + " div.ui-tabs-panel[aria-hidden='false']");
    }

    base.getPanelOnActivation = function(panel) {
        return getCurrentlyOpenFilePanel(panel);
    };

    this.applicationFocused = base.applicationFocused;

    base.getPanelOnFocus = function() {
        return getCurrentlyOpenFilePanel();
    };

    base.getEditedTab = function() {
        return base.getCurrentlyOpenPanel().find(".ui-tabs-active a");
    };

    base.getIdOfPanelContainingEditor = function() {
        return getCurrentlyOpenFilePanel().attr("id");
    };

    ///////////////////// HANDLING EVENTS //////////////////////////////

    function save() {
        base.save(saveAs);
    }

    function saveAs() {
        function viewSaved(name, ok, id, existingId) {
            if (!ok) {
                alert("Failed to save view " + name);
            } else {
                base.changeTabName(name);
                base.setEdited(false, id + "_Html");
                base.setEdited(false, id + "_Css");
                base.setEdited(false, id + "_Js");
                if (existingId != undefined) {
                    $("a[href$=" + existingId + "]").next("span").click();
                }
            }
        }

        base.saveAs(base.viewmodel.viewExists, base.viewmodel.saveViewAs, "View", viewSaved);
    }

    function createTab(id, name, operation) {
        if (operation == "exists") {
            base.fileTabs.tabs("option", "active", $('a[href$=' + id + ']').parent().index());
            return;
        }

        base.fileTabs.find(".ui-tabs-nav").first().append(HTML_OF_TAB.format(id, name));
        if (operation == "new") {
            base.fileTabs.append(HTML_OF_FILETABS.format(id, 'class="file-changed" '));
        } else {
            base.fileTabs.append(HTML_OF_FILETABS.format(id, ""));
        }

        base.fileTabs.tabs("refresh");

        var tabs = $("#" + id + "_Tabs").tabs({
            "active": 0,
            activate: function(ev, ui) {
                base.panelActivated(ui.newPanel);
            }
        });

        base.fileTabs.tabs("option", "active", $("#" + base.viewId + "_Tabs > ul > li").last().index());
    }

    function newTab() {
        base.viewmodel.newViewContent(createTab);
    }

    function load() {
        function gotViewName(viewName) {
            base.viewmodel.loadViewContent(viewName, createTab);
        }

        function optionsArrived(options) {
            var dialog = new ChoiceDialogWithButton(base.viewId, "GuiEditor_Dialog_LoadView", {
                "header": "Select a view",
                "text": "Please select a view from the table below.",
                "choices": options,
                "callback": gotViewName,
                "buttonHandler": deleteView,
                "buttonText": "X",
                "buttonStyle": "color: red;",
                "closeOnButtonPress": true
            });
            dialog.open();
        }

        base.viewmodel.listViews(optionsArrived);
    }

    function loadTemplate() {
        function gotViewName(viewName) {
            base.viewmodel.loadViewTemplate(viewName, createTab);
        }

        function optionsArrived(options) {
            var dialog = new ChoiceDialog(base.viewId, "GuiEditor_Dialog_LoadView", {
                "header": "Select a view",
                "text": "Please select a view from the table below.",
                "choices": options,
                "callback": gotViewName
            });
            dialog.open();
        }

        base.viewmodel.listViewTemplates(optionsArrived);
    }

    function deleteView(viewName) {
        var text = "Are you sure your want to delete view " + viewName + "?";

        function viewDeleted(ok, id) {
            if (!ok) {
                alert("Failed to delete view " + viewName);
            } else if (id != undefined) {
                base.setEdited(true, id + "_Html");
                base.setEdited(true, id + "_Css");
                base.setEdited(true, id + "_Js");
            }
            load();
        }

        var dialog = new ConfirmationDialog(base.viewId, "GuiEditor_Dialog_DeleteView", {
            "header": "Delete view",
            "text": text,
            "callback": function() {
                base.viewmodel.deleteView(viewName, viewDeleted);
            }
        });
        dialog.open();
    }
}
//# sourceURL=GuiEditor\Views\View_ViewContentEditor.js