// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_ViewModelContentEditor_View(p_viewmodel, p_id) {

    var base = new GuiEditor_BaseContentEditor_View(p_viewmodel, p_id);

    var HTML_OF_TAB = '' +
        '<li>' +
            '<a {2}href="#{0}">{1}</a>' +
            '<span class="ui-icon ui-icon-close">Remove Tab</span>' +
        '</li>';

    var HTML_OF_FILETABS = '<div id="{0}"></div>';

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.applicationCreated = function() {
        base.applicationCreated();
        setupCallbacks();
    };

    ///////////////////// HANDLING TABS AND PANELS //////////////////////////////

    this.applicationFocused = base.applicationFocused;

    function setupCallbacks() {
        $("#" + base.viewId + "_Button_New").on("click", newTab);
        $("#" + base.viewId + "_Button_Load").on("click", load);
        $("#" + base.viewId + "_Button_LoadTemplate").on("click", loadTemplate);
        $("#" + base.viewId + "_Button_Save").on("click", save);
        $("#" + base.viewId + "_Button_SaveAs").on("click", saveAs);
    }

    ///////////////////// HANDLING EVENTS //////////////////////////////

    function save() {
        base.save(saveAs);
    }

    function saveAs() {
        function viewmodelSaved(name, ok, id, existingId) {
            if (!ok) {
                alert("Failed to save viewmodel " + name);
            } else {
                base.changeTabName(name);
                base.setEdited(false, id);
                if (existingId != undefined) {
                    $("a[href$=" + existingId + "]").next("span").click();
                }
            }
        }

        base.saveAs(base.viewmodel.viewmodelExists, base.viewmodel.saveViewmodelAs, "Viewmodel", viewmodelSaved);
    }

    function createTab(id, name, operation) {
        if (operation == "exists") {
            base.fileTabs.tabs("option", "active", $('a[href$=' + id + ']').parent().index());
            return;
        }

        if (operation == "new") {
            base.fileTabs.find(".ui-tabs-nav").first().append(HTML_OF_TAB.format(id, name, 'class="file-changed" '));
        } else {
            base.fileTabs.find(".ui-tabs-nav").first().append(HTML_OF_TAB.format(id, name, ""));
        }
        base.fileTabs.append(HTML_OF_FILETABS.format(id));

        base.fileTabs.tabs("refresh");
        base.fileTabs.tabs("option", "active", $("#" + base.viewId + "_Tabs > ul > li").last().index());
    }

    function newTab() {
        base.viewmodel.newViewmodelContent(createTab);
    }

    function load() {
        function gotViewmodelName(viewmodelName) {
            base.viewmodel.loadViewmodelContent(viewmodelName, createTab);
        }

        function optionsArrived(options) {
            var dialog = new ChoiceDialogWithButton(base.viewId, "GuiEditor_Dialog_LoadViewmodel", {
                "header": "Select a viewmodel",
                "text": "Please select a viewmodel from the table below.",
                "choices": options,
                "callback": gotViewmodelName,
                "buttonHandler": deleteViewmodel,
                "buttonText": "X",
                "buttonStyle": "color: red;",
                "closeOnButtonPress": true
            });
            dialog.open();
        }

        base.viewmodel.listViewmodels(optionsArrived);
    }

    function loadTemplate() {
        function gotViewmodelName(viewmodelName) {
            base.viewmodel.loadViewmodelTemplate(viewmodelName, createTab);
        }

        function optionsArrived(options) {
            var dialog = new ChoiceDialog(base.viewId, "GuiEditor_Dialog_LoadViewmodel", {
                "header": "Select a viewmodel",
                "text": "Please select a viewmodel from the table below.",
                "choices": options,
                "callback": gotViewmodelName
            });
            dialog.open();
        }

        base.viewmodel.listViewmodelTemplates(optionsArrived);
    }

    function deleteViewmodel(viewmodelName) {
        var text = "Are you sure your want to delete viewmodel " + viewmodelName + "?";

        function viewmodelDeleted(ok, id) {
            if (!ok) {
                alert("Failed to delete viewmodel " + viewmodelName);
            } else if (id != undefined) {
                base.setEdited(true, id);
            }
            load();
        }

        var dialog = new ConfirmationDialog(base.viewId, "GuiEditor_Dialog_DeleteViewmodel", {
            "header": "Delete viewmodel",
            "text": text,
            "callback": function() {
                base.viewmodel.deleteViewmodel(viewmodelName, viewmodelDeleted);
            }
        });
        dialog.open();
    }
}
//# sourceURL=GuiEditor\Views\View_ViewModelContentEditor.js