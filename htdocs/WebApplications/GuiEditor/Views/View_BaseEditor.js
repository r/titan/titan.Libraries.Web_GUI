// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_BaseEditor_View(p_viewmodel, p_parentId, p_viewId, p_parent, p_desktopData, p_parentObject) {

    this.viewmodel = p_viewmodel;
    this.parentId = p_parentId;
    this.id = p_viewId;
    this.parent = p_parent;
    this.jsTreeUtils = new JsTreeUtils();

    this.tree;
    this.isVisible = true;
    this.zIndex = 800;
    this.isEnabled = true;

    var v_parentDiv = document.getElementById(p_parentId);
    var v_desktopData = p_desktopData;
    var v_customDataSchema;
    var v_errorMsg = "";

    var v_this = this;
    var v_parentObject = p_parentObject;

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.init = function(callback, className, defaultOffSet) {
        createMainDiv(className, defaultOffSet);
        v_this.tree = addTreeDiv("_Tree");
        v_this.setupCallbacks();

        function treeCreated(ok) {
            if (!v_this.isVisible) {
                v_this.tree.slideToggle(0);
            }
            callback(ok);
        }

        v_this.createTree(treeCreated);

        $("#" + v_this.id).on('click', function(){
            v_this.parent.setFocusedObj(v_parentObject);
        });
        $("#" + v_this.id).on('dragstart', function(){
            v_this.parent.setFocusedObj(v_parentObject);
        });
    };

    this.destroy = function() {
        $("#" + v_this.id).remove();
    };

    this.setDefaultZidx = function() {
        $("#" + v_this.id).css("z-index", 800);
        v_this.zIndex = 800;

        v_this.parent.refreshConnections();
    };

    this.setZidx = function() {
        $("#" + v_this.id).css("z-index", 3000);
        v_this.zIndex = 3000;

        v_this.parent.refreshConnections();
    };

    ///////////////////// GENERAL EDITOR VIEW FUNCTIONS //////////////////////////////

    this.disabled = function(p_disabled) {
        if (p_disabled) {
            $("#" + v_this.id).addClass("GuiEditor_Disabled");
            if (!v_this.isEnabled) {
                // already set... this can be used to detect a cycle
                return true;
            } else {
                v_this.isEnabled = false;
                return false;
            }
        } else {
            $("#" + v_this.id).removeClass("GuiEditor_Disabled");
            if (v_this.isEnabled) {
                // already set... this can be used to detect a cycle
                return true;
            } else {
                v_this.isEnabled = true;
                return false;
            }
        }
    };

    this.isNodeFromTree = function(id) {
        return v_this.tree.jstree("get_node", id);
    };

    this.validate = function() {
        v_errorMsg = v_this.viewmodel.isValid();
        if (v_errorMsg == "") {
            $("#" + v_this.id).css("border", "1px solid #aaa");
        } else {
            $("#" + v_this.id).css("border", "1px solid red");
        }
        v_this.setHeaderText();
    };

    this.search = function(string) {
        if (v_this.viewmodel.matches(string)) {
            $("#" + v_this.id).addClass("found");
        }
    };

    ///////////////////// CREATING THE VIEW //////////////////////////////

    this.addButtonToHeader = function(header, id, className, text) {
        var button = document.createElement("button");
        button.setAttribute("id", v_this.id + id);
        button.setAttribute("class", className);
        var buttonText = document.createTextNode(text);
        button.appendChild(buttonText);
        header.appendChild(button);
    };

    this.fillHeader = function(header) {};
    this.createTree = function(callback) {callback(true)};

    function addTreeDiv(p_id) {
        var treediv = document.createElement("div");
        treediv.setAttribute("id", v_this.id + p_id);
        document.getElementById(v_this.id).appendChild(treediv);
        return $("#" + v_this.id + p_id);
    }

    function createMainDiv(className, defaultOffset) {
        var div = document.createElement("div");
        div.setAttribute("id", v_this.id);
        div.setAttribute("class", className);

        var header = document.createElement("div");
        header.setAttribute("id", v_this.id + "_Header");
        header.setAttribute("class", "GuiEditor_EditorHeader");

        v_this.fillHeader(header);

        div.appendChild(header);
        v_parentDiv.appendChild(div);

        if (v_desktopData.pos == undefined) {
            v_desktopData.pos = {
                "top": 0,
                "left": defaultOffset
            }
        }

        var offset = $("#" + v_this.parentId).offset();
        offset.top += v_desktopData.pos.top;
        offset.left += v_desktopData.pos.left;
        $("#" + v_this.id).offset(offset);
        $("#" + v_this.id).offset(offset);

        if (v_desktopData.visible == undefined) {
            v_desktopData.visible = true;
        }

        v_this.isVisible = v_desktopData.visible;

        $("#" + v_this.id).draggable({
            stop: function(event, ui) {
                redrawOnDrag(event, ui);
                document.getElementById(v_this.id).style.height = "auto";
                document.getElementById(v_this.id).style.width = "auto";
            },
            handle: "#" + v_this.id + "_Header",
            drag: redrawOnDrag,
            containment: [$("#" + v_this.parentId).offset().left, $("#" + v_this.parentId).offset().top, 20000, 20000]
        });

        v_this.setHeaderText();
        changeMinimizeButtonText(v_this.isVisible, v_this.id + "_Button_Minimize");
    }

    this.setHeaderText = function() {
        var headerObject = document.getElementById(v_this.id + "_HeaderText");
        var oldName = headerObject.innerHTML;
        var newName = v_this.viewmodel.getName();
        headerObject.innerHTML = newName;
        if (v_errorMsg == "") {
            $(headerObject).prop("title", v_this.viewmodel.getTooltip());
        } else {
            $(headerObject).prop("title", "WARNING: " + v_errorMsg + "\n\n" + v_this.viewmodel.getTooltip());
        }
        if (oldName != newName) {
            v_this.refreshConnections();
        }
        $("#" + v_this.id).width("auto");
    };

    ///////////////////// HANDLING EVENTS //////////////////////////////

    function redrawOnDrag(event, ui) {
        v_this.refreshConnections();

        if (event.type == "dragstop") {
            var parentOffset = $("#" + v_this.parentId).offset();
            var offset = $("#" + v_this.id).offset();
            offset.top = offset.top - parentOffset.top + $("#" + v_this.parentId).scrollTop();
            offset.left = offset.left - parentOffset.left + $("#" + v_this.parentId).scrollLeft();
            v_desktopData.pos = offset;
        }
    }

    this.setupCallbacks = function() {
        $("#" + v_this.id + "_Button_Edit").on("click", function(event) {
            // this stops the propagation of the event
            // when an event occures, it propagates to the parents of the object
            // we want to focus on the custom data editor, but if we do not stop the propagate, thie editor would also be focused since we listen for the click event on the div
            event.stopPropagation();
            editCustomData();
            return false;
        });

        $("#" + v_this.id + "_Header").on('contextmenu', function(event) {
            var list = [
                {
                    "text": "copy",
                    "callback": v_this.copyEditor
                },
                {
                    "text": "change class",
                    "callback": changeClass
                },
                {
                    "text": "show only this",
                    "callback": v_this.showOnlyThis
                },
                {
                    "text": "show all",
                    "callback": v_this.showAll
                },
                {
                    "text": "delete",
                    "callback": v_this.remove
                },
            ];

            v_customDataSchema = v_this.viewmodel.getCustomDataSchema();
            if (v_customDataSchema != undefined) {
                list.unshift({
                    "text": "edit with schema",
                    "callback": editCustomDataWithSchema
                });
            }

            v_this.parent.openContextMenu(list, $("#" + v_this.id).offset());

            event.preventDefault();
            return false;
        });

        v_this.setupMinimizedCallback();
    };

    this.setupMinimizedCallback = function() {
        $("#" + v_this.id + "_Button_Minimize").on("click", function() {
            v_this.isVisible = !v_this.isVisible;
            v_desktopData.visible = v_this.isVisible;
            v_this.tree.slideToggle("fast", v_this.treeSlid);
            changeMinimizeButtonText(v_this.isVisible, this.id);
            $("#" + v_this.id).width("auto");
        });
    };

    function changeMinimizeButtonText(visible, button) {
        if (!visible) {
            document.getElementById(button).innerHTML = "+";
        } else {
            document.getElementById(button).innerHTML = "_";
        }
    }

    this.copyEditor = function() {};

    this.treeSlid = function() {
        v_this.refreshConnections();
    };

    this.showOnlyThis = function() {
        v_this.parent.showOnlyThis(v_parentObject);
    };

    this.showAll = function() {
        v_this.parent.allDisabled(false);
        v_this.parent.refreshConnections();
    };

    this.remove = function() {
        v_this.destroy();
        v_this.parent.deleteConnectionsByObject(v_parentObject);
        v_this.parent.refreshConnections();
        v_this.editorDeleted();
    };

    this.editorDeleted = function() {};

    function changeClass() {
        function callback(param) {
            v_this.viewmodel.setClass(param);
            v_this.setHeaderText();
            v_this.validate();
        }
        v_this.getClasses(callback);
    }

    this.getClasses = function () {};

    this.saveOpenNodes = function() {
        v_desktopData.openNodes = v_this.jsTreeUtils.findOpenNodes(v_this.tree);
    };

    this.openNodes = function() {
        v_this.jsTreeUtils.openNodes(v_this.tree, v_desktopData.openNodes);
    };

    ///////////////////// EDITING CUSTOM DATA //////////////////////////////

    function editCustomDataWithSchema() {
        var data = v_this.viewmodel.getCustomData();
        var offset = $("#" + v_this.id).offset();
        offset.left += $("#" + v_this.id).width();
        v_this.parent.editCustomDataWithSchema(v_customDataSchema, data, offset, saveCustomData, v_this);
    }

    function saveCustomData(data) {
        v_this.viewmodel.setCustomData(data);
        v_this.setHeaderText();
        v_this.validate();
    }

    function saveCustomDataText(text) {
        var data = JSON.parse(text);
        saveCustomData(data);
    }

    function editCustomData() {
        var data = v_this.viewmodel.getCustomData();
        var offset = $("#" + v_this.id).offset();
        offset.left += $("#" + v_this.id).width();
        v_this.parent.editCustomData(JSON.stringify(data, null, 4), offset, saveCustomDataText, "Custom data for " + v_this.viewmodel.getClass(), v_this);
    }

    ///////////////////// HANDLING CONNECTIONS //////////////////////////////

    this.connectionAdded = function(connections, index, endpoint) {
        for (var i = 0; i < connections.length; ++i) {
            connections[i].updatePath(index, +1);
        }
        connections.splice(index, 0, endpoint);
        v_this.parent.addEndpoint(endpoint);
    };

    this.connectionDeleted = function(connections, index) {
        for (var i = 0; i < connections.length; ++i) {
            connections[i].updatePath(index, -1);
        }
        var endpoint = connections.splice(index, 1)[0];
        v_this.parent.deleteEndpoint(endpoint);
    };

    this.connectionMoved = function(connections, fromIndex, toIndex) {
        if (fromIndex < toIndex) {
            for (var i = 0; i < connections.length; ++i) {
                connections[i].updatePath(toIndex + 1, 1);
            }
            for (var i = 0; i < connections.length; ++i) {
                connections[i].setPath(fromIndex, toIndex + 1);
            }
            for (var i = 0; i < connections.length; ++i) {
                connections[i].updatePath(fromIndex, -1);
            }
        } else {
            for (var i = 0; i < connections.length; ++i) {
                connections[i].updatePath(toIndex, 1);
            }
            for (var i = 0; i < connections.length; ++i) {
                connections[i].setPath(fromIndex + 1, toIndex);
            }
            for (var i = 0; i < connections.length; ++i) {
                connections[i].updatePath(fromIndex + 1, -1);
            }
        }

        var endpoint = connections.splice(fromIndex, 1)[0];
        connections.splice(toIndex, 0, endpoint);
    };

    this.refreshConnections = function() {
        v_this.parent.refreshConnections(v_parentObject);
    };

    this.Endpoint = function(p_path, p_identifier, p_endpointType, p_connectionType, p_options, p_getOffsetOfHtml) {

        this.path = p_path;

        this.getOffset = function() {
            var htmlObj;

            if (!v_this.isVisible) {
                var id = v_this.id + "_Header";
                htmlObj = $("#" + id);
            } else {
                var id = v_this.jsTreeUtils.getLastNodeIdFromPath(v_this.tree, this.path);
                htmlObj = $("#" + id + "_anchor");
            }

            return p_getOffsetOfHtml(htmlObj);
        };

        this.getZIndex = function() {
            return v_this.zIndex + 1;
        };

        this.isEnabled = function() {
            return v_this.isEnabled;
        };

        this.updatePath = function(index, amount) {
            if (this.path[1] >= index) {
                this.path[1] += amount;
            }
        };

        this.setPath = function(p_from, p_to) {
            if (this.path[1] == p_from) {
                this.path[1] = p_to;
            }
        };

        this.identifier = p_identifier;
        this.endpointType = p_endpointType;
        this.connectionType = p_connectionType;
        this.object = v_parentObject;
        this.options = p_options;
    };
}
//# sourceURL=GuiEditor\Views\View_BaseEditor.js