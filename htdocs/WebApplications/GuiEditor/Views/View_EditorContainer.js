// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_EditorContainer_View(p_viewmodel, p_viewId, p_parentView) {
    "use strict";

    var v_viewId = p_viewId;
    var v_viewmodel = p_viewmodel.getEditorContainerViewmodel();
    var v_parentView = p_parentView;

    var v_viewmodelBoxes = [];
    var v_viewBoxes = [];
    var v_importBoxes = [];
    var v_htmlBox;

    var counter = {normal: -1};

    var v_editorId = "GuiEditor_CustomDataEditor";
    var v_customDataForEditor = {
        "formatAllowed": true,
        "closeable": true,
        "draggable": true,
        "focusEditorOnOpen": true,
        "css": {
            "width": "600px",
            "height": "506px"
        }
    }

    var v_schemaEditorId = "GuiEditor_CustomDataSchemaEditor";
    var v_customDataForSchemaEditor = {
        "closeable": true,
        "draggable": true,
        "css": {
            "width": "600px",
            "height": "506px"
        },
        "editorOptions": {
            "disable_edit_json": true,
            "keep_oneof_values": false
        },
        "headerText": "Edit custom data"
    }

    var v_lastOpenedBy;

    var v_this = this;

    var v_connectionsView = new GuiEditor_Connections_View(v_viewId, v_this);

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.applicationCreated = function() {
        $('#GuiEditor_Button_AddViewmodel').on("click", addViewModel_View);
        $('#GuiEditor_Button_AddView').on("click", addView_View);
        $('#GuiEditor_Button_AddImport').on("click", addImport_View);
        $('#GuiEditor_Button_Search').on("click", function() {
            $(".found").removeClass("found");
            var string = prompt("Please enter a string to search for");
            if (string != undefined) {
                string = string.toLowerCase();
                search(string);
            }
        });
        $('#GuiEditor_Button_ClearSearch').on("click", function() {
            $(".found").removeClass("found");
        });
        $("#GuiEditor_LegendToggle").on("click", function() {
            $("#GuiEditor_LegendTable").toggleClass("hidden");
        });

        /*
        $("#GuiEditor_Playground").selectable({
            "filter": ".ui-draggable",
            "selected": function(event, ui) {
                console.log(event, ui);
            }
        });
        */
    };

    function search(string) {
        v_parentView.search(string);
        v_htmlBox.search(string);
        for (var i = 0; i < v_viewmodelBoxes.length; ++i) {
            v_viewmodelBoxes[i].search(string);
        }
        for (var i = 0; i < v_viewBoxes.length; ++i) {
            v_viewBoxes[i].search(string);
        }
        for (var i = 0; i < v_importBoxes.length; ++i) {
            v_importBoxes[i].search(string);
        }
    }

    this.destroy = function() {
        for (var i = 0; i < v_viewmodelBoxes.length; ++i) {
            v_viewmodelBoxes[i].destroy();
        }

        for (var j = 0; j < v_viewBoxes.length; ++j) {
            v_viewBoxes[j].destroy();
        }

        for (var j = 0; j < v_importBoxes.length; ++j) {
            v_importBoxes[j].destroy();
        }

        if (v_htmlBox != undefined) {
            v_htmlBox.destroy();
            v_htmlBox = undefined;
        }

        v_viewmodelBoxes = [];
        v_viewBoxes = [];
        v_importBoxes = [];
        counter = {normal: -1};

        v_connectionsView.destroy();

        $("#" + v_editorId).remove();
        $("#" + v_schemaEditorId).remove();
    };

    this.setFocusedObj = function(p_object) {
        v_parentView.setFocusedObj(p_object);
    };

    ///////////////////// LISTING SETUPS AND CLASSES /////////////////////

    this.getViewClasses = function(p_callback) {
        function optionsArrived(options) {
            v_viewmodel.sortOptions(options);
            var dialog = new ChoiceDialogWithButton(v_viewId, "GuiEditor_Dialog_AddView", {
                "header": "Select a viewclass",
                "text": "Select a viewclass",
                "choices": options,
                "callback": p_callback,
                "buttonHandler": showHelp,
                "buttonText": "?"
            });
            dialog.open();
        }

        v_viewmodel.getViewClassNames(optionsArrived);
    };

    this.getViewmodelClasses = function(p_callback) {
        function optionsArrived(options) {
            v_viewmodel.sortOptions(options);
            var dialog = new ChoiceDialogWithButton(v_viewId, "GuiEditor_Dialog_AddViewmodel", {
                "header": "Select a viewmodel class",
                "text": "Select a viewmodel class",
                "choices": options,
                "callback": p_callback,
                "buttonHandler": showHelp,
                "buttonText": "?"
            });
            dialog.open();
        }

        v_viewmodel.getViewmodelClassNames(optionsArrived);
    };

    function showHelp(name) {
        alert(v_viewmodel.getHelp(name));
    }

    this.getSetups = function(p_callback) {
        function optionsArrived(options) {
            var dialog = new ChoiceDialog(v_viewId, "GuiEditor_Dialog_AddImport", {
                "header": "Select a setup to import",
                "text": "Select a setup to import",
                "choices": options,
                "callback": p_callback
            });
            dialog.open();
        }

        v_viewmodel.listSetups(optionsArrived);
    };

    ///////////////////// CREATING EDITORS /////////////////////

    function getCounter(key) {
        if (key == undefined) {
            key = "normal";
        }
        if (counter[key] == undefined) {
            counter[key] = -1;
        }
        ++counter[key];
        return counter[key];
    };

    this.createViewsFromExistingData = function(viewsInitialized) {
        function viewmodelsArrived(data) {
            function createViews(viewmodels, desktopData, ClassToInit, listToAppendInto, taskList, classType) {
                for (var i = 0; i < viewmodels.length; ++i) {
                    var viewmodel = viewmodels[i];
                    var idCounter = getCounter(classType);
                    var view = new ClassToInit(viewmodel, v_viewId, classType + idCounter, v_this, desktopData[i]);
                    listToAppendInto.push(view);
                    taskList.push(new GenericTask(view.init));
                }
            }

            var taskList = new TaskList([], viewsInitialized);

            createViews(data.view.viewmodels, data.view.desktopData, GuiEditor_ViewEditor_View, v_viewBoxes, taskList, "GuiEditor_ViewEditor_");
            createViews(data.viewmodel.viewmodels, data.viewmodel.desktopData, GuiEditor_ViewModelEditor_View, v_viewmodelBoxes, taskList, "GuiEditor_ViewmodelEditor_");
            createViews(data.imports.viewmodels, data.imports.desktopData, GuiEditor_Imports_View, v_importBoxes, taskList, "GuiEditor_ImportEditor_");

            v_htmlBox = new GuiEditor_HtmlEditor_View(v_viewmodel.getHtmlEditorViewmodel(), v_viewId, "GuiEditor_HtmlEditor", v_this);
            taskList.push(new GenericTask(v_htmlBox.init));

            taskList.taskOperation();
        }

        v_viewmodel.createViewModelsFromExistingData(viewmodelsArrived);
    };

    function createViewFromViewmodel(ClassToInit, classType, viewmodel, desktopData, listToAppendInto) {
        var idCounter = getCounter(classType);
        var view = new ClassToInit(viewmodel, v_viewId, classType + idCounter, v_this, desktopData);
        listToAppendInto.push(view);
        view.init(function() {});
    }

    function addView(ClassToInit, listToAppendInto, viewmodelFunctionToCall, classType, getClasses) {
        function viewmodelCreated(viewmodel, desktopData) {
            createViewFromViewmodel(ClassToInit, classType, viewmodel, desktopData, listToAppendInto);
        }

        function createViewModel(classname) {
            viewmodelFunctionToCall(classname, viewmodelCreated);
        }

        getClasses(createViewModel);
    }

    function addViewModel_View() {
        addView(
            GuiEditor_ViewModelEditor_View,
            v_viewmodelBoxes,
            v_viewmodel.createViewModelEditor_ViewModel,
            "GuiEditor_ViewmodelEditor_",
            v_this.getViewmodelClasses
        );
    }

    function addView_View() {
        addView(
            GuiEditor_ViewEditor_View,
            v_viewBoxes,
            v_viewmodel.createViewEditor_ViewModel,
            "GuiEditor_ViewEditor_",
            v_this.getViewClasses
        );
    }

    function addImport_View() {
        addView(
            GuiEditor_Imports_View,
            v_importBoxes,
            v_viewmodel.createImport_ViewModel,
            "GuiEditor_ImportEditor_",
            v_this.getSetups
        );
    }

    ///////////////////// COPYING EDITORS /////////////////////

    function copyView(ClassToInit, listToAppendInto, viewmodelFunctionToCall, classType, className, customData) {
        function viewmodelCreated(viewmodel, desktopData) {
            createViewFromViewmodel(ClassToInit, classType, viewmodel, desktopData, listToAppendInto);
        }

        viewmodelFunctionToCall(className, viewmodelCreated, customData);
    }

    this.copyViewEditor = function(className, customData) {
        copyView(
            GuiEditor_ViewEditor_View,
            v_viewBoxes,
            v_viewmodel.createViewEditor_ViewModel,
            "GuiEditor_ViewEditor_",
            className,
            customData
        );
    };

    this.copyViewmodelEditor = function(className, customData) {
        copyView(
            GuiEditor_ViewModelEditor_View,
            v_viewmodelBoxes,
            v_viewmodel.createViewModelEditor_ViewModel,
            "GuiEditor_ViewmodelEditor_",
            className,
            customData
        );
    };

    ///////////////////// DELETING EDITORS /////////////////////

    this.editorDeleted = function(type, editor) {
        if (type == "View") {
            var index = v_viewBoxes.indexOf(editor);
            v_viewBoxes.splice(index, 1);
            v_viewmodel.deleteViewEditor_ViewModel(index);
        } else if (type == "Import") {
            var index = v_importBoxes.indexOf(editor);
            v_importBoxes.splice(index, 1);
            v_viewmodel.deleteImport_ViewModel(index);
        } else {
            var index = v_viewmodelBoxes.indexOf(editor);

            for (var i = 0; i < v_viewBoxes.length; ++i) {
                v_viewBoxes[i].removeViewmodelConnection(index);
            }

            v_viewmodelBoxes.splice(index, 1);
            v_viewmodel.deleteViewModelEditor_ViewModel(index);
        }
    };

    ///////////////////// HANDLING SUBVIEWS: EDITING CUSTOM DATA AND OPENING CONTEXT MENU /////////////////////

    function createEditor(ClassName, viewmodel, p_id, customData, offset) {
        var id = p_id;
        customData.offset = offset;

        var editor = new ClassName([viewmodel], id, v_viewId, customData);
        editor.applicationCreated();
        ViewUtils.applyCss(customData, id);

        var editorObj = $("#" + id);
        editorObj.on('click', function(){
            v_this.setFocusedObj(editor);
        });
        editorObj.on('dragstart', function(){
            v_this.setFocusedObj(editor);
        });
        editor.setDefaultZidx = function() {
            editorObj.css("z-index" , 2500);
        };
        editor.setZidx = function() {
            editorObj.css("z-index" , 3500);
        };
        editorObj.on('remove', function() {
            v_lastOpenedBy = undefined;
        });

        v_this.setFocusedObj(editor);
        editor.refresh(true);

        ViewUtils.jumpToEditor(id);
    }

    this.editCustomData = function(p_data, p_offset, p_save, p_name, p_callerId) {
        if (v_lastOpenedBy == p_callerId) {
            $("#" + v_schemaEditorId).remove();
            $("#" + v_editorId).remove();
        } else {
            $("#" + v_schemaEditorId).remove();
            $("#" + v_editorId).remove();

            var editorViewmodel = {
                "getTextData": function(callback) {
                    callback(p_data)
                },
                "setTextData": p_save
            }

            v_customDataForEditor.headerText = p_name;
            createEditor(CView_JSONCodeEditor, editorViewmodel, v_editorId, v_customDataForEditor, p_offset);
            v_lastOpenedBy = p_callerId;
        }
    };

    this.editCustomDataWithSchema = function(schema, data, offset, saveCustomData, p_callerId) {
        if (v_lastOpenedBy == p_callerId) {
            $("#" + v_schemaEditorId).remove();
            $("#" + v_editorId).remove();
        } else {
            $("#" + v_schemaEditorId).remove();
            $("#" + v_editorId).remove();

            var editorViewmodel = {
                "getSchema": function() {
                    return schema
                },
                "getJSONData": function(callback) {
                    callback(data);
                },
                "setJSONData": saveCustomData
            }

            createEditor(CView_JSONEditor, editorViewmodel, v_schemaEditorId, v_customDataForSchemaEditor, offset);
            v_lastOpenedBy = p_callerId;
        }
    };

    this.openContextMenu = function(list, offset) {
        var customDataForMenu = {
            "offset": offset
        };
        var id = "GuiEditor_ContextMenu";
        var contextMenu = new CView_ContextMenu([{
            "getMenuElements": function() {
                return list;
            }
        }], id, v_viewId, customDataForMenu);
        contextMenu.applicationCreated();
        ViewUtils.applyCss(customDataForMenu, id);
    };

    ///////////////////// HANDLING CONNECTIONS /////////////////////

    this.getObjectsToConnect = function() {
        var list = [];
        for (var i = 0; i < v_viewmodelBoxes.length; ++i) {
            list.push(v_viewmodelBoxes[i]);
        }
        for (var i = 0; i < v_viewBoxes.length; ++i) {
            list.push(v_viewBoxes[i]);
        }
        for (var i = 0; i < v_importBoxes.length; ++i) {
            list.push(v_importBoxes[i]);
        }
        list.push(v_htmlBox);
        return list;
    };

    this.getEndpoint = function(connectionType, identifier) {
        if (connectionType == "DR_VM" || connectionType == "SR_VM" || connectionType == "R_I") {
            return v_parentView.getEndpoint(identifier);
        } else if (connectionType == "VM_V") {
            for (var i = 0; i < v_viewmodelBoxes.length; ++i) {
                var endpoint = v_viewmodelBoxes[i].getEndpoint(identifier);
                if (endpoint != undefined) {
                    return endpoint;
                }
            }
        } else if (connectionType == "V_V" || connectionType == "V_HTML") {
            var list = v_viewBoxes.concat(v_importBoxes);
            for (var i = 0; i < list.length; ++i) {
                var endpoint = list[i].getEndpoint(identifier);
                if (endpoint != undefined) {
                    return endpoint;
                }
            }
        } else {
            alert("Unhandled connection type: " + connectionType);
        }
    }

    this.allDisabled = function(p_disabled) {
        for (var i = 0; i < v_viewmodelBoxes.length; ++i) {
            v_viewmodelBoxes[i].disabled(p_disabled);
        }
        for (var i = 0; i < v_viewBoxes.length; ++i) {
            v_viewBoxes[i].disabled(p_disabled);
        }
        for (var i = 0; i < v_importBoxes.length; ++i) {
            v_importBoxes[i].disabled(p_disabled);
        }
    }

    this.refreshConnections = v_connectionsView.refreshConnections;
    this.deleteConnectionsByObject = v_connectionsView.deleteConnectionsByObject;
    this.deleteEndpoint = v_connectionsView.deleteEndpoint;
    this.addEndpoint = v_connectionsView.addEndpoint;
    this.fullRebuildConnections = v_connectionsView.fullRebuild;
    this.showOnlyThis = v_connectionsView.showOnlyThis;

    this.getConnectionInformation = function(connectionType, identifier) {
        if (connectionType == "VM_V") {
            for (var i = 0; i < v_viewmodelBoxes.length; ++i) {
                var information = v_viewmodelBoxes[i].getConnectionInformation(identifier);
                if (information != undefined) {
                    return information;
                }
            }
        } else if (connectionType == "V_V" || connectionType == "V_HTML") {
            var list = v_viewBoxes.concat(v_importBoxes);
            for (var i = 0; i < list.length; ++i) {
                var information = list[i].getConnectionInformation(identifier);
                if (information != undefined) {
                    return information;
                }
            }
        } else {
            alert("Unhandled connection type: " + connectionType);
        }
    };

    ///////////////////// EDITOR MAINTENANCE AFTER CHANGES SOMEWHERE ELSE /////////////////////

    this.updatePaths = function(path, amount) {
        for (var i = 0; i < v_viewmodelBoxes.length; ++i) {
            v_viewmodelBoxes[i].updatePaths(mcopy(path), amount);
        }
        for (var i = 0; i < v_importBoxes.length; ++i) {
            v_importBoxes[i].updatePaths(mcopy(path), amount);
        }
    };

    this.pathsMoved = function(fromPath, fromPathStr, toPath, toPathStr) {
        for (var i = 0; i < v_viewmodelBoxes.length; ++i) {
            v_viewmodelBoxes[i].pathsMoved(mcopy(fromPath), fromPathStr, mcopy(toPath), toPathStr);
        }
        for (var i = 0; i < v_importBoxes.length; ++i) {
            v_importBoxes[i].pathsMoved(mcopy(fromPath), fromPathStr, mcopy(toPath), toPathStr);
        }
    };

    this.requestRenamed = function(p_path, name) {
        for (var i = 0; i < v_viewmodelBoxes.length; ++i) {
            v_viewmodelBoxes[i].requestRenamed(p_path, name);
        }
    };

    this.selectionAddedInRequestTree = function(requestNodeId, parentId) {
        for (var i = 0; i < v_viewmodelBoxes.length; ++i) {
            if (v_viewmodelBoxes[i].selectionAddedInRequestTree(requestNodeId, parentId)) {
                break;
            }
        }
    };

    this.importAddedInRequestTree = function(requestNodeId, parentId) {
        for (var i = 0; i < v_viewmodelBoxes.length; ++i) {
            if (v_importBoxes[i].importAddedInRequestTree(requestNodeId, parentId)) {
                break;
            }
        }
    };

    this.selectionAdded = function(path) {
        v_parentView.selectionAdded(path);
    };

    this.removeViewConnection = function(viewIndex, connectionIndex) {
        if (viewIndex == -1) {
            v_htmlBox.removeViewConnection(connectionIndex);
        } else {
            v_viewBoxes[viewIndex].removeViewConnection(connectionIndex);
        }
    };

    ///////////////////// INTER VIEW COMMUNICATION /////////////////////

    this.validateEditors = function() {
        for (var i = 0; i < v_viewmodelBoxes.length; ++i) {
            v_viewmodelBoxes[i].validate();
        }

        for (var i = 0; i < v_viewBoxes.length; ++i) {
            v_viewBoxes[i].validate();
        }
    };

    this.wouldCreateACycle = function(treeId, id) {
        for (var i = 0; i < v_viewBoxes.length; ++i) {
            if (v_viewBoxes[i].wouldCreateACycle(treeId, id)) {
                return true;
            }
        }
        return false;
    };

    this.getViewmodelIndex = function(obj) {
        return v_viewmodelBoxes.indexOf(obj);
    };

    this.getRequestTree = function() {
        return v_parentView.getRequestTree();
    };
}
//# sourceURL=GuiEditor\Views\View_EditorContainer.js