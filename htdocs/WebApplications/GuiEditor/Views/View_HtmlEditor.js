// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_HtmlEditor_View(p_viewModel, p_parentId, p_viewId, p_parent) {
    "use strict";
    
    var base = new GuiEditor_BaseEditor_View(p_viewModel, p_parentId, p_viewId, p_parent, p_viewModel.getDesktopData(), this);
    
    var v_this = this;
    var v_selectedNode = "";
    
    var v_ids;
    
    var v_htmlEditorId = "GuiEditor_HtmlDataEditor";
    var v_htmlEditorOpen = false;
    var v_customDataForHtmlEditor = {
        "headerText": "Setup html",
        "closeable": true,
        "draggable": true,
        "focusEditorOnOpen": true,
        "css": {
            "width": "600px",
            "height": "506px"
        }
    }
    
    var v_cssEditorId = "GuiEditor_CssDataEditor";
    var v_cssEditorOpen = false;
    var v_customDataForCssEditor = mcopy(v_customDataForHtmlEditor);
    v_customDataForCssEditor.headerText = "Setup css";
    
    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////
    
    this.init = function(callback) {
        function baseInitDone(ok) {
            for (var i = 0; i < v_ids.length; ++i) {
                if (base.parent.getConnectionInformation("V_HTML", {"parentId": v_ids[i].id, "type": "get"})) {
                    base.connectionAdded(i, new ViewConnectionEndpoint([i], v_ids[i].id));
                }
            }
            
            callback(ok);
        }
        
        v_ids = base.viewmodel.collectIds();
        base.init(baseInitDone, "GuiEditor_HtmlEditor", 970);
    };
    
    this.destroy = function() {
        base.destroy();
        $("#" + v_htmlEditorId).off("remove");
        $("#" + v_htmlEditorId).remove();
        $("#" + v_cssEditorId).off("remove");
        $("#" + v_cssEditorId).remove();
    };
    
    this.setDefaultZidx = base.setDefaultZidx;
    this.setZidx = base.setZidx;
    this.deletePressed = function() {
        var node = base.tree.jstree("get_selected");
        if (node.length > 0) {
            deleteNode(node);
        }
    };
    
    ///////////////////// GENERAL EDITOR VIEW FUNCTIONS //////////////////////////////
    
    this.disabled = function() {};
    this.search = base.search;
    
    ///////////////////// HTML EDITOR SPECIFIC FUNCTIONS //////////////////////////////
    
    this.removeViewConnection = function(connectionIndex) {
        base.connectionsDeleted(connectionIndex);
        base.refreshConnections();
    };
    
    ///////////////////// CREATING THE VIEW //////////////////////////////
    
    base.fillHeader = function(header) {
        base.addButtonToHeader(header, "_Button_Html", "GuiEditor_EditorButtonLeft", "HTML");
        base.addButtonToHeader(header, "_Button_Css", "GuiEditor_EditorButtonLeft", "CSS");
        var labelText = document.createElement("label");
        labelText.setAttribute("id", base.id + "_HeaderText");
        labelText.setAttribute("class", "GuiEditor_HtmlEditorHeaderLabel");
        header.appendChild(labelText);
        base.addButtonToHeader(header, "_Button_Minimize", "GuiEditor_EditorButtonRight", " ");
    };

    base.createTree = function(p_callback) {
        var data = base.viewmodel.getTreeData();
        base.tree.jstree("destroy");
        base.tree.jstree({
            "core": {
                "data" : data,
                "check_callback" : function(operation, node, node_parent, node_position, more) {
                    if (operation !== "copy_node") {
                        return true;
                    } else if (operation === "copy_node" && !base.jsTreeUtils.isRoot(node_parent) && (isAllowedViewConnection(node, node_parent, node_position) || isAllowedImportConnection(node, node_parent, node_position))) {
                        return true;
                    } else {
                        return false;
                    }
                },
                "multiple" : false,
                "animation": false,
                "worker": false
            },
            "plugins" : ["contextmenu", "dnd"],
            "contextmenu" : {
                "items" : function($node) {
                    return {
                        "Delete" : {
                            "label" : "Delete",
                            "action" : function(data) {
                                deleteNode(data.reference);
                            }
                        }
                    };
                },
                "select_node" : false
            },
            'dnd' : {
                'always_copy' : true
            }
        });

        base.tree.bind("copy_node.jstree", function(e, data) {
            
            base.tree.jstree("delete_node", data.node.id);
            base.tree.jstree("delete_node", data.node.id);
            base.tree.jstree("delete_node", data.node.id);
            
            var index = base.jsTreeUtils.getPath(base.tree.jstree("get_node", data.parent)).path[0];            
            var parentId = v_ids[index].id;
            
            if (base.connectionsDeleted(index)) {
                base.viewmodel.removeChildView(index);
            }
            
            var oldParentId = base.parent.getConnectionInformation("V_HTML", {"treeId": data.original.id, "parentId": parentId, "type": "set"});
            base.viewmodel.removeConnectedChild(oldParentId);
            
            base.connectionAdded(index, new ViewConnectionEndpoint([index], parentId));
            
            base.refreshConnections();
            
            base.tree.jstree("delete_node", data.node.id);
            base.tree.jstree("delete_node", data.node.id);
            
        });

        base.tree.bind("select_node.jstree", function(e, data) {
            if (v_selectedNode == data.node.id) {
                data.instance.deselect_node(data.node);
                v_selectedNode = "";
            } else {
                v_selectedNode = data.node.id;
            }
        });

        base.tree.bind("redraw.jstree", function(e, data) {
            document.getElementById(base.id).style.height = "auto";
        });

        base.tree.bind("ready.jstree", function(e, data) {
            p_callback(true);
        });
    };

    base.setHeaderText = function() {
        document.getElementById(base.id + "_HeaderText").innerHTML = base.viewmodel.getName();
        $("#" + base.id + "_HeaderText").prop("title", base.viewmodel.getName());
    };
    
    ///////////////////// HANDLING EVENTS //////////////////////////////
    
    base.setupCallbacks = function() {
        $("#" + base.id + "_Button_Css").on("click", function(event) {
            event.stopPropagation();
            editCss();
            return false;
        });
        
        $("#" + base.id + "_Button_Html").on("click", function(event) {
            event.stopPropagation();
            editHtml();
            return false;
        });
        
        base.setupMinimizedCallback();
    };
    
    ///////////////////// EDITING HTML AND CSS //////////////////////////////
    
    function initEditor(ClassName, viewmodel, p_id, customData) {
        var id = p_id;
        
        var offset = $("#" + base.id).offset();
        offset.left += $("#" + base.id).width();
        customData.offset = offset;
        
        var editor = new ClassName([viewmodel], id, base.parentId, customData);
        editor.setDefaultZidx = function() {
            $("#" + id).css("z-index" , 2500);
        };
        editor.setZidx = function() {
            $("#" + id).css("z-index" , 3500);
        };
        editor.applicationCreated();
        ViewUtils.applyCss(customData, id);
        $("#" + id).on('click', function(){
            base.parent.setFocusedObj(editor);
        });
        $("#" + id).on('dragstart', function(){
            base.parent.setFocusedObj(editor);
        });
        base.parent.setFocusedObj(editor);
        editor.refresh(true);
        
        ViewUtils.jumpToEditor(id);
    }
    
    function saveCss(text) {
        base.viewmodel.setCss(text);
    }

    function editCss() {
        if (v_cssEditorOpen) {
            $("#" + v_cssEditorId).remove();
            v_cssEditorOpen = false;
        } else {
            var viewmodel = {
                "getTextData": base.viewmodel.getCss,
                "setTextData": saveCss
            }
            
            initEditor(CView_CssEditor, viewmodel, v_cssEditorId, v_customDataForCssEditor);
            $("#" + v_cssEditorId).on("remove", function() {
                v_cssEditorOpen = false;
            });
            v_cssEditorOpen = true;
        }
    }
    
    function htmlEditorClosed() {        
        v_htmlEditorOpen = false;
        var newIds = base.viewmodel.collectIds();
        
        base.createTree(function() {
            for (var i = 0; i < newIds.length; ++i) {
                if (newIds[i].prevIndex != -1 && v_ids[newIds[i].prevIndex].endpoint != undefined) {
                    newIds[i].endpoint = v_ids[newIds[i].prevIndex].endpoint;
                    newIds[i].endpoint.setPath([i]);
                    v_ids[newIds[i].prevIndex].endpoint = undefined;
                }
            }
                        
            for (var i = 0; i < v_ids.length; ++i) {
                if (v_ids[i].endpoint != undefined) {
                    base.parent.deleteEndpoint(v_ids[i].endpoint);
                    base.viewmodel.removeChildViewById(v_ids[i].id);
                }
            }
            
            v_ids = newIds;
                        
            base.refreshConnections();
        });
    }
    
    function saveHtml(text) {
        base.viewmodel.setHtml(text);
    }

    function editHtml() {
        if (v_htmlEditorOpen) {
            $("#" + v_htmlEditorId).remove();
            v_htmlEditorOpen = false;
        } else {
            var viewmodel = {
                "getTextData": base.viewmodel.getHtml,
                "setTextData": saveHtml
            };
            initEditor(CView_HtmlEditor, viewmodel, v_htmlEditorId, v_customDataForHtmlEditor);
            $("#" + v_htmlEditorId).on("remove", htmlEditorClosed);
            v_htmlEditorOpen = true;
        }
    }
    
    ///////////////////// CHECKING OWN EVENTS //////////////////////////////
    
    function isAllowedViewConnection(node, parent) {
        if (node.text === "Child of ..." && !base.jsTreeUtils.isNodeFromTree(node, base.tree) && base.jsTreeUtils.getDepth(parent, base.tree) === 1) {
            return true;
        } else {
            return false;
        }
    }
    
    function isAllowedImportConnection(node, parent) {
        if (node.text === "Import into ..." && !base.jsTreeUtils.isNodeFromTree(node, base.tree) && base.jsTreeUtils.getDepth(parent, base.tree) === 1) {
            return true;
        } else {
            return false;
        }
    }
    
    ///////////////////// HANDLING OWN EVENTS //////////////////////////////
    
    function deleteNode(p_data) {
        var treeNode = base.tree.jstree("get_node", p_data);
        var jqueryNode = $("#" + treeNode.id);
        var index = jqueryNode.parent().children().index(jqueryNode[0]);
        if (base.connectionsDeleted(index)) {
            base.viewmodel.removeChildView(index);
        }
        base.refreshConnections();
    }
    

    
    ///////////////////// HANDLING CONNECTIONS //////////////////////////////
    
    base.connectionAdded = function(index, endpoint) {
        v_ids[index].endpoint = endpoint;
        base.parent.addEndpoint(endpoint);
    };
    
    base.connectionsDeleted = function(connectionIndex) {
        if (v_ids[connectionIndex].endpoint != undefined) {
            base.parent.deleteEndpoint(v_ids[connectionIndex].endpoint);
            v_ids[connectionIndex].endpoint = undefined;
            return true;
        } else {
            return false;
        }
    };
    
    function ViewConnectionEndpoint(p_path, p_identifier) {
        var endpoint = new base.Endpoint(
            p_path,
            p_identifier,
            "target",
            "V_HTML",
            {
                "stroke": "#9ccc5c"
            },
            function(htmlObj) {
                var offset = htmlObj.offset();
                offset.top += htmlObj.height() / 2;
                return offset;
            }
        );
        
        endpoint.setPath = function(path) {
            endpoint.path = path;
        }
        
        return endpoint;
    }
    
    this.getEndpoints = function() {
        var list = [];
        for (var i = 0; i < v_ids.length; ++i) {
            if (v_ids[i].endpoint != undefined) {
                list.push(v_ids[i].endpoint);
            }
        }
        return list;
    };
}
//# sourceURL=GuiEditor\Views\View_HtmlEditor.js
