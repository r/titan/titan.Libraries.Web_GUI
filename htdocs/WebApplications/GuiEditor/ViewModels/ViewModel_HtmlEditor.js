// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_HtmlEditor_ViewModel(p_parentViewmodel) {
    "use strict";

    var v_parentViewmodel = p_parentViewmodel;
    var v_ids = [];
    var setup;
    
    ///////////////////// GENERAL FUNCTIONS /////////////////////
    
    this.setSetup = function(p_setup) {
        setup = p_setup;
        v_ids = [];
    };

    ///////////////////// GENERAL EDITOR FUNCTIONS /////////////////////

    this.getTreeData = function() {
        var data = [];
        for (var i = 0; i < v_ids.length; ++i) {
            data.push({"text" : v_ids[i].id});
        }
        return data;
    };

    this.getName = function() {
        return "Setup html";
    };

    this.getTooltip = this.getName;

    this.matches = function(string) {
        return JSON.stringify(setup.html.getData() + setup.css.getData()).toLowerCase().indexOf(string) != -1;
    };

    ///////////////////// GENERAL VIEW EDITOR FUNCTIONS /////////////////////

    this.getChildIds = function() {
        var list = [];
        for (var i = 0; i < v_ids.length; ++i) {
            list.push(v_ids[i].id);
        }
        return list;
    };

    this.removeChildView = function(p_index) {
        v_parentViewmodel.childRenamed(v_ids[p_index].id);
        v_parentViewmodel.setupChanged("Delete HTML connection");
    };

    this.removeConnectedChild = function(parentId) {
        v_parentViewmodel.removeConnectedChild(parentId);
    };

    ///////////////////// HTML EDITOR SPECIFIC FUNCTIONS /////////////////////

    this.getDesktopData = function() {
        return setup.desktop.getData()["HtmlEditor"];
    };

    this.removeChildViewById = function(p_id) {
        v_parentViewmodel.childRenamed(p_id);
    };

    this.getCss = function(callback) {
        callback(setup.css.getData());
    };

    this.setCss = function(text) {
        setup.css.setData(text);
        v_parentViewmodel.setupChanged("CSS changed");
    };

    this.getHtml = function(callback) {
        callback(setup.html.getData());
    };

    this.setHtml = function(text) {
        setup.html.setData(text);
        v_parentViewmodel.setupChanged("HTML changed");
    };

    this.collectIds = function() {
        var newIds = [];
        var str = setup.html.getData();
        var parser = new DOMParser();
        var doc = parser.parseFromString('<DIV>' + str + '</DIV>', "text/xml");
        var elements_with_id = doc.querySelectorAll("*[id]");
        for(var i = 0; i < elements_with_id.length; i++) {
            newIds.push({
                "id": elements_with_id[i].getAttribute("id"),
                "prevIndex": findIdIndex(elements_with_id[i].getAttribute("id"))
            });
        }
        v_ids = newIds;
        return v_ids;
    };

    function findIdIndex(id) {
        for (var i = 0; i < v_ids.length; ++i) {
            if (v_ids[i].id === id) {
                return i;
            }
        }
        return -1;
    }
}
//# sourceURL=GuiEditor\ViewModels\ViewModel_HtmlEditor.js