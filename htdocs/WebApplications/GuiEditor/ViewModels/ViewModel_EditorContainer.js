// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_EditorContainer_ViewModel(p_model, p_parent) {
    "use strict";

    var v_model = p_model;
    var v_parent = p_parent;
    var v_binder;

    var v_viewModelBoxes = [];
    var v_viewBoxes = [];
    var v_importBoxes = [];
    var v_htmlBox = new GuiEditor_HtmlEditor_ViewModel(this);
    var v_sanityChecker = new GuiEditor_SanityChecker_ViewModel(v_model, this);

    var v_this = this;

    ///////////////////// GENERAL FUNCTIONS /////////////////////

    this.setupChanged = function(how) {
        v_parent.setupChanged(how);
    };

    this.init = function(p_callback) {
        p_callback(true);
    };

    this.setBinder = function(p_binder) {
        v_binder = p_binder;
    };

    this.setSetup = function(setup) {
        v_htmlBox.setSetup(setup);
        v_sanityChecker.setSetup(setup);
    };

    ///////////////////// GETTER FOR SUBVIEWMODELS /////////////////////

    this.getSanityCheckerViewmodel = function() {
        return v_sanityChecker;
    };

    this.getHtmlEditorViewmodel = function() {
        return v_htmlBox;
    };

    ///////////////////// CREATING AND DELETING EDITORS /////////////////////

    this.createViewModelEditor_ViewModel = function(className, callback, customData) {
        function editorCreated(model, desktopData) {
            var viewmodel = new GuiEditor_ViewModelEditor_ViewModel(model, v_this);
            v_viewModelBoxes.push(viewmodel);
            callback(viewmodel, desktopData);
        }
        v_model.createViewModelEditor_Model(className, editorCreated, customData);
        v_parent.setupChanged("Viewmodel created");
    };

    this.deleteViewModelEditor_ViewModel = function(index) {
        v_viewModelBoxes.splice(index, 1);
        v_model.deleteViewModelEditor_Model(index);
        v_parent.setupChanged("Viewmodel deleted");
    };

    this.createViewEditor_ViewModel = function(className, callback, customData) {
        function editorCreated(model, desktopData) {
            var viewmodel = new GuiEditor_ViewEditor_ViewModel(model, v_this);
            v_viewBoxes.push(viewmodel);
            callback(viewmodel, desktopData);
        }
        v_model.createViewEditor_Model(className, editorCreated, customData);
        v_parent.setupChanged("View created");
    };

    this.deleteViewEditor_ViewModel = function(index) {
        var viewmodel = v_viewBoxes.splice(index, 1)[0];
        v_model.deleteViewEditor_Model(index);
        deleteIdConnections(viewmodel);
        v_parent.setupChanged("View deleted");
    };

    this.createImport_ViewModel = function(setupName, callback) {
        function editorCreated(model, desktopData) {
            var viewmodel = new GuiEditor_Imports_ViewModel(model, v_this);
            v_importBoxes.push(viewmodel);
            callback(viewmodel, desktopData);
        }
        v_model.createImport_Model(setupName, editorCreated);
        v_parent.setupChanged("Import created");
    };

    this.deleteImport_ViewModel = function(index) {
        var viewmodel = v_importBoxes.splice(index, 1)[0];
        v_model.deleteImport_Model(index);
        deleteIdConnections(viewmodel);
        v_parent.setupChanged("Import deleted");
    };

    this.createViewModelsFromExistingData = function(viewModelsCreated) {
        v_viewBoxes = [];
        v_viewModelBoxes = [];
        v_importBoxes = [];

        function modelsArrived(data) {
            function createViewModels(models, ClassToInit, listToAppendInto) {
                for (var i = 0; i < models.length; ++i) {
                    var model = models[i];
                    var viewmodel = new ClassToInit(model, v_this);
                    listToAppendInto.push(viewmodel);
                }
            }

            createViewModels(data.view.models, GuiEditor_ViewEditor_ViewModel, v_viewBoxes);
            createViewModels(data.viewmodel.models, GuiEditor_ViewModelEditor_ViewModel, v_viewModelBoxes);
            createViewModels(data.imports.models, GuiEditor_Imports_ViewModel, v_importBoxes);

            data.view.viewmodels = v_viewBoxes;
            data.viewmodel.viewmodels = v_viewModelBoxes;
            data.imports.viewmodels = v_importBoxes;

            viewModelsCreated(data);
        }

        v_model.getEditorModels(modelsArrived);
    };

    function deleteIdConnections(viewmodel) {
        var parentId = viewmodel.getParentId();
        v_this.removeConnectedChild(parentId);
        viewmodel.removeAllChildren();
    }

    ///////////////////// LISTING SETUPS, CLASSES AND FILES /////////////////////

    this.getViewClassNames = function(p_callback) {
        getResources(v_model.listViews, p_callback);
    };

    this.getViewmodelClassNames = function(p_callback) {
        getResources(v_model.listViewmodels, p_callback);
    };

    function getResources(modelFunction, callback) {
        var options = [];
        function resourcesListed(data) {
            var tasks = [];
            for (var i = 0; i < data.length; ++i) {
                tasks.push(new GetClassTask(data[i], options, i));
            }
            new TaskList(tasks, function() {callback(options);}).taskOperation();
        }
        modelFunction(resourcesListed);
    }

    function GetClassTask(file, options, index) {
        var v_file = file;
        var v_options = options;
        var v_index = index;
        var re = /^\s*function\s+(\w+)\s*\(/mi; // Match first function not commented out. Does not check for multiline comments!

        this.taskOperation = function(callback) {
            v_model.getFileHandler().loadFile(v_file, function(ok, content) {
                var matches = content.replace(/\/\*[^]*?\*\//g, '').match(re);
                if (matches != undefined && matches.length > 0) {
                    v_options[v_index] = {
                        "value" : matches[1],
                        "text" : matches[1],
                        "location": v_file
                    };
                }
                callback(true);
            });
        };
    };

    this.listSetups = function(callback) {
        function setupsListed(setups) {
            var options = [];
            for (var i = 0; i < setups.length; ++i) {
                options.push({
                    "value": setups[i],
                    "text": setups[i]
                });
            }
            callback(options);
        }

        v_model.listSetups(setupsListed);
    };

    this.sortOptions = v_model.sortOptions;

    ///////////////////// VIEW ID MAINTENANCE /////////////////////

    this.getViewEditorIndex = function(editor) {
        return v_viewBoxes.indexOf(editor);
    };

    this.removeConnectedChild = function(parentId) {
        for (var i = 0; i < v_viewBoxes.length; ++i) {
            var index = v_viewBoxes[i].getChildIds().indexOf(parentId);
            if (index != -1) {
                v_viewBoxes[i].removeChildView(index);
                v_binder.removeViewConnection(i, index);
                return;
            }
        }

        var index = v_htmlBox.getChildIds().indexOf(parentId);
        if (index != -1) {
            v_htmlBox.removeChildView(index);
            v_binder.removeViewConnection(-1, index);
        }
    };

    this.childRenamed = function(p_from, p_to) {
        var list = v_viewBoxes.concat(v_importBoxes);
        for (var i = 0; i < list.length; ++i) {
            if (list[i].getParentId() == p_from) {
                list[i].setParentId(p_to);
                break;
            }
        }
    };

    ///////////////////// SANITY CHECKER FUNCTIONS /////////////////////

    this.getHelp = v_sanityChecker.getHelp;

    this.isValidView = function(view) {
        var connectedViewmodels = [];
        var viewmodelIndexes = view.getViewModelIndexes();
        for (var i = 0; i < viewmodelIndexes.length; ++i) {
            connectedViewmodels.push(v_viewModelBoxes[viewmodelIndexes[i]]);
        }

        return v_sanityChecker.isValidView(view, connectedViewmodels);
    };

    this.isValidViewmodel = v_sanityChecker.isValidViewmodel;
    this.getCustomDataSchema = v_sanityChecker.getCustomDataSchema;
}
//# sourceURL=GuiEditor\ViewModels\ViewModel_EditorContainer.js