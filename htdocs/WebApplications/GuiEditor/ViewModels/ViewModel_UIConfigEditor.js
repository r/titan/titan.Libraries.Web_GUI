// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_UIConfigEditor_ViewModel(p_model) {
    "use strict";

    var v_model = p_model;

    var v_configList = [];
    var v_schemaPresent;
    var v_config;
    var v_schema;

    this.getName = function() {
        return v_config.getUrl();
    };

    this.listEditableConfigs = function(callback) {
        function configsListed(configs) {
            v_configList = configs;
            var result = [];
            for (var i = 0; i < configs.length; ++i) {
                result.push({
                    "value" : i,
                    "text" : configs[i].config
                });
            }
            callback(result);
        }

        v_model.listEditableConfigs(configsListed);
    };

    this.loadConfig = function(index, callback) {
        v_config = new JsonLoader(v_configList[index].config, v_model.getFileHandler(), {});
        v_schema = new JsonLoader(v_configList[index].schema, v_model.getFileHandler(), {});

        function schemaLoaded(ok, data) {
            if (ok) {
                v_schemaPresent = true;
            } else {
                v_schemaPresent = false;
            }
            callback(true);
        }

        function configLoaded(ok, data) {
            if (ok) {
                v_schema.taskOperation(schemaLoaded);
            } else {
                callback(false);
            }
        }

        v_config.taskOperation(configLoaded);
    };

    this.isSchemaPresent = function() {
        return v_schemaPresent;
    };

    this.getJSONData = function(callback) {
        callback(v_config.getData());
    };

    this.setJSONData = function(data) {
        v_config.setData(data);
    };

    this.getSchema = function() {
        return v_schema.getData();
    };

    this.getTextData = function(callback) {
        callback(JSON.stringify(v_config.getData(), null, 4));
    };

    this.setTextData = function(data) {
        v_config.setData(JSON.parse(data));
    };

    this.save = function() {
        v_config.save();
    };
}
//# sourceURL=GuiEditor\ViewModels\ViewModel_UIConfigEditor.js
