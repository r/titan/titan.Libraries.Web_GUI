// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_ViewEditor_ViewModel(p_model, p_parent) {
    "use strict";

    var v_model = p_model;
    var v_parent = p_parent;

    var v_this = this;

    ///////////////////// GENERAL EDITOR FUNCTIONS /////////////////////

    this.getTreeData = function() {
        var data = [
            {"text" : "Viewmodel connections", "children" : []},
            {"text" : "Child of ...", "children" : []},
            {"text" : "Parent of ...", "children" : []}
        ];

        var viewModelIndexes = v_model.getViewModelIndexes();
        for (var i = 0; i < viewModelIndexes.length; ++i) {
            data[0].children.push({"text" : "ViewModel " + viewModelIndexes[i]});
        }

        var childIds = v_model.getChildIds();
        for (var i = 0; i < childIds.length; ++i) {
            data[2].children.push({"text" : "Child view " + i});
        }

        return data;
    };

    this.getName = function() {
        var name = v_model.getClassName();
        if (v_model.getCustomData().name != undefined) {
            name += " - " + v_model.getCustomData().name;
        }
        return name;
    };

    this.getTooltip = function() {
        return this.getName() + "\n" + v_parent.getHelp(v_model.getClassName()) + "\nCurrent custom data:\n" + JSON.stringify(v_model.getCustomData(), null, 4);
    };

    this.matches = function(string) {
        return JSON.stringify(v_model.getDescriptorCopy()).toLowerCase().indexOf(string) != -1;
    };

    ///////////////////// GENERAL VIEW AND VIEWMODEL EDITOR FUNCTIONS /////////////////////

    this.getCustomData = v_model.getCustomData;
    this.setCustomData = function(data) {
        v_model.setCustomData(data);
        v_parent.setupChanged("Custom data changed");
    };

    this.getClass = v_model.getClassName;
    this.setClass = function(p_class) {
        v_model.setClass(p_class);
        var childIds = v_model.getChildIds();
        for (var i = 0; i < childIds.length; ++i) {
            renameChildId(i, generateChildId(i));
        }
        v_parent.setupChanged("Class changed");
    };

    this.getCustomDataSchema = function() {
        return v_parent.getCustomDataSchema(v_model.getClassName());
    };

    this.isValid = function() {
        return v_parent.isValidView(v_this);
    };

    ///////////////////// VIEW EDITOR SPECIFIC FUNCTIONS /////////////////////

    this.addViewModelIndex = function(p_viewModelIndex, p_index) {
        v_model.addViewModelIndex(p_viewModelIndex, p_index);
        v_parent.setupChanged("Viewmodel connected");
    };
    this.deleteViewModelIndexFromPosition = function(p_index) {
        v_model.deleteViewModelIndexFromPosition(p_index);
        v_parent.setupChanged("Viewmodel connection deleted");
    };
    this.viewModelDeleted = v_model.viewModelDeleted;
    this.connectionOrderChanged = function(fromIndex, toIndex) {
        v_model.connectionOrderChanged(fromIndex, toIndex);
        v_parent.setupChanged("Connection order changed");
    };

    this.getViewModelIndexes = v_model.getViewModelIndexes;

    function generateChildId(index) {
        if (v_model.getParentId() != undefined) {
            return v_model.getParentId() + "_" + v_model.getClassName() + "_" + index;
        } else {
            return v_parent.getViewEditorIndex(v_this) + "_" + v_model.getClassName() + "_" + index;
        }
    }

    this.getParentId = v_model.getParentId;
    this.getChildIds = v_model.getChildIds;

    this.setParentId = function(p_id) {
        v_model.setParentId(p_id);
        var childIds = v_model.getChildIds();
        for (var i = 0; i < childIds.length; ++i) {
            renameChildId(i, generateChildId(i));
        }
        v_parent.setupChanged("Parent id changed");
    };

    function renameChildId(p_index, p_to) {
        var origName = v_model.getChildIds()[p_index];
        v_model.renameChildId(p_index, p_to);
        v_parent.childRenamed(origName, p_to);
    }

    function updateChildId(p_index, p_to) {
        var origName = v_model.getChildIds()[p_index];
        // we do not actually rename it in this case
        v_parent.childRenamed(origName, p_to);
    }

    this.addChildView = function(p_index) {
        var fromIndex = v_model.getChildIds().length;
        var id = generateChildId(fromIndex);

        v_model.addChildView(id);
        v_this.childViewOrderChanged(fromIndex, p_index);

        return generateChildId(p_index);
    };

    this.removeChildView = function(p_index) {
        var toIndex = v_model.getChildIds().length - 1;

        var origName = v_model.getChildIds()[toIndex];

        v_this.childViewOrderChanged(p_index, toIndex);
        v_parent.childRenamed(origName);

        v_model.removeChildView();
    };

    this.removeAllChildren = function() {
        var childIds = v_model.getChildIds();
        for (var i = 0; i < childIds.length; ++i) {
            v_parent.childRenamed(childIds[i]);
        }
    };

    this.childViewOrderChanged = function(fromIndex, toIndex) {
        if (toIndex < fromIndex) {
            updateChildId(toIndex, "temp");
            updateChildId(fromIndex, generateChildId(toIndex));
            for (var i = fromIndex - 1; i > toIndex; --i) {
                updateChildId(i, generateChildId(i + 1));
            }
            v_parent.childRenamed("temp", generateChildId(toIndex + 1));
        } else if (fromIndex < toIndex) {
            updateChildId(toIndex, "temp");
            updateChildId(fromIndex, generateChildId(toIndex));
            for (var i = fromIndex + 1; i < toIndex; ++i) {
                updateChildId(i, generateChildId(i - 1));
            }
            v_parent.childRenamed("temp", generateChildId(toIndex - 1));
        } else {
            updateChildId(toIndex, generateChildId(toIndex));
        }
        v_parent.setupChanged("Connection order changed");
    };

    this.wouldCreateACycle = function(newParentId) {
        var childIds = v_model.getChildIds();
        for (var i = 0; i < childIds.length; ++i) {
            if (newParentId.startsWith(childIds[i])) {
                return true;
            }
        }

        var currentParentId = v_model.getParentId();
        if (currentParentId != undefined && getPrefixOfParentId(currentParentId) == getPrefixOfParentId(newParentId)) {
            return true;
        }

        return false;
    };

    function getPrefixOfParentId(parentId) {
        var index = parentId.lastIndexOf("_");
        if (index != -1) {
            return parentId.substr(0, index);
        } else {
            return parentId;
        }
    }

    this.wouldUseId = function(index) {
        return generateChildId(index);
    };

    this.removeConnectedChild = function(parentId) {
        v_parent.removeConnectedChild(parentId);
    };
}
//# sourceURL=GuiEditor\ViewModels\ViewModel_ViewEditor.js