// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function RequestConsole_View(p_viewModel, p_parentId, p_viewId) {
    "use strict";

    var HTML = "WebApplications/RequestConsole/Views/View.html";

    var parentDiv = document.getElementById(p_parentId);
    var v_parentId = p_parentId;
    var v_viewId = p_viewId;
    var v_viewmodel = p_viewModel;
    var v_this = this;

    var v_requestEditorView = new RequestConsole_RequestEditor_View(v_viewmodel, v_this);
    var v_responseDisplay = new CView_CodeEditor([p_viewModel.getResponseEditorViewModel()], 'RequestConsole_ResponseDisplay_', 'RequestConsole_ResponseDisplay', {"editorType": "json", "formatAllowed": true, "headerText": "Response text:"});
    var v_autoGuiView = new CAutoGuiView([v_viewmodel.getAutoGuiViewModel()], "autogui", "RequestConsole_Button_AutoGui");
    var v_focused_obj;

    ///////////////////// GETTER FOR SUBVIEWS //////////////////////////////

    this.getRequestEditorView = function() {
        return v_requestEditorView;
    };
    
    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.init = function(p_callback) {
        var mainDiv = document.createElement("div");
        mainDiv.setAttribute("id", v_viewId);
        parentDiv.appendChild(mainDiv);

        function htmlLoaded(ok, data) {
            if (ok) {
                $("#" + v_viewId).append(data);
                $("#" + v_viewId).on("keydown", keyPressed);
                $("#" + v_viewId).trigger("resize");
                $("#RequestConsole_WebAppStyle").load("WebApplications/RequestConsole/Views/View.css", function() {
                    v_this.toggleButtons(false);
                    p_callback(true);
                });
            } else {
                p_callback(false, "Error loading " + HTML);
            }
        }

        v_viewmodel.loadFile(HTML, htmlLoaded);
    };

    function onWindowResize(event) {
        if (event.target == window) {
            $("#RequestConsole_MainView").height(ViewUtils.getSuggestedHeight("RequestConsole_MainView"));
        }
    }

    this.applicationCreated = function() {
        v_requestEditorView.applicationCreated();
        $("#RequestConsole_Button_New").on("click", newSetup);
        $("#RequestConsole_Button_Load").on("click", switchSetup);
        $("#RequestConsole_Button_History").on("click", showHistory);
        $("#RequestConsole_Button_SetApplication").on("click", changeApplication);
        $("#RequestConsole_LegendToggle").on("click", function() {
            $("#RequestConsole_LegendTable").toggleClass("hidden");
        });
        $("#RequestConsole_Button_Send").on('click', sendRequest);
        $("#RequestConsole_Button_GenerateRqFromHelp").on('click', sendFullRequest);
        $("#RequestConsole_Button_ClearRequest").on('click', clearRequest);
        $(document).on("keydown", keyPressed);

        $(window).on("resize", onWindowResize);
        $("#RequestConsole_MainView").height(ViewUtils.getSuggestedHeight("RequestConsole_MainView"));

        v_responseDisplay.applicationCreated();
        v_responseDisplay.refresh(true);
        v_autoGuiView.applicationCreated();
    };

    this.destroy = function() {
        v_requestEditorView.destroy();
        $("#" + v_viewId).remove();
        $(document).off("keydown", keyPressed);
        $(window).off("resize", onWindowResize);
    };

    this.unload = function(p_callback) {
        if (v_viewmodel.isSetupChanged() == true && v_viewmodel.getAppConfig().confirmExit == true) {
            var exitDialog = new ExitDialog(v_viewId, "RequestConsole_Dialog_Exit", {
                "header": "Exit RequestConsole",
                "text": "Do you wish to save changes before leaving?",
                "callback": function(exit, save) {
                    p_callback(exit);
                }
            });
            exitDialog.open();
        } else {
            p_callback(true);
        }
    };

    this.refreshResponseDisplays = function() {
        v_responseDisplay.refresh(true);
        v_autoGuiView.refresh(true);
        document.getElementById("RequestConsole_Time").innerHTML = v_viewmodel.getTime();
    }

    ///////////////////// EVENT HANDLING FUNCTIONS //////////////////////////////

    function keyPressed(event) {
        if(event.keyCode === 46 && v_focused_obj != undefined && v_focused_obj.deletePressed != undefined) {
            v_focused_obj.deletePressed();
        }
    }

    function newSetup() {
        v_this.toggleButtons(false);
        v_viewmodel.newSetup();
        v_this.updateSetupName();
        v_focused_obj = undefined;
    }

    function deleteSetup(value) {
        var text = "Are you sure your want to delete setup " + value + "?";
        if (v_viewmodel.isCurrentlyEdited(value)) {
            text += "<br><b>Warning! This is the currently edited setup<b>";
        }

        function setupDeleted(ok) {
            if (!ok) {
                alert("Failed to delete setup " + value);
            }
            v_this.updateSetupName();
            switchSetup();
        }

        var dialog = new ConfirmationDialog(v_viewId, "RequestConsole_Dialog_DeleteSetup", {
            "header": "Delete setup",
            "text": text,
            "callback": function() {
                v_viewmodel.deleteSetup(value, setupDeleted);
            }
        });
        dialog.open();
    }

    function switchSetup() {
        function gotSetupName(p_setup) {
            v_this.toggleButtons(false);
            v_viewmodel.switchSetup(p_setup,
                function callback(ok) {
                    if (!ok) {
                        alert("Loading setup failed");
                    }
                    v_this.updateSetupName();
                    v_focused_obj = undefined;
                }
            );
        }

        function optionsArrived(options) {
            var dialog = new ChoiceDialogWithButton(v_viewId, "RequestConsole_Dialog_LoadSetup", {
                "header": "Select setup",
                "text": "Please select a setup from the table below.",
                "choices": options,
                "callback": gotSetupName,
                "buttonHandler": deleteSetup,
                "buttonText": "X",
                "buttonStyle": "color: red;",
                "closeOnButtonPress": true,
                "searchFunction": v_viewmodel.globalSetupSearch
            });
            dialog.open();
        }

        v_viewmodel.listSetups(optionsArrived);
    }

    function changeApplication() {
        function gotApplicationName(p_application) {
            v_viewmodel.setEditedApp(p_application, function() {
                newSetup();
            });
        }

        function optionsArrived(options) {
            var dialog = new ChoiceDialog(v_viewId, "RequestConsole_Dialog_SetApplication", {
                "header": "Select application",
                "text": "Please select an application from the table below.",
                "choices": options,
                "callback": gotApplicationName
            });
            dialog.open();
        }

        v_viewmodel.listEditableApps(optionsArrived);
    }

    ///////////////////// USEFUL FUNCTION FOR VIEWS //////////////////////////////

    this.toggleButtons = function(on) {
        $(".RequestConsole_Button_Left").prop("disabled", !on);
        $(".RequestConsole_Button_Right").prop("disabled", !on);
    };

    this.setFocusedObj = function(p_object) {
        if (v_focused_obj != undefined) {
            v_focused_obj.setDefaultZidx();
        }
        if (p_object != undefined) {
            p_object.setZidx();
        }
        v_focused_obj = p_object;
    };

    this.updateSetupName = function() {
        var config = v_viewmodel.getAppConfig();
        if (config.lastEditedApp != undefined) {
            document.getElementById("RequestConsole_AppNameLabel").innerHTML = config.lastEditedApp;
        } else {
            document.getElementById("RequestConsole_AppNameLabel").innerHTML = "... undefined application ...";
        }
        if (config.lastEditedSetup != undefined) {
            document.getElementById("RequestConsole_SetupNameLabel").innerHTML = config.lastEditedSetup;
        } else {
            document.getElementById("RequestConsole_SetupNameLabel").innerHTML = "... unsaved setup ...";
        }
    };

    ///////////////////// HISTORY //////////////////////////////

    function HistoryElement(index, element) {
        var v_index = index;
        this.text = element.text;
        this.callback = function() {
            if (v_index != v_viewmodel.getCurrentPositionInHistory()) {
                v_viewmodel.rewind(v_index);
            }
        };
    }

    function HistoryClass(history) {
        var v_history = history;
        this.getMenuElements = function() {
            var contextItems = [];
            for (var i = 0; i < v_history.length; ++i) {
                contextItems.unshift(new HistoryElement(i, v_history[i]));
            }
            return contextItems;
        };
    }

    function showHistory(event) {
        v_viewmodel.historyEnabled(false);
        var history = v_viewmodel.getHistory();
        var contextMenuViewmodel = new HistoryClass(history);
        var contextParentId = "RequestConsole_Buttonbar";
        var contextId = "RequestConsole_History";
        var customDataForContextMenu = {
            "offset": $(this).offset()
        };
        var contextMenu = new CView_ContextMenu([contextMenuViewmodel], contextId, contextParentId, customDataForContextMenu);
        contextMenu.applicationCreated();
        $("#" + contextId).on("remove", function() {
            v_viewmodel.historyEnabled(true);
        });
        $($("#" + contextId + " li")[history.length - v_viewmodel.getCurrentPositionInHistory() - 1]).css("color", "green");
        $("#" + contextId + " li").css("padding-left", "65px");
        $("#" + contextId + " li").css("background-color", "rgba(197,197,197,0.9)");
    }
    
    function refreshScroll() {
        $("html, body").animate({ scrollTop: $("#RequestConsole_Button_Send").offset().top - 4}, "fast");
    }

    function sendRequest() {
        v_viewmodel.sendRequest(refreshScroll);
    };
    
    function sendFullRequest() {
        var request = DSHelpToRequest_full(v_viewmodel.getHelp());
        v_viewmodel.setTextData(JSON.stringify(request), 'Request generation from full help');
        v_viewmodel.modelChanged();
    };

    function clearRequest() {
        v_viewmodel.setTextData('[]', 'Request cleared');
        v_viewmodel.modelChanged();
    };
}
//# sourceURL=RequestConsole\Views\View.js
