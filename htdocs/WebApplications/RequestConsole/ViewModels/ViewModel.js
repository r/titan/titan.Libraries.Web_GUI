// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function RequestConsole_ViewModel_Response() {
    var v_data = undefined;
    
    this.getTextData = function(callback) {
        if (v_data)
            callback(JSON.stringify(v_data, null, 4));
        else
            callback("{}")
    };

    this.setTextData = function(data) {
        v_data = mcopy(data);
    };
}

function RequestConsole_ViewModel(p_model) {
    "use strict";

    var v_binder;
    var v_setup;
    var v_model = p_model;
    var v_setupName;
    var v_setupChanged = false;
    var v_requestEditorViewModel = new RequestConsole_RequestEditor_ViewModel(v_model, this);
    var v_responseEditorViewModel = new RequestConsole_ViewModel_Response(v_model, this);
    var v_time = 0;
    var v_lastCodeEditorChange = 0;
    //var v_lastCodeEditorStateSave = 0;
    setInterval(rebuildTreeBecauseOfCodeEditorChange, 100);

    /////////// AUTO GUI //////////

    var dataSourceUtils = new DataSourceUtils();   
    var v_response;
    var v_autogui_ViewModel = new CViewModel_AutoGUI({
        "getResponseElement" : function() {
            return dataSourceUtils.expandResponse(v_response, 0, [], [], v_setup.request.getData());
        },
        "getDsRestAPI": function() {
            return v_model.getDsRestAPI();
        }
    });
    
    this.getHelp = function(callback) {
        return v_help;
    }

    /////////// HISTORY ////////////

    var v_history = [];
    var v_nextHistoryEntry = 0;
    var v_currentState;
    var v_changes = {};
    var v_historyEnabled = true;
    var v_help;

    var v_this = this;

    ///////////////////// GETTER FOR SUBVIEWMODELS /////////////////////

    this.getRequestEditorViewModel = function() {
        return v_requestEditorViewModel;
    };
    
    this.getResponseEditorViewModel = function() {
        return v_responseEditorViewModel;
    };

    this.getAutoGuiViewModel = function() {
        return v_autogui_ViewModel;
    }

    ///////////////////// GENERAL VIEWMODEL FUNCTIONS /////////////////////

    this.init = function(p_callback) {
        function viewmodelsInitilaized() {
            // we need to load the setup as the config loads with it, and we need that to know the locations of views and viewmodels for customizable app
            p_callback(true);
            resetHistory();
        }

        var taskList = new TaskList([new GenericTask(v_requestEditorViewModel.init)], viewmodelsInitilaized);
        taskList.taskOperation();
        
        function helpArrived(ok, help) {
            if (ok) {
                v_help = new HelpTreeBuilder(help).getHelpTree(true);
            } else {
            }
        }
        v_model.getDsRestAPI().getHelp(helpArrived);
    };

    this.destroy = function() {
    }

    this.setBinder = function(p_binder) {
        v_binder = p_binder;
        v_requestEditorViewModel.setBinder(p_binder);
        v_autogui_ViewModel.setBinder(p_binder);
    };

    this.loadFile = v_model.getFileHandler().loadFile;
    this.loadCss = v_model.getFileHandler().loadCss;
    this.getAppConfig = v_model.getAppConfig;

    ///////////////////// HANDLING SETUP AND APPLICATION CHANGED /////////////////////

    this.modelChanged = function() {
        if (v_binder)
            v_binder.modelChanged();
    }

    this.isSetupChanged = function() {
        return v_setupChanged;
    };

    this.setupChanged = function(how) {
        v_setupChanged = true;
        if (how != undefined && v_changes[how] == undefined) {
            v_changes[how] = true;
        }
        v_binder.modelChanged();
    };

    this.setEditedApp = v_model.setEditedApp;

    this.listEditableApps = function(callback) {
        var result = [];

        function appsListed(apps) {
            for (var i = 0; i < apps.length; ++i) {
                result.push({
                    "value" : apps[i],
                    "text" : apps[i]
                });
            }
            callback(result);
        }

        v_model.listEditableApps(appsListed);
    };


    ///////////////////// SETUP HANDLING FUNCTIONS /////////////////////

    this.newSetup = function() {
        var setup = v_model.newSetup();
        v_this.setSetup(setup);
        v_binder.fullRefresh();
        v_setupChanged = false;
        resetHistory();
        v_this.modelChanged();
    };

    this.deleteSetup = function(directory, callback) {
        function setupDeleted(ok) {
            if (ok && directory == v_setupName) {
                v_setupName = undefined;
            }
            callback(ok);
        }
        v_model.deleteSetup(directory, setupDeleted);
    };

    this.switchSetup = function(selected, callback) {
        function setupLoaded(ok, setup, setupName) {
            v_this.setSetup(setup);
            v_binder.fullRefresh();
            if (ok) {
                v_setupChanged = false;
            }
            v_this.modelChanged();
            resetHistory();
            callback(ok);
        }
        v_model.switchSetup(selected, setupLoaded);
    };

    this.listSetups = function(p_callback) {
        function setupsLoaded(setups) {
            var options = [];
            for (var i = 0; i < setups.length; ++i) {
                options.push({
                    "value" : setups[i],
                    "text" : setups[i]
                });
            }
            p_callback(options);
        }

        //v_model.resetSetupDir();
        v_model.listSetups(setupsLoaded);
    };

    this.saveSetup = function(callback) {
        v_model.saveSetup(callback);
        v_setupChanged = false;
    };

    this.saveSetupAs = function(name, callback) {
        function setupSaved(ok) {
            if (ok) {
                v_setupName = name;
                v_setupChanged = false;
            }
            callback(ok);
        }

        v_model.saveSetupAs(name, setupSaved);
    };

    this.setupExists = v_model.setupExists;
    this.globalSetupSearch = v_model.globalSetupSearch;

    this.isCurrentlyEdited = function(name) {
        return v_setupName == name;
    };

    this.isSaveable = function() {
        return v_setupName != undefined;
    };

    this.getSetupName = function() {
        return v_setupName;
    };
    
    this.getTextData = function(callback) {
        callback(JSON.stringify(v_setup.request.getData(), null, 4));
    };

    this.setTextData = function(data, changeText) {
        v_setup.request.setData(JSON.parse(data));
        this.setSetup(v_setup);
        if (changeText) {
            v_this.saveState(changeText);
            v_binder.fullRefresh();
        }
        var nowdate = Date.now();
        /*if (!v_lastCodeEditorChange && nowdate - v_lastCodeEditorStateSave > 500) {
            v_binder.fullRefresh();
            v_this.saveState("Text editor changes");            
        }*/
        v_lastCodeEditorChange = nowdate;
    };

    this.setSetup = function(setup) {
        v_setup = setup;
        v_setupName = setup.name;
        v_requestEditorViewModel.setSetup(setup);
        v_requestEditorViewModel.setDesktopData(v_model.getDesktopDataForRequestEditor());
    }

    ///////////////////// HISTORY /////////////////////

    function rebuildTreeBecauseOfCodeEditorChange() {
        var nowdate = Date.now();
        if (v_lastCodeEditorChange && (nowdate - v_lastCodeEditorChange) > 500) {
            var stateChanged = v_this.saveState("Text editor changes");
            if (stateChanged)
                v_binder.fullRefresh();
            v_lastCodeEditorChange = 0;
            //v_lastCodeEditorStateSave = nowdate;
        }
    }

    function createState() {
        var state = {};
        state.request = mcopy(v_setup.request.getData());
        state.viewmodels = mcopy(v_setup.viewmodels.getData());
        state.views = mcopy(v_setup.views.getData());
        state.imports = mcopy(v_setup.imports.getData());
        state.desktop = mcopy(v_setup.desktop.getData());
        state.html = v_setup.html.getData();
        state.css = v_setup.css.getData();
        return state;
    }

    function getStateAsString() {
        var state = createState();
        state.desktop = undefined;
        return JSON.stringify(state);
    }

    function getChangeText(changes) {
        var text = "";
        var count = 0;
        for (var key in changes) {
            ++count;
            text += key + ", "
        }
        text = text.slice(0, text.length - 2);

        if (count == 0) {
            text = "Minor changes";
        }
        return text;
    }

    this.saveState = function(changeText) {
        var retval = false;
        if (v_historyEnabled) {
            var state = createState();
            var stateString = getStateAsString();
            if (stateString != v_currentState) {
                if (v_history[v_nextHistoryEntry] != undefined) {
                    v_history = v_history.slice(0, v_nextHistoryEntry);
                }
                if (changeText == undefined)
                    state.text = getChangeText(v_changes);
                else
                    state.text = changeText;
                v_history[v_nextHistoryEntry] = state;
                ++v_nextHistoryEntry;
                v_currentState = stateString;
                retval = true;
            }
            v_changes = {};
        }
        return retval;
    }

    function resetHistory() {
        v_nextHistoryEntry = 1;
        var state = createState();
        state.text = "History Beginning";
        v_history = [state];
        v_currentState = getStateAsString();
    }

    this.getHistory = function() {
        return v_history;
    };

    this.getCurrentPositionInHistory = function() {
        return v_nextHistoryEntry - 1;
    }

    this.historyEnabled = function(enabled) {
        v_historyEnabled = enabled;
    };

    this.rewind = function(to) {
        if (to < v_history.length) {
            var state = v_history[to];
            v_nextHistoryEntry = to + 1;
            changeState(state);
        }
    };

    function changeState(state) {
        state = mcopy(state);
        v_setup.request.setData(state.request);
        v_setup.viewmodels.setData(state.viewmodels);
        v_setup.views.setData(state.views);
        v_setup.imports.setData(state.imports);
        v_setup.desktop.setData(state.desktop);
        v_setup.html.setData(state.html);
        v_setup.css.setData(state.css);
        v_this.setSetup(v_setup);
        v_currentState = getStateAsString();
        v_setupChanged = true;
        v_binder.fullRefresh();
        v_binder.modelChanged();
    }
    
    this.sendRequest = function(refresh) {
        function responseArrived(response) {
            v_time = Date.now() - v_time;
            v_responseEditorViewModel.setTextData(response);
            v_response = response;
            v_binder.responseArrived();
            if (refresh)
                refresh();
        }
        v_time = Date.now();
        v_model.getDsRestAPI().getList(v_setup.request.getData(), responseArrived);
    };
    
    this.getTime = function() {
        return "Roundtrip time: " + v_time + " ms.";
    };
}
//# sourceURL=RequestConsole\ViewModels\ViewModel.js
