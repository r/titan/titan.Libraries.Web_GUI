// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
var WebApplications = WebApplications || [];

WebApplications.push({"application": new WebpageFrame_Application()});

function WebpageFrame_Application() {

    var v_appBase = new WebAppBase();
    var v_location;
    var v_sendCredentials = false;

    this.info = function() {
        return {
            defaultIcon: "WebApplicationFramework/Res/help.png",
            defaultName: "WebpageFrame"
        };
    };

    this.load = function(p_webAppModel, p_params, p_framework)  {
        v_location = p_params.location;
        v_sendCredentials = p_params.sendCredentials;
        v_appBase.load([], [], start, p_webAppModel.getFileHandler());
    };

    this.unload = function(webappUnloaded) {
        v_appBase.unload(destroy);
        webappUnloaded(true);
    };

    function onWindowResize(event) {
        if (event.target == window) {
            $("#WebpageFrame").height(ViewUtils.getSuggestedHeight("WebpageFrame"));
        }
    }

    function start(callback) {
        if (v_location != undefined) {
            var html = '<iframe src="' + v_location + '" id="WebpageFrame" width="100%"></iframe>';
            $("#TSGuiFrameworkMain").append(html);

            $(window).on("resize", onWindowResize);
            $("#WebpageFrame").height(ViewUtils.getSuggestedHeight("WebpageFrame"));

            if (v_sendCredentials) {
                $("#WebpageFrame").load(function() {
                    this.contentWindow.postMessage({'username': localStorage.username, 'password': localStorage.password}, '*');
                });
            }
        }
        callback(true);
    }

    function destroy() {
        $(window).off("resize", onWindowResize);
        $("#WebpageFrame").remove();
    }
}