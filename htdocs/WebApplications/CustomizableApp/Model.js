// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CCustomizableApp_Model(p_webAppModel, p_applicationFramework) {
    "use strict";

    var m_webAppModel = p_webAppModel;
    var m_DsRestApi = new DsRestAPI(m_webAppModel.getAppConfig().apiExtension);
    var m_applicationFramework = p_applicationFramework;

    this.getWebAppModel = function() {
        return m_webAppModel;
    };

    this.getDsRestApi = function() {
        return m_DsRestApi;
    };

    this.setAppParam = m_applicationFramework.setAppParam;

    this.loadApp = m_applicationFramework.loadApp;
}