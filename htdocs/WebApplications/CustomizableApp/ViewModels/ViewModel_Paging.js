// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_Paging(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    
    var v_binder;
    var v_dataPaths = [];
    var v_selections = [];
    
    /** public functions - interface for parent */
    
    this.setSelectionToControl = function(p_selection) {
        v_selections.push(p_selection);
    };
    
    this.setReponseDataPath = function(p_index, p_path) {
        v_dataPaths[p_index] = p_path;
    };
    
    this.setBinder = function(p_binder) {
        v_binder = p_binder;
    };
	
	/** public functions - interface for views */ 
    
    this.getSize = function() {
        var response = v_viewmodel.getResponseElement(v_dataPaths[0]);
        if (response != undefined && response.node != undefined) {
            return response.node.val;
        } else {
            return 0;
        }
    };
    
    this.getRangeFilter = function() {
        var size = this.getSize();
        var filter = v_viewmodel.getRequestFromPath(v_dataPaths[1]).getData.rangeFilter;
        
        if (filter == undefined) {
            return {
                "offset": 0,
                "count": size
            };
        } else {
            var offset = 0;
            if (filter.offset != undefined) {
                offset = filter.offset;
            }
            if (offset >= size) {
                offset = Math.max(0, size - 1);
            }
            
            var count;
            if (filter.count != undefined) {
                count = filter.count;
            } else {
                count = size - offset;
            }

            return {
                "offset": offset,
                "count": count
            }
        }
    };
    
    this.changeFilter = function(obj) {
        var getData = v_viewmodel.getRequestFromPath(v_dataPaths[1]).getData;
        var filter = getData.rangeFilter;;
        var size = this.getSize();
        
        if (obj == undefined) {
            getData.rangeFilter = undefined;
            return;
        } else if (filter == undefined) {
            filter = {};
            getData.rangeFilter = filter;
        }
        
        if (obj.offset != undefined) {
            if (obj.offset.to != undefined) {
                filter.offset = obj.offset.to;
            } else if (obj.offset.by != undefined) {
                if (filter.offset != undefined) {
                    filter.offset += obj.offset.by;
                } else {
                    filter.offset = obj.offset.by;
                }
            }
            if (filter.offset < 0) {
                filter.offset = 0;
            } else if (filter.count != undefined && v_options.allowScrollBelow == false && filter.offset > size - filter.count) {
                filter.offset = Math.max(size - filter.count, 0);
            } else if (filter.offset > size - 1) {
                filter.offset = Math.max(size - 1, 0);
            }
        }
        
        if (obj.count != undefined) {
            if (obj.count.to != undefined) {
                filter.count = obj.count.to;
            } else if (obj.count.by != undefined && filter.count != undefined) {
                filter.count += obj.count.by;
            }
            
            if (filter.count < 1) {
                filter.count = 1;
            }
        }
    };
    
    this.hasFilter = function() {
        return v_viewmodel.getRequestFromPath(v_dataPaths[1]).getData.rangeFilter != undefined;
    }
}

CViewModel_Paging.getHelp = function() {
    return "A viewmodel that can manipulate the rangeFilter of a request.";
}

CViewModel_Paging.providesInterface = function() {
    return ["getSize", "getRangeFilter", "changeFilter"];
};

CViewModel_Paging.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_Paging",
        "type": "object",
        "properties": {
            "allowScrollBelow": {
                "description": "Whether we want to allow paging to the very bottom (so that only one element remains unfiltered). Set it to false if the connected view can show every element.",
                "type": "boolean",
                "format": "checkbox",
                "default": false
            }
        },
        "additionalProperties": false
    };
};

CViewModel_Paging.expectsConnection = function() {
    var dataConnections = [
        {
            "source": "DataSource",
            "element": "sizeOf"
        },
        {
            "valueType": ["charstringlistType", "integerlistType", "floatlistType"]
        }
    ];
    
    return {
        "dataConnections": dataConnections,
        "selectionConnections": []
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_Paging.js
