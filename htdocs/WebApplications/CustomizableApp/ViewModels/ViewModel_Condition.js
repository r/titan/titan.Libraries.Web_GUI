// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_Condition(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    
    var v_binder;
    var v_dataPaths = [];
    var v_selections = [];
    
    /** public functions - interface for parent */
    
    this.setSelectionToControl = function(p_selection) {
        v_selections.push(p_selection);
    };
    
    this.setReponseDataPath = function(p_index, p_path) {
        v_dataPaths[p_index] = p_path;
    };
    
    this.setBinder = function(p_binder) {
        v_binder = p_binder;
    };
	
    /** public functions - interface for views */ 
    
    this.getState = function() {
        var returnValue;
        var request = v_viewmodel.getRequestFromPath(v_dataPaths[0]);
        var response = v_viewmodel.getResponseElement(v_dataPaths[0]);
        if (response != undefined && response.node != undefined) {
            if (response.node.tp == 0) {
                returnValue = false;
            } else if ((response.node.tp == 3 || response.node.tp == -3) && request.getData.filter == undefined) {
                returnValue = response.node.val === "true";
            } else {
                returnValue = true;
            }
            if (v_options.negate) {
                returnValue = !returnValue;
            }
        } else if (response != undefined && response.list != undefined) {
            returnValue = response.list.length != 0;
            if (v_options.negate) {
                returnValue = !returnValue;
            }
        } else {
            returnValue = false;
        }
        
        return returnValue;
    };
}

CViewModel_Condition.getHelp = function() {
    return "This viewmodel can process a response element that can be used for conditions.\n" +
        "If the element was filtered, the returned value is false.\n" +
        "If the element does not contain a filter and it is a boolean value, the returned value will be the element's value.\n" +
        "If the element is an empty list, it will be false.\n" +
        "Otherwise, it is true";
}

CViewModel_Condition.providesInterface = function() {
    return ["getState"];
};

CViewModel_Condition.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_Condition",
        "type": "object",
        "properties": {
            "negate": {
                "description": "whether we want to negate the condition",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            }
        },
        "additionalProperties": false
    };
};

//# sourceURL=CustomizableApp\ViewModels\CViewModel_Condition.js