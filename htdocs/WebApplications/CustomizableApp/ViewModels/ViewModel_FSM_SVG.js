// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////

function CViewModel_FSM_SVG(p_viewmodel, p_options) {
    "use strict";

    /** constructor */

    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    var v_dataPaths = [];
    var mDecodedSVGs = {};
    
    /** public functions - interface for parent */

    this.setSelectionToControl = function(p_selection) {};
    this.setBinder = function(p_binder) {};
    this.setReponseDataPath = function(p_index, p_path) {
        v_dataPaths[p_index] = p_path;
    };

    /** public functions - interface for views */
    
    this.getListWithElementInfo = function() {
        var lValues = [];
        var svgenc = v_viewmodel.getResponseElement(v_dataPaths[0]);
        var state;
        if (v_dataPaths[1] != undefined)
            state = v_viewmodel.getResponseElement(v_dataPaths[1]);
                
        var lKey = getRequestKeyS(v_dataPaths[0]);

        if (svgenc != undefined)
        {
            if (mDecodedSVGs[lKey] == undefined)
            {
                try
                {
                    mDecodedSVGs[lKey] = window.atob(svgenc.node.val);
                }
                catch (e)
                {
                    console.log("svg base64 decode error", svgenc);
                }
            }

            var svgdec = mDecodedSVGs[lKey];
            if (svgdec != undefined)
            {
                var lsvgdec = mcopy(svgdec);
                if (state != undefined)
                {
                    state = state.node.val;
                    if (state.indexOf(',') != -1)
                        state = state.split(',');
                    else
                        state = [state];

                    for (var i = 0; i < state.length; ++i) 
                        if (!i || (state[i] != state[i - 1]))
                        {
                            var activeIndx = lsvgdec.indexOf(state[i]);
                            var colorindx = lsvgdec.lastIndexOf("<rect fill=\"#FEFECE\"", activeIndx);
                            lsvgdec = replaceAt(lsvgdec, colorindx, "<rect fill=\"#ffafaf\"");
                        }
                }
                lValues[0] = {val: lsvgdec};
            }
        }
        return {values: lValues};
    };
    
    /** private functions */
    
    function getRequestKeyS(p_path) {
        var key = '';
        var currentPath = [];
        for (var i = 0; i < p_path.length - 1; ++i) {
            currentPath.push(p_path[i]);
            var request = v_viewmodel.getRequestFromPath(currentPath);
            if (request.getData.selectionValues != undefined) {
                key += request.getData.selectionValues + "SV_";
            } else if (request.getData.selection != undefined) {
                key += request.getData.selection[0] + 'S_';
            }
        }
        key += JSON.stringify(p_path) + 'P_';
        return key;
    }
}

CViewModel_FSM_SVG.getHelp = function() {
    return "A viewmodel that can be used to display FSMs with colored active state.";
}

CViewModel_FSM_SVG.providesInterface = function() {
    return ["getListWithElementInfo"];
};

CViewModel_FSM_SVG.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_FSM_SVG",
        "type": "object",
        "properties": {
            "schema": {
                "description": "The json schema that the FSM SVG will use if a second data connection is not present.",
                "type": "object"
            }
        },
        "additionalProperties": false
    };
};

CViewModel_FSM_SVG.expectsConnection = function() {
    var dataConnections = [
        {
            "valueType": ["charstringType", "octetstringType"]
        },
        {
            "valueType": ["charstringType"],
            "optional": true
        }
    ];

    var selectionConnections = [];

    return {
        "dataConnections": dataConnections,
        "selectionConnections": selectionConnections
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_FSM_SVG.js