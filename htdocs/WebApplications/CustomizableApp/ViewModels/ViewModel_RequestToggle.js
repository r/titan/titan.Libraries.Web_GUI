// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_RequestToggle(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    
    var v_binder;
    var v_dataPaths = [];
    var v_selections = [];
    
    var v_selected = 0;
    var v_defaultFilter;
    
    var v_filters = [];
    
    /** public functions - interface for parent */
    
    this.setSelectionToControl = function(p_selection) {
        v_selections.push(p_selection);
    };
    
    this.setReponseDataPath = function(p_index, p_path) {
        v_dataPaths[p_index] = p_path;
        var request = v_viewmodel.getRequestFromPath(p_path);
        v_filters[p_index] = request.getData.filter;
        if (v_options.toggleGroups[1] != undefined && p_index >= v_options.toggleGroups[1]) {
            request.getData.filter = {"dataValue": "false"};
        }
    };
    
    this.setBinder = function(p_binder) {
        v_binder = p_binder;
    };
	
	/** public functions - interface for views */ 
    
    this.select = function(groupIndex) {
        v_selected = groupIndex;
        
        var firstDataPathIndex = v_options.toggleGroups[groupIndex];
        var lastDataPathIndex = v_options.toggleGroups[groupIndex + 1];
        if (lastDataPathIndex == undefined) {
            lastDataPathIndex = v_dataPaths.length;
        }
        
        for (var i = 0; i < firstDataPathIndex; ++i) {
            setFilter(i, false);
        }
        
        for (var i = firstDataPathIndex; i < lastDataPathIndex; ++i) {
            setFilter(i, true);
        }
        
        for (var i = lastDataPathIndex; i < v_dataPaths.length; ++i) {
            setFilter(i, false);
        }
        
        v_binder.notifyChange(true);
    };
    
    this.getList = function() {
        return {"selections": [v_selected]};
    };
    
    /** private functions */
    
    function setFilter(dataPathIndex, setOriginal) {
        var request = v_viewmodel.getRequestFromPath(v_dataPaths[dataPathIndex]);
        if (request != undefined) {
            if (setOriginal) {
                request.getData.filter = v_filters[dataPathIndex];
            } else {
                request.getData.filter = {"dataValue": "false"};
            }
        }
    }
}

CViewModel_RequestToggle.getHelp = function() {
    return "This viewmodel can be used to toggle the filter of the connected request groups between their original filters and a constant false filter.";
}

CViewModel_RequestToggle.providesInterface = function() {
    return ["select", "getList"];
};

CViewModel_RequestToggle.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_RequestToggle",
        "type": "object",
        "properties": {
            "toggleGroups": {
                "description": "The index of the first element in each group.",
                "type": "array",
                "format": "table",
                "items": {
                    "title": "group",
                    "type": "integer",
                    "min": 0,
                    "uniqueItems": true,
                    "minItems": 1
                }
            },
            "filterIndex": {
                "type": "integer",
                "description": "The data path index whose filter will be used as default.",
                "default": 0
            }
        },
        "additionalProperties": false,
        "required": ["toggleGroups"]
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_RequestToggle.js