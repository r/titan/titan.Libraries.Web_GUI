// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_FlexAligner(p_viewModel, p_options) {
    "use strict";
    
    /** constructor */
    
    var v_viewmodel = p_viewModel;
    var v_options = p_options;
    var v_binder;
    
    var v_this = this;
    
    /** public functions - interface for parent */ 
    
    this.setBinder = function(p_binder) {
        v_binder = p_binder;
    };
    
    /** public functions - interface for views */ 
    
    this.getChildPercentages = function(p_ids) {
        var total = 0;
        var flexes = [];
        for (var i = 0; i < p_ids.length; ++i) {
            flexes.push(1);
        }
        
        var descriptors = v_viewmodel.getViewDescriptors();
        for (var i = 0; i < descriptors.length; ++i) {
            var index = p_ids.indexOf(descriptors[i].parentID);
            if (index != -1) {
                var flex = descriptors[i].customData.flex;
                if (flex != undefined) {
                    flexes[index] = flex;
                    total += flex;
                } else {
                    total += 1;
                }
            }
        }
        
        v_this.getChildPercentagesFromFlex(flexes, total);
        
        return flexes;
    };
    
    this.getChildPercentagesFromFlex = function(flexes, total) {
        for (var i = 0; i < flexes.length; ++i) {
            flexes[i] = "" + ((flexes[i] / total) * 100) + "%";
        }
    };
    
    this.getChildSizes = function(p_ids) {
        var totalFlex = 0;
        var totalPx = 0;
        var sizes = [];
        for (var i = 0; i < p_ids.length; ++i) {
            sizes.push(1);
        }
        
        var descriptors = v_viewmodel.getViewDescriptors();
        for (var i = 0; i < descriptors.length; ++i) {
            var index = p_ids.indexOf(descriptors[i].parentID);
            if (index != -1) {
                var flex = descriptors[i].customData.flex;
                if (flex != undefined) {
                    sizes[index] = flex;
                    if (Number.isInteger(flex)) {
                        totalFlex += flex;
                    } else {
                        totalPx += parseInt(flex.substring(0, flex.length - 2));
                    }
                } else {
                    totalFlex += 1;
                }
            }
        }
        
        for (var i = 0; i < sizes.length; ++i) {
            if (Number.isInteger(sizes[i])) {
                sizes[i] = "calc(" + (100 * sizes[i] / totalFlex) + "% - " + totalPx + "px)";
            }
        }
        
        return sizes;
    };
}

CViewModel_FlexAligner.getHelp = function() {
    return "A viewmodel that can calculate percentages from the flex of subviews.";
}

CViewModel_FlexAligner.providesInterface = function() {
    return ["getChildPercentages", "getChildPercentagesFromFlex", "getChildSizes"];
};

CViewModel_FlexAligner.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_FlexAligner",
        "type": "object",
        "properties": {},
        "additionalProperties": false
    };
};

CViewModel_FlexAligner.expectsConnection = function() {
    return {
        "dataConnections": [],
        "selectionConnections": []
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_FlexAligner.js
