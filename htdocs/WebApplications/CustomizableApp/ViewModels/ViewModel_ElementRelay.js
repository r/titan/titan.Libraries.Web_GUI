// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_ElementRelay(aViewModel, aOptions)
{
    "use strict";
    /** private members */
    var mViewModel;
    var mOptions;
    var mRq;
    var mBinder;
    var mReponseDataPaths;
    var mSelections = []; // mSelectionss is a list of references to objects, each containing a field named "selection"
    
    /** constructor */
    mViewModel = aViewModel;
    mRq = mViewModel.getRequest();
    mOptions = aOptions;
    mReponseDataPaths = [];

    /** public functions - Interface for parent */ 
    this.expectedReponseData = function() // TODO: might be put in prototype (without header info)
    {
        return[{type: "elementRelay"}, {type: "string"}];
    };

    this.setSelectionToControl = function(aSelection)
    {
        mSelections.push(aSelection);
    };
    
    this.setReponseDataPath = function(aDataPathIndex, aReponseDataPath)
    {
        mReponseDataPaths[aDataPathIndex] = aReponseDataPath;
    };
    
    this.setBinder = function(aBinder)
    {
        mBinder = aBinder;
    };
	
	/** public functions - Interface for views */ 
    this.select = function(aIndex)
    {
        for (var i = 0; i < mSelections.length; ++i)
            mViewModel.select(mSelections[i], aIndex);
        mBinder.notifyChange(true);
    };

    this.getOptions = function()
    {
        return mOptions;
    };
    
    this.getList = function()
    {
        return prepareResponse(getSimpleDataFromResponseElement);
    };
    
    this.getListWithElementInfo = function()
    {
        return prepareResponse(getDataFromResponseElement);
    };

    this.setValue = function(aDataPathIndex, aValue, aIndexInList, aLastSelectionIndexes, aDataHasBeenSet, aAdditionalData)
    {
        mViewModel.setResponseElement(mReponseDataPaths[aDataPathIndex], aValue, aIndexInList, aLastSelectionIndexes, aDataHasBeenSet, aAdditionalData);
    };

    /** private functions */

    function prepareResponse(aGetValueFunction)
    {
        var lValues = [];
        var lSelections = [];
        for (var i = 0; i < mReponseDataPaths.length; ++i)
        {
            lValues[i] = aGetValueFunction(mViewModel.getResponseElement(mReponseDataPaths[i]), mViewModel.getRequestFromPath(mReponseDataPaths[i]));
        }
        for (var i = 0; i < mSelections.length; ++i)
        {
            lSelections[i] = mSelections[i] ? mSelections[i].selection : undefined;
        }
        return {
            selections: lSelections,
            values: lValues
        };
    }

    function getEncodedVal(aVal)
    {
        if (mOptions.decodeBase64) 
        {
            try
            {
                return window.atob(aVal);
            }
            catch (e)
            {
                return aVal;
                console.log("Base64 decode error", aVal);
            }
        }
        else
            return aVal;
    }
    
    function getRow(node)
    {
        var row = [];
        row.push(node.val);
        if (node.childVals)
        {
            var lChildren = [];
            for (var j = 0; j < node.childVals.length; ++j)
            {
                if (node.childVals[j].node)
                {
                    lChildren.push(getEncodedVal(node.childVals[j].node.val));
                }
                else if (node.childVals[j].list)
                {
                    var lList = [];
                    for (var i = 0; i < node.childVals[j].list.length; ++i)
                    {
                        lList.push(getEncodedVal(node.childVals[j].list[i].node.val));
                    }
                    lChildren.push(lList);
                    //console.log ("lListVM::", lList);
                }
            }
            row.push(lChildren);
        }
        return row;
    }
    
    function getSimpleDataFromResponseElement(lElement, aRq)
    {
        var lTable = [];
        if (lElement)
        {
            if (lElement.error)
                lTable.push([lElement.error]);
            else if (lElement.node)
                lTable.push(getRow(lElement.node));
            else if (lElement.list)
            {
                /*if (mSelections[0] && mSelections[0][0] >= lElement.list.length)
                    mSelections[0][0] = 0;*/
                if (mOptions.showOnlySelected != undefined && aRq.getData.selection != undefined) {
                    if (lElement.list.length > 0) {
                        if (aRq.getData.selectionValues != undefined) {
                            lTable.push(getRow(lElement.list[0].node));
                        } else {
                            lTable.push(getRow(lElement.list[aRq.getData.selection[0]].node));
                        }
                    }
                } else {
                    for (var i = 0; i < lElement.list.length; ++i) {
                        lTable.push(getRow(lElement.list[i].node));
                    }
                }
            }
            //else
                //console.log("something else lElement: ", lElement)
        }
        return lTable;
    }

    function getChildren(node)
    {
        var lChildren = [];
        if (node.childVals)
        {
            for (var j = 0; j < node.childVals.length; ++j)
            {
                if (node.childVals[j].node)
                {
                    lChildren.push(node.childVals[j].node.val);
                }
                else if (node.childVals[j].list)
                {
                    var lList = [];
                    for (var i = 0; i < node.childVals[j].list.length; ++i)
                        lList.push(node.childVals[j].list[i].node.val);
                    lChildren.push(lList);
                }
            }
        }
        return lChildren;
    }

    function getDataFromResponseElement(lElement, aRq)
    {
        var lData = {};
        if (lElement)
        {
            lData.element = aRq.getData.element;
            lData.selection = aRq.getData.selection;
            if (lElement.error)
                lData.error = lElement.error;
            else if (lElement.node)
            {
                lData.val = getEncodedVal(lElement.node.val);
                lData.isWritable = lElement.node.tp > 0;
                lData.children = getChildren(lElement.node, aRq.getData)[1];
            }
            else if (lElement.list)
            {

                if (mOptions.showOnlySelected != undefined && aRq.getData.selection != undefined) {
                    if (lElement.list.length > 0) {
                        var node;
                        var lChildList = [];
                        if (aRq.getData.selectionValues != undefined) {
                            node = lElement.list[0].node
                        } else {
                            
                            node = lElement.list[aRq.getData.selection[0]].node;
                        }
                        lData.val = getEncodedVal(node.val);
                        if (aRq.getData.children) {
                            var lChildren = getChildren(node);
                            if (aRq.getData.children) {
                                for (var j = 0; j < lChildren.length; ++j) {
                                    lChildList[j] = {
                                        element: aRq.getData.children[j].getData.element, 
                                        val:[lChildren[j]],
                                        selection: aRq.getData.children[j].getData.selection
                                    };
                                }
                            }
                        }
                        lData.children = lChildList;
                    } else {
                        lData.val = "";
                        lData.children = [];
                    }
                } else {
                    var lList = [];
                    var lChildList = [];
                    
                    for (var i = 0; i < lElement.list.length; ++i) {
                        lList.push(getEncodedVal(lElement.list[i].node.val));
                        var lChildren = getChildren(lElement.list[i].node);
                        if (aRq.getData.children)
                        {
                            if (lChildList.length == 0)
                            {
                                for (var j = 0; j < lChildren.length; ++j)
                                {
                                    lChildList[j] = {
                                        element: aRq.getData.children[j].getData.element, 
                                        val:[],
                                        selection: aRq.getData.children[j].getData.selection
                                    };
                                }
                            }
                            for (var j = 0; j < lChildren.length; ++j)
                            {
                                lChildList[j].val.push(lChildren[j]);
                            }
                        }
                    }
                    
                    lData.val = lList;
                    lData.children = lChildList;
                }
                

            }
        }
        return lData;
    }
}

CViewModel_ElementRelay.getHelp = function() {
    var help = 'Simple interface for views.\n';
    help += 'The function getList will return the following structure: {values: [[value, [children], isWritable]], selections: [[]]}\n';
    help += 'The function getListWithElementInfo will return the following structure: {values: [{val: "", element: "", isWritable: boolean, selection: [], children: []}], selections: [[]]}\n';
    return help;
}

CViewModel_ElementRelay.providesInterface = function() {
    return ["setValue", "getListWithElementInfo", "getList", "getOptions", "select"];
};

CViewModel_ElementRelay.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_ElementRelay",
        "type": "object",
        "properties": {
            "showOnlySelected": {
                "type": "boolean",
                "format": "checkbox",
                "description": "whether we only show the selected element from an iterator",
                "default": true
            },
            "decodeBase64": {
                "type": "boolean",
                "format": "checkbox",
                "description": "if true, the returned data will be base64 decoded",
                "default": true
            }
        }
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_ElementRelay.js
