// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_SetupTabs(a_viewmodel, a_options) {
    "use strict";
    
    /** constructor */
    
    var m_Binder;
    var m_options = a_options;
    var m_setupModel = a_viewmodel.getSetupModel();
    var m_setupList = a_viewmodel.getSetupList();
    var m_hierarchy = m_options.hierarchy;
    var m_parentViewmodel = a_viewmodel;
    var m_conditionViewmodels = [];
    var m_setupName = m_setupModel.getSetup().name;
    var m_setupParams = m_setupModel.getSetup().setupParams;
    
    function insertMissingSetups() {
        var otherOrganizer = {"displayName": "other", "type": "organizer", "menuElements": []};
        var setupsInHierarchy = [];
        findAllSetupsInHierarchy(setupsInHierarchy, m_hierarchy.menuElements);
        for (var i = 0; i < m_setupList.length; ++i) {
            if (setupsInHierarchy.indexOf(m_setupList[i]) == -1)
                otherOrganizer.menuElements.push({"setupName": m_setupList[i], "type": "setup"});
        }
        if (otherOrganizer.menuElements.length > 0)
            m_hierarchy.menuElements.push(otherOrganizer);
    }
    
    function findAllSetupsInHierarchy(aFoundSetups, aMenuElements) {
        for (var i = 0; i < aMenuElements.length; ++i) {
            if (aMenuElements[i].setupName != undefined) {
                aFoundSetups.push(aMenuElements[i].setupName);
            }
            if (aMenuElements[i].menuElements != undefined) {
                findAllSetupsInHierarchy(aFoundSetups, aMenuElements[i].menuElements);
            }
        }
    }
    
    if (m_options.displayOtherSetups)
        insertMissingSetups();
    
    var m_this = this;
    
    /** public functions - interface for parent */
    
    this.setBinder = function(aBinder)
    {
        m_Binder = aBinder;
    };
    
    this.setReponseDataPath = function(aIndex, aPath) {
        m_conditionViewmodels[aIndex] = new CViewModel_Condition(m_parentViewmodel, {});
        m_conditionViewmodels[aIndex].setReponseDataPath(0, aPath);
    };
    
    /** public functions - interface for views */
    
    this.getRecursiveSetups = function(callback)
    {
        return new CViewmodel_RecursiveSetups(m_this, m_hierarchy.menuElements, m_this.getFullNameOfSetup(m_setupName, m_setupParams), []);
    };
    
    /** public functions - interface for subviewmodels */
    
    this.select = function(a_setup, a_setupParams)
    {
        m_parentViewmodel.loadSetup(a_setup, a_setupParams);
    };
    
    this.checkVisibility = function(dataPathIndex) {
        if (m_conditionViewmodels[dataPathIndex] != undefined) {
            return m_conditionViewmodels[dataPathIndex].getState();
        } else {
            return true;
        }
    };
    
    this.getFullNameOfSetup = function(setupName, setupParams) {
        var name = setupName;
        if (setupParams != undefined) {
            name += "_" + JSON.stringify(setupParams);
        }
        return name;
    };
};

function CViewmodel_RecursiveSetups(a_parentViewmodel, a_menuElements, a_selectedSetupName, a_organizerNames) {
    "use strict";
    var m_setupList = [];
    var m_setupNameList = [];
    var m_setupParamsList = [];
    var m_menuElements = a_menuElements;
    var m_selectedIndex = 0;
    var m_subViewModels = [];
    var m_parentViewmodel = a_parentViewmodel;
    var m_selectedSetupName = a_selectedSetupName;
    var m_organizerNames = a_organizerNames;
    var m_this = this;
    
    var counter = 0;
    var selectedElementIsFound = false;
    for (var i = 0; i < m_menuElements.length; ++i) {
        if (m_menuElements[i].isVisible == undefined || m_parentViewmodel.checkVisibility(m_menuElements[i].isVisible)) {
            if (m_selectedSetupName != undefined && m_menuElements[i] && m_selectedSetupName == m_parentViewmodel.getFullNameOfSetup(m_menuElements[i].setupName, m_menuElements[i].setupParams)) {
                m_selectedIndex = counter;
                localStorage.setItem(getKey(), counter);
                selectedElementIsFound = true;
                break;
            }
            ++counter;
        }
    }
        
    counter = 0;
    for (var i = 0; i < m_menuElements.length; ++i) 
    {
        if (m_menuElements[i].isVisible == undefined || m_parentViewmodel.checkVisibility(m_menuElements[i].isVisible)) {
            if (m_menuElements[i].displayName)
                m_setupList[counter] = [m_menuElements[i].displayName];
            else
                m_setupList[counter] = [m_menuElements[i].setupName];
            m_setupNameList[counter] = m_menuElements[i].setupName;
            m_setupParamsList[counter] = m_menuElements[i].setupParams;
            var setupToSelect;
            if (!selectedElementIsFound)
                setupToSelect = m_selectedSetupName;
            if (m_menuElements[i].menuElements && m_menuElements[i].menuElements.length) {
                var organizerPath = mcopy(m_organizerNames);
                organizerPath.push(m_menuElements[i].displayName);
                m_subViewModels[counter] = new CViewmodel_RecursiveSetups(m_parentViewmodel, m_menuElements[i].menuElements, setupToSelect, organizerPath);
                if (m_subViewModels[counter].isSelectedElementFound()) {
                    m_selectedIndex = counter;
                    localStorage.setItem(getKey(), counter);
                    selectedElementIsFound = true;
                }
            }
            
            ++counter;
        }
    }
    
    this.getSubViewModels = function()
    {
        return m_subViewModels;
    };
    
    this.getselectedSetup = function()
    {
        var selectedIndex = localStorage.getItem(getKey());
        if (selectedIndex == undefined || selectedIndex >= m_setupNameList.length) {
            selectedIndex = m_selectedIndex;
        }
        
        var selectedSetupName;
        if (!m_subViewModels[selectedIndex])
           return [m_setupNameList[selectedIndex], m_setupParamsList[selectedIndex]];
        else
           return m_subViewModels[selectedIndex].getselectedSetup();
    };

    this.getList = function()
    {
        return {
            selections: [[m_selectedIndex]],
            values: [m_setupList]
        };
    };
    
    this.select = function(a_Index)
    {
        m_selectedIndex = a_Index;
        localStorage.setItem(getKey(), m_selectedIndex);
        var selected = m_this.getselectedSetup();
        m_parentViewmodel.select(selected[0], selected[1]);
    };
    
    this.isSelectedElementFound = function() {
        return selectedElementIsFound;
    };
    
    function getKey() {
        var key = "setupTabSelection";
        for (var i = 0; i < m_organizerNames.length; ++i) {
            key += "_" + m_organizerNames[i];
        }
        return key;
    }
}

CViewModel_SetupTabs.getHelp = function() {
    return "A viewmodel that contains the setup hierarchy.";
};

CViewModel_SetupTabs.providesInterface = function() {
    return ["getRecursiveSetups"];
};

CViewModel_SetupTabs.getCustomDataSchema = function() {
    return {
        "$schema" : "http://json-schema.org/draft-04/schema#",
        "definitions": {
            "setup": {
                "title": "Setup",
                "description": "defines a setup",
                "type": "object",
                "additionalProperties": false,
                "properties": {
                    "displayName": {
                        "title": "display name",
                        "description": "the name that is displayed",
                        "type": "string"
                    },
                    "setupName": {
                        "title": "setup name",
                        "description": "the name of the setup",
                        "type": "string"
                    },
                    "type": {
                        "type": "string",
                        "enum": ["setup"]
                    },
                    "isVisible": {
                        "type": "integer",
                        "description": "the data path index of a connected request that returns a bool value or contains a filter"
                    },
                    "setupParams": {
                        "type": "object",
                        "additionalProperties": true,
                        "properties": {}
                    }
                },
                "required": ["setupName", "type"]
            },
            "organizer": {
                "title": "Organizer",
                "description": "defines an organizer which can contain additional organizers and setups",
                "type": "object",
                "additionalProperties": false,
                "properties": {
                    "displayName": {
                        "title": "display name",
                        "description": "the name that is displayed",
                        "type": "string"
                    },
                    "type": {
                        "type": "string",
                        "enum": ["organizer"]
                    },
                    "menuElements": {
                        "$ref" : "#/definitions/menuElements"
                    },
                    "isVisible": {
                        "type": "integer",
                        "description": "the data path index of a connected request that returns a bool value or contains a filter"
                    }
                },
                "required" : ["displayName", "type"]
            },
            "menuElements": {
                "title": "Menu elements",
                "description": "the list of menu elements",
                "type": "array",
                "format": "tabs",
                "items": {
                    "title": "menu element",
                    "oneOf" : [
                        {
                            "$ref" : "#/definitions/organizer"
                        },
                        {
                            "$ref" : "#/definitions/setup"
                        }
                    ]
                }
            }
        },
        "title": "Setup hierarchy",
        "type" : "object",
        "additionalProperties" : false,
        "properties" : {
            "hierarchy": {
                "description": "The setup hierarchy.",
                "type": "object",
                "properties": {
                    "menuElements" : {
                        "$ref" : "#/definitions/menuElements"
                    }
                },
                "additionalProperties" : false
            },
            "displayOtherSetups": {
                "description": "Whether we want to display the other setups not included in the hierarchy.",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            }
        }
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_SetupTabs.js
