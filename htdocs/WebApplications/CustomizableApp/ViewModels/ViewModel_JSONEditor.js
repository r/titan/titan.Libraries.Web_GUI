// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_JSONEditor(p_viewmodel, p_options) {
    "use strict";

    /** constructor */

    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    var v_dataPaths = [];

    /** public functions - interface for parent */

    this.setSelectionToControl = function(p_selection) {};
    this.setBinder = function(p_binder) {};
    this.setReponseDataPath = function(p_index, p_path) {
        v_dataPaths[p_index] = p_path;
    };

    /** public functions - interface for views */

    this.getJSONData = function(callback) {
        callback(getResponse(0));
    };

    this.setJSONData = function(json) {
        var response = v_viewmodel.getResponseElement(v_dataPaths[0]);
        if (response != undefined && response.node != undefined) {
            if (response.node.tp == 5) {
                v_viewmodel.setResponseElement(v_dataPaths[0], a2hex(JSON.stringify(json)));
            } else {
                v_viewmodel.setResponseElement(v_dataPaths[0], JSON.stringify(json));
            }
        }
    };

    this.getSchema = function() {
        if (v_dataPaths.length > 1) {
            return getResponse(1);
        } else {
            return v_options.schema;
        }
    };

    function getResponse(index) {
        var returnValue;
        var response = v_viewmodel.getResponseElement(v_dataPaths[index]);
        if (response != undefined && response.node != undefined) {
            if (response.node.tp == 5) {
                return JSON.parse(hex2a(response.node.val));
            } else {
                return JSON.parse(response.node.val);
            }
        } else {
            return {};
        }
    }
}

CViewModel_JSONEditor.getHelp = function() {
    return "A viewmodel that can be used for editing json data using a json schema.";
}

CViewModel_JSONEditor.providesInterface = function() {
    return ["getJSONData", "setJSONData", "getSchema"];
};

CViewModel_JSONEditor.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_JSONEditor",
        "type": "object",
        "properties": {
            "schema": {
                "description": "The json schema that the json editor will use if a second data connection is not present.",
                "type": "object"
            }
        },
        "additionalProperties": false
    };
};

CViewModel_JSONEditor.expectsConnection = function() {
    var dataConnections = [
        {
            "valueType": ["charstringType", "octetstringType"]
        },
        {
            "valueType": ["charstringType", "octetstringType"],
            "optional": true
        }
    ];

    var selectionConnections = [];

    return {
        "dataConnections": dataConnections,
        "selectionConnections": selectionConnections
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_CodeEditor.js