// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_TableForLargeData(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    
    var v_binder;
    var v_dataPaths = [];
    var v_selections = [];
    
    var v_streamingThreshold = 50;
    if (v_options.streamingThreshold != undefined) {
        v_streamingThreshold = v_options.streamingThreshold;
    }
    
    var v_rangeFilterCount = v_streamingThreshold;
    if (v_options.rangeFilterCount != undefined) {
        v_rangeFilterCount = v_options.rangeFilterCount;
    }
    
    var v_wasStreaming = false;
    
    var base = new CViewModel_DynamicTable(p_viewmodel, p_options);
    var paging = new CViewModel_Paging(p_viewmodel, p_options);
    
    /** public functions - interface for parent */ 
    
    this.setSelectionToControl = function(p_selection) {
        base.setSelectionToControl(p_selection);
        v_selections.push(p_selection);
    };
    
    this.setReponseDataPath = function(p_index, p_path) {
        if (p_index == 0) {
            paging.setReponseDataPath(p_index, p_path);
        } else if (p_index == 1) {
            paging.setReponseDataPath(p_index, p_path);
            base.setReponseDataPath(p_index - 1, p_path);
            v_dataPaths[0] = p_path;
        } else {
            base.setReponseDataPath(p_index - 1, p_path);
        }
    };
    
    this.setBinder = function(p_binder) {
        base.setBinder(p_binder);
        paging.setBinder(p_binder);
        v_binder = p_binder;
    };
    
    /** public functions - interface for views */
	
    this.select = function(p_index) {
        for (var i = 0; i < v_selections.length; ++i) {
            var responseElement = v_viewmodel.getResponseElement(v_dataPaths[0]);
            if (responseElement != undefined && responseElement.list[p_index] != undefined) {
                v_viewmodel.selectByValue(v_selections[i], responseElement.list[p_index].node.val, p_index);
            } else {
                v_viewmodel.select(v_selections[i], p_index);
            }
        }
        if (v_selections.length > 0) {
            v_binder.notifyChange(true);
        }
    };
    this.getHeader = base.getHeader;
    this.getName = base.getName;
    this.getTable = base.getTable;
    this.setValue = base.setValue;
    
    this.getRange = function() {
        var size = paging.getSize();
        var max = size;
        if (v_options.allowScrollBelow == false) {
            max = max - paging.getRangeFilter().count + 1;
        }
        return {
            "min": 0,
            "max": max
        };
    };
    
    this.getPosition = function() {
        return paging.getRangeFilter().offset;
    };
    
    this.setPosition = function(position) {
        return paging.changeFilter({"offset": {"to": position}});
    };
    
    this.getViewportSize = function() {
        return paging.getRangeFilter().count;
    };
    
    this.isStreaming = function() {
        var size = paging.getSize();
        var isStreaming = size > v_streamingThreshold;
        
        if (isStreaming && !v_wasStreaming) {
            var request = v_viewmodel.getRequestFromPath(v_dataPaths[0]);
            request.getData.rangeFilter.count = v_rangeFilterCount;
        } else if (v_wasStreaming && !isStreaming) {
            var request = v_viewmodel.getRequestFromPath(v_dataPaths[0]);
            request.getData.rangeFilter.count = v_streamingThreshold;
        }
        
        v_wasStreaming = isStreaming;
        return isStreaming;
    };
}

CViewModel_TableForLargeData.getHelp = function() {
    return "A table viewmodel that can handle large amounts of data.\n" + 
        "The second data connection must be a list which defines the rows of the table. The first data connection must be its size.\n" + 
        "If more connections are given, they will represent the columns of the table.";
};

CViewModel_TableForLargeData.providesInterface = function() {
    return ["select", "getName", "getHeader", "getTable", "setValue", "getRange", "getPosition", "setPosition", "getViewportSize", "isStreaming"];
};

CViewModel_TableForLargeData.getCustomDataSchema = function() {
    var schema = CViewModel_DynamicTable.getCustomDataSchema();
    var pagingSchema = CViewModel_Paging.getCustomDataSchema();
    for (var id in pagingSchema.properties) {
        schema.properties[id] = pagingSchema.properties[id];
    }
    
    schema.title = "Custom data for CViewModel_TableForLargeData";
    schema.properties.streamingThreshold = {
        "description": "If the list is longer than this amount, use range filter to reduce the number of elements returned.",
        "type": "integer",
        "min": 1,
        "default": 50
    };
    schema.properties.rangeFilterCount = {
        "description": "When using range filter, this many elements will be returned.",
        "type": "integer",
        "min": 1,
        "default": 20
    };
    return schema;
};

CViewModel_TableForLargeData.expectsConnection = function() {
    var dataConnections = [
        {
            "source": "DataSource",
            "element": "sizeOf"
        },
        {
            "valueType": ["charstringlistType", "integerlistType", "floatlistType"]
        },
        {
            "childOfDataConnection": 1,
            "valueType": ["charstringType", "intType", "floatType", "statusLEDType", "boolType"],
            "multiple": true,
            "optional": true
        }
    ];
    
    var selectionConnections = [
        {
            "dataElementOfDataConnection": 1,
            "multiple": true,
            "optional": true
        }
    ];
    
    return {
        "dataConnections": dataConnections,
        "selectionConnections": selectionConnections
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_TableForLargeData.js