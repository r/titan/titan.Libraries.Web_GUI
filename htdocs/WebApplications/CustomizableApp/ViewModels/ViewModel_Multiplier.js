// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_Multiplier(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    
    var v_binder;
    var v_dataPaths = [];
    var v_selections = [];

    var v_currentBundle = [];
    var v_last_response;

    var v_this = this;

    /** public functions - interface for parent */ 

    this.setSelectionToControl = function(p_selection) {
        v_selections.push(p_selection);
    };
    
    this.setReponseDataPath = function(p_index, p_path) {
        v_dataPaths[p_index] = p_path;
    };
    
    this.setBinder = function(p_binder) {
        v_binder = p_binder;
    };
	
	/** public functions - interface for views */ 

    this.select = function(p_index) {
        for (var i = 0; i < v_selections.length; ++i) {
            v_viewmodel.select(v_selections[i], p_index);
        }
        if (v_selections.length > 0) {
            v_binder.notifyChange(true);
        }
    };

    this.getViewmodelBundle = function() {
        
        var response = v_viewmodel.getResponseElement(v_dataPaths[0]);
        var numberOfBundles = 0;
        if (response != undefined && response.list != undefined) {
            numberOfBundles = response.list.length;
        }
        
        var vmbjson = JSON.stringify(response);
        if (v_last_response != vmbjson && numberOfBundles != v_currentBundle.length)
        {
            v_last_response = vmbjson;
            v_currentBundle = [];
        }
        
        var currentBundles = v_currentBundle.length;
        if (numberOfBundles < currentBundles) {
            while (v_currentBundle.length > numberOfBundles) {
                v_currentBundle.pop();
            }
        } else if (numberOfBundles > currentBundles) {
            for (var i = currentBundles; i < numberOfBundles; ++i) {
                v_currentBundle.push(createBundle(i));
            }
        }

        return v_currentBundle;
    };
    
    this.getSelectionIndx = function() {
        if (v_selections[0])
            return v_selections[0].selection;
        else
            return 0;
    };

    /** private functions */
    
    function createBundle(indexInList) {
        var bundle = [];
        if (v_options.subviewmodels != undefined) {
            for (var i = 0; i < v_options.subviewmodels.length; ++i) {
                if (v_options.subviewmodels[i].subViewmodel != undefined && window[v_options.subviewmodels[i].subViewmodel] != undefined) {
                    if (v_options.subviewmodels[i].subViewmodelType == "ViewmodelForIteratorViewmodels") {
                        bundle[i] = new window[v_options.subviewmodels[i].subViewmodel](new ViewmodelForIteratorViewmodels(indexInList), v_options.subviewmodels[i]);
                    } else {
                        bundle[i] = new window[v_options.subviewmodels[i].subViewmodel](new ViewmodelForsubviewmodels(indexInList), v_options.subviewmodels[i]);
                    }
                    
                    if (v_options.subviewmodels[i].dataPathIndexes != undefined) {
                        for (var j = 0; j < v_options.subviewmodels[i].dataPathIndexes.length; ++j) {
                            if (bundle[i].setReponseDataPath != undefined) {
                                bundle[i].setReponseDataPath(j, v_dataPaths[v_options.subviewmodels[i].dataPathIndexes[j]]);
                            }
                            if (bundle[i].setBinder != undefined) {
                                bundle[i].setBinder(v_binder);
                            }
                            bundle[i].loadFile = v_this.loadFile;
                        }
                    }
                    
                } else {
                    alert("No class found for subviewmodel " + i + ": " + v_options.subviewmodels[i].subViewmodel);
                }
            }
        }
        return bundle;
    }
    
    /** private Classes */
    
    function ViewmodelForIteratorViewmodels(indexInList) {
        var v_indexInList = indexInList;
        
        for (var id in v_viewmodel) {
            if (v_viewmodel.hasOwnProperty(id)) {
                this[id] = v_viewmodel[id];
            }
        }
        
        this.getResponseElement = function(reponseDataPath, lastSelectionIndexes) {
            var response = v_viewmodel.getResponseElement(reponseDataPath, lastSelectionIndexes);
            if (response != undefined && response.list != undefined) {
                response = response.list[v_indexInList];
            } else {
                response = undefined;
            }
            return response;
        };
        
        this.setResponseElement = function(reponseDataPath, value, indexInList, lastSelectionIndexes, callback) {
            v_viewmodel.setResponseElement(reponseDataPath, value, v_indexInList, lastSelectionIndexes, callback);
        };
    }
    
    function ViewmodelForsubviewmodels(indexInList) {
        var v_indexInList = indexInList;
        
        for (var id in v_viewmodel) {
            if (v_viewmodel.hasOwnProperty(id)) {
                this[id] = v_viewmodel[id];
            }
        }
        
        this.getResponseElement = function(reponseDataPath, lastSelectionIndexes) {
            if (lastSelectionIndexes != undefined) {
                lastSelectionIndexes.unshift(v_indexInList);
            } else {
                lastSelectionIndexes = [v_indexInList];
            }
            return v_viewmodel.getResponseElement(reponseDataPath, lastSelectionIndexes);
        };
        
        this.setResponseElement = function(reponseDataPath, value, indexInList, lastSelectionIndexes, callback) {
            if (lastSelectionIndexes != undefined) {
                lastSelectionIndexes.unshift(v_indexInList);
            } else {
                lastSelectionIndexes = [v_indexInList];
            }
            v_viewmodel.setResponseElement(reponseDataPath, value, indexInList, lastSelectionIndexes, callback);
        };
    }
}

CViewModel_Multiplier.getHelp = function() {
    return "Viewmodel that can be used to create other viewmodels based on the number of elements in an iterator.";
};

CViewModel_Multiplier.providesInterface = function() {
    return ["getViewmodelBundle", "getSelectionIndx"];
};

CViewModel_Multiplier.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_DynamicTable",
        "type": "object",
        "properties": {
            "subviewmodels": {
                "descriptor": "A list of sub viewmodels, including their custom data, that will be created for every element in the iterator.",
                "type": "array",
                "format": "tabs",
                "items": {
                    "type": "object",
                    "title": "subViewmodel",
                    "properties": {
                        "subViewmodel": {
                            "description": "The class name of the viewmodel.",
                            "type": "string"
                        },
                        "dataPathIndexes": {
                            "description": "Which connected data paths belong to this viewmodel?",
                            "type": "array",
                            "format": "table",
                            "items": {
                                "title": "viewmodel index",
                                "type": "integer",
                                "min": 0
                            }
                        },
                        "subViewmodelType": {
                            "description": "The type of the sub viewmodel. Use ViewmodelForIteratorViewmodels when using data path 0 for a label.",
                            "type": "string",
                            "enum": ["ViewmodelForIteratorViewmodels", "ViewmodelForsubviewmodels"],
                            "default": "ViewmodelForIteratorViewmodels"
                        }
                    },
                    "additionalProperties": true,
                    "required": ["subViewmodel"]
                },
                "minItems": 1
            }
        },
        "additionalProperties": false,
        "required": ["subviewmodels"]
    };
};

CViewModel_Multiplier.expectsConnection = function() {
    var dataConnections = [
        {
            "valueType": ["charstringlistType", "integerlistType", "floatlistType"]
        },
        {
            "childOfDataConnection": 0,
            "multiple": true,
            "optional": true
        }
    ];
    
    var selectionConnections = [
        {
            "dataElementOfDataConnection": 0,
            "multiple": true,
            "optional": true
        }
    ];
 
    return {
        "dataConnections": dataConnections,
        "selectionConnections": selectionConnections
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_Multiplier.js