// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_RequestManipulator(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    
    var v_binder;
    var v_dataPaths = [];
    var v_selections = [];
    
    var v_request;
    var v_schema = {
        "$schema" : "http://json-schema.org/draft-04/schema#",
        "definitions" : {
            "getDataDef" : {
                "title" : "request",
                "type" : "object",
                "additionalProperties" : false,
                "properties" : {
                    "getData": {
                        "type": "object",
                        "additionalProperties" : false,
                        "properties": {
                            "source": {
                                "description": "The source",
                                "type": "string"
                            },
                            "element": {
                                "description": "The element",
                                "type": "string"
                            },
                            "ptcname": {
                                "description": "The ptc name",
                                "type": "string"
                            },
                            "params": {
                                "type": "array",
                                "format": "table",
                                "items": {
                                    "type": "object",
                                    "title": "Parameter",
                                    "properties": {
                                        "paramName" : {
                                            "type" : "string"
                                        },
                                        "paramValue" : {
                                            "type" : "string"
                                        }
                                    },
                                    "additionalProperties": false
                                },
                                "uniqueItems": true
                            },
                            "selection": {
                                "type": "array",
                                "format": "table",
                                "items": {
                                    "type": "integer",
                                    "minimum": "0",
                                    "title": "Selection item"
                                },
                                "uniqueItems": true
                            },
                            "selectionValues": {
                                "type": "array",
                                "format": "table",
                                "items": {
                                    "type": "string",
                                    "title": "Selected value"
                                }
                            },
                            "rangeFilter": {
                                "type": "object",
                                "properties": {
                                    "offset": {
                                        "type": "integer",
                                        "minimum": 0
                                    },
                                    "count": {
                                        "type": "integer",
                                        "minimum": 0
                                    }
                                },
                                "additionalProperties": false
                            },
                            "children" : {
                                "type" : "array",
                                "additionalProperties" : false,
                                "items" : {
                                    "$ref" : "#/definitions/getDataDef"
                                }
                            }
                        },
                        "required" : [
                            "source",
                            "element"
                        ]
                    }
                },
                "required" : ["getData"]
            }
        },
        "$ref" : "#/definitions/getDataDef"
    }
    
    /** public functions - interface for parent */
    
    this.setSelectionToControl = function(p_selection) {
        v_selections.push(p_selection);
    };
    
    this.setReponseDataPath = function(p_index, p_path) {
        v_dataPaths[p_index] = p_path;
    };
    
    this.setBinder = function(p_binder) {
        v_binder = p_binder;
    };
	
	/** public functions - interface for views */ 
    
    this.setJSONData = function(p_data) {
        if (v_request != undefined) {
            for (var id in p_data) {
                v_request[id] = p_data[id];
            }
        }
    }
    
    this.getJSONData = function(callback) {
        v_request = v_viewmodel.getRequestFromPath(v_dataPaths[0]);
        callback(v_request);
    };
    
    this.getSchema = function() {
        return v_schema;
    }
}

CViewModel_RequestManipulator.getHelp = function() {
    return "A viewmodel that can be connected to a JSON editor to edit the connected request.";
};

CViewModel_RequestManipulator.providesInterface = function() {
    return ["getSchema", "setJSONData", "getJSONData"];
};

CViewModel_RequestManipulator.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_RequestManipulator",
        "type": "object",
        "properties": {},
        "additionalProperties": false
    };
};

CViewModel_RequestManipulator.expectsConnection = function() {
    return {
        "dataConnections": [{}],
        "selectionConnections": []
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_RequestManipulator.js
