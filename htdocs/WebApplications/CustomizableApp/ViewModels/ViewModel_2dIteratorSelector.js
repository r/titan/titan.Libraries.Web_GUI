// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_2dIteratorSelector(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    v_options.iteratorColumnPosition = -1;
    var base = new CViewModel_DynamicTable(p_viewmodel, v_options);
    
    var v_rowIndexes = [0];
    var v_tableData = {
        "table": []
    };
    
    var v_selections = [];
    var v_binder;
    
    /** public functions - interface for parent */ 

    this.setSelectionToControl = function(p_selection) {
        v_selections.push(p_selection);
    };
    this.setReponseDataPath = base.setReponseDataPath;
    this.setBinder = function(p_binder) {
        v_binder = p_binder;
    };
    
	/** public functions - interface for views */
    
    this.select = function(p_index) {
        var select1 = 0;
        var select2 = 0;
        for (var i = v_rowIndexes.length - 1; i >= 0; --i) {
            if (p_index >= v_rowIndexes[i]) {
                select1 = i;
                select2 = p_index - v_rowIndexes[i];
                break;
            }
        }
        
        v_viewmodel.select(v_selections[0], select1);
        for (var i = 1; i < v_selections.length; ++i) {
            v_viewmodel.select(v_selections[i], select2);
        }
        
        v_binder.notifyChange(true);
    };
    
    this.getList = function() {
        v_tableData = base.getTable();
        var list = flattenTable();
        return list;
    };
    
    /** private Classes */
    
    function flattenTable() {
        v_rowIndexes = [];
        var list = {
            "values" : [[]],
            "selections": [[]]
        };
        var elements = 0;
        for (var i = 0; i < v_tableData.table.length; ++i) {
            v_rowIndexes.push(elements);
            for (var j = 0; j < v_tableData.table[i].length; ++j) {
                if (v_options.text != undefined) {
                    list.values[0].push([getValue(v_tableData.table[i][j].val, i, j, v_options.text)]);
                } else {
                    list.values[0].push([v_tableData.table[i][j].val]);
                }
                
                if (v_selections[0].selection[0] == i && v_selections[1].selection[0] == j) {
                    list.selections[0][0] = elements;
                }
                ++elements;
            }
        }
        return list;
    }
    
    function getValue(val, row, col, text) {
        text = text.replace(/#i/gi, row);
        text = text.replace(/#j/gi, col);
        text = text.replace(/#val/gi, val);
        return text;
    }
}

CViewModel_2dIteratorSelector.getHelp = function() {
    return "Viewmodel that can be used to set the selection of two iterators.";
};

CViewModel_2dIteratorSelector.providesInterface = function() {
    return ["getList", "select"];
};

CViewModel_2dIteratorSelector.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_2dIteratorSelector",
        "type": "object",
        "properties": {
            "text": {
                "type": "string",
                "description": "Display additional data, use #i for the rows, #j for the columns, and #val for the value. If not defined, the value will be used."
            }
        }
    };
};

//# sourceURL=CustomizableApp\ViewModels\CViewModel_2dIteratorSelector.js