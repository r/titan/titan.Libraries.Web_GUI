// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_FilterAndSort(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    var v_binder;
    
    var v_currentIndexes = [];
    var v_currentSorting = v_options.initialSorting;
    var v_currentFilter = v_options.initialFilter;
    
    /** public functions - interface for parent */ 
    
    this.setBinder = function(p_binder) {
        v_binder = p_binder;
    };
	
	/** public functions - interface for views */ 
    
    this.setFilter = function(p_filter) {
        v_currentFilter = p_filter;
    };
    
    this.getFilter = function() {
        return v_currentFilter;
    };
    
    this.setSort = function(p_sort) {
        v_currentSorting = p_sort;
    };
    
    this.getSort = function() {
        return v_currentSorting;
    };
    
    this.getAlteredData = function(p_data) {
        // only table for now...
        v_currentIndexes = [];
        for (var i = 0; i < p_data.table.length; ++i) {
            v_currentIndexes.push(i);
        }
        if (v_options.filter !== false) {
            filter(p_data.table);
        }
        if (v_options.sort !== false) {
            sort(p_data.table);
        }
        if (p_data.selection != undefined) {
            p_data.selection = updateSelection(mcopy(p_data.selection));
        }
        
        return p_data;
    };
    
    this.getUnalteredIndex = function(p_index) {
        if (v_currentIndexes.length != 0) {
            return v_currentIndexes[p_index];
        } else {
            return p_index;
        }
    };
    
    /** private functions */
    
    function filter(data) {
        if (v_currentFilter != undefined && v_currentFilter != "") {
            var regexps = [];
            var strings = v_currentFilter.split(" ");
            for (var i = 0; i < strings.length; ++i) {
                regexps.push(new RegExp(strings[i], "gi"));
            }
            
            var index = 0;
            var currentIndex = 0;
            while (index < data.length) {
                if (!matchesAll(data[index], regexps)) {
                    data.splice(index, 1);
                    v_currentIndexes.splice(index, 1);
                } else {
                    ++index;
                }
                ++currentIndex;
            }
        }
    }
    
    function matchesAll(obj, regexps) {
        var isAMatch = true;
        for (var i = 0; i < regexps.length && isAMatch; ++i) {
            isAMatch = isAMatch && matches(obj, regexps[i]);
        }
        return isAMatch;
    }
    
    function matches(obj, regexp) {
        // TODO this will not work with element relay's getListWithElementInfo()
        var isAMatch = false;
        for (var i = 0; i < obj.length && !isAMatch; ++i) {
            if ($.type(obj[i].val) === "string" && obj[i].val.match(regexp) != null) {
                isAMatch = true;
            } else if ($.type(obj[i].val) === "array") {
                isAMatch = matches(obj[i].val, regexp);
            }
        }
        return isAMatch;
    }
    
    function sort(data) {
        var insertTo = 0;
        var dataLength = data.length;
        for (var i = 0; i < dataLength; ++i) {
            var min = findNext(data, insertTo);
            changeOrder(data, insertTo, min);
            ++insertTo;
        }
    }
    
    function findNext(data, p_from) {
        var index = p_from;
        for (var i = p_from + 1; i < data.length; ++i) {
            if (checkSort(data[i], data[index])) {
                index = i;
            }
        }
        return index;
    }
    
    function checkSort(obj1, obj2) {
        if (v_currentSorting != undefined) {
            var index = v_currentSorting.sortBy;
            var method = v_currentSorting.sortDirection;
            // TODO index is not correct when obj[1] contains the children and is > 0
            // It will also not work with element relay's getListWithElementInfo()
            if ((method == "asc" && obj1[index].val < obj2[index].val) || (method == "desc" && obj1[index].val > obj2[index].val)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    function changeOrder(data, insertTo, min) {
        if (insertTo != min) {
            var obj = data[min];
            var index = v_currentIndexes[min];
            data[min] = data[insertTo];
            v_currentIndexes[min] = v_currentIndexes[insertTo];
            data[insertTo] = obj;
            v_currentIndexes[insertTo] = index;
        }
    }
    
    function updateSelection(selection) {
        if (v_currentIndexes.length != 0) {
            var i = 0;
            while (i < selection.length) {
                var index = v_currentIndexes.indexOf(selection[i]);
                if (index != -1) {
                    selection[i] = index;
                    ++i;
                } else {
                    selection.splice(i, 1);
                }
            }
        }
        return selection;
    }
}

CViewModel_FilterAndSort.getHelp = function() {
    return "This viewmodel can be used with a table to filter and sort its data. It will only work correctly if the request is not already filtered.";
}

CViewModel_FilterAndSort.providesInterface = function() {
    return ["getUnalteredIndex", "getAlteredData", "getSort", "setSort", "getFilter", "setFilter"];
};

CViewModel_FilterAndSort.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_FilterAndSort",
        "type": "object",
        "properties": {
            "sort": {
                "type": "boolean",
                "format": "checkbox",
                "description": "Allow sorting the data (default true)",
                "default": false
            },
            "filter": {
                "type": "boolean",
                "format": "checkbox",
                "description": "Allow filtering the data (default true)",
                "default": false
            },
            "initialSorting": {
                "type": "object",
                "description": "The initial sorting of the data",
                "properties": {
                    "sortBy": {
                        "type": "integer",
                        "description": "The index of the data we sort on",
                        "default": 0
                    },
                    "sortDirection": {
                        "type": "string",
                        "enum": ["asc", "desc"],
                        "description": "The direction of the sort",
                        "default": "asc"
                    }
                },
                "required": ["sortBy", "sortDirection"],
                "additionalProperties": false
            },
            "initialFilter": {
                "type": "string",
                "description": "The initial filter string"
            }
        },
        "additionalProperties": false
    };
};

CViewModel_FilterAndSort.expectsConnection = function() {
    return {
        "dataConnections": [],
        "selectionConnections": []
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_FilterAndSort.js
