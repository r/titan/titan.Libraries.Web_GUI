// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_CodeEditor(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    var v_dataPaths = [];
    
    /** public functions - interface for parent */
    
    this.setSelectionToControl = function(p_selection) {};
    this.setBinder = function(p_binder) {};
    this.setReponseDataPath = function(p_index, p_path) {
        v_dataPaths[p_index] = p_path;
    };
	
    /** public functions - interface for views */ 
    
    this.getTextData = function(callback) {
        var returnValue;
        var response = v_viewmodel.getResponseElement(v_dataPaths[0]);
        if (response != undefined && response.node != undefined) {
            if (response.node.tp == 5) {
                callback(hex2a(response.node.val));
            } else {
                callback(response.node.val);
            }
        } else {
            callback("");
        }
    };
    
    this.setTextData = function(text) {
        var response = v_viewmodel.getResponseElement(v_dataPaths[0]);
        if (response != undefined && response.node != undefined) {
            if (response.node.tp == 5) {
                v_viewmodel.setResponseElement(v_dataPaths[0], a2hex(text));
            } else {
                v_viewmodel.setResponseElement(v_dataPaths[0], text);
            }
        }
    };
}

CViewModel_CodeEditor.getHelp = function() {
    return "A viewmodel that can be used for editing code data.";
}

CViewModel_CodeEditor.providesInterface = function() {
    return ["getTextData", "setTextData"];
};

CViewModel_CodeEditor.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_CodeEditor",
        "type": "object",
        "properties": {},
        "additionalProperties": false
    };
};

CViewModel_CodeEditor.expectsConnection = function() {
    var dataConnections = [
        {
            "valueType": ["charstringType", "octetstringType"]
        }
    ];
    
    var selectionConnections = [];
    
    return {
        "dataConnections": dataConnections,
        "selectionConnections": selectionConnections
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_CodeEditor.js