// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
/** Common functionality for applications, such as
 - mainLoop,
 - request issuing
 - response comparison
 - binding between view and viewModel */

var WebApplications = WebApplications || [];

WebApplications.push({'application': new CustomizableApp()});

function CustomizableApp() {
    "use strict";

    var m_appBase = new WebAppBase();
    var m_DataSourceUtils = new DataSourceUtils();
    var m_webAppModel;
    var m_viewModel;
    var m_view;
    var m_Binder;
    var m_model;
    var m_frameWork;

    var cssfiles = [];

    this.info = function() {
        return {
            defaultIcon: "WebApplications/CustomizableApp/Res/main_icon.png",
            defaultName: "CustomizableApp"
        };
    };

    this.load = function(webAppModel, params, framework)  {
        m_frameWork = framework;
        var jsfiles = [
            "WebApplications/CustomizableApp/ViewModel.js",
            "WebApplications/CustomizableApp/View.js",
            "WebApplications/CustomizableApp/Model.js"
        ];
        var alwaysLoadableJsFiles = [];

        function setupLoaded(ok, setup, setupName) {
            if (ok) {
                // load js and css files of main files, views and viewmodels, start the application
                new MultipleDirectoryListTask(
                    [
                        "WebApplications/CustomizableApp/Views",
                        "WebApplications/CustomizableApp/ViewModels",
                        params.customization + "/ViewModels",
                        params.customization + "/Views"
                    ],
                    m_webAppModel.getFileHandler()
                ).taskOperation(function(ok, resources) {
                    jsfiles = jsfiles.concat(resources.jsfiles);
                    cssfiles = resources.cssfiles;
                    //htmlfiles = resources.htmlfiles;
                    m_appBase.load(jsfiles, alwaysLoadableJsFiles, start, m_webAppModel.getFileHandler());
                });
            } else {
                alert("Failed to load setup " + setupName);
            }
        }

        function appConfigLoaded(config) {
            if (config.filesToInclude != undefined) {
                alwaysLoadableJsFiles = config.filesToInclude;
            }

            if (params.setup != undefined) {
                m_webAppModel.getSetupModel().loadSetup(params.setup, setupLoaded, false, params.setupParams);
            } else if (config.setup != undefined) {
                m_webAppModel.getSetupModel().loadSetup(config.setup, setupLoaded, false, config.setupParams);
            } else {
                alert('No setup was specified in config.');
            }
        }

        m_webAppModel = webAppModel;
        // set setup directory
        if (params.customization != undefined) {
            m_webAppModel.getSetupModel().setSetupDirectory(params.customization + '/Setups');
        } else {
            alert('Customizable app customization directory not set in params.');
            return;
        }

        // load app config
        m_webAppModel.loadAppConfig(params.customization + '/AppConfig.json', appConfigLoaded);
    };

    this.unload = function(webappUnloaded) {
        m_appBase.unload(destroy);
        webappUnloaded(true);
    };

    /** private functions */

    function start(p_callback) {
        m_model = new CCustomizableApp_Model(m_webAppModel, m_frameWork);
        m_viewModel = new CViewModel(m_model, m_DataSourceUtils);
        m_view = new CView(m_viewModel, "customAppMainview", "TSGuiFrameworkMain");

        function callback(ok, msg) {
            if (ok) {
                m_webAppModel.getFileHandler().loadCssFiles(cssfiles, "customAppMainview");
                m_viewModel.applicationCreated();
                m_viewModel.initRequestSelectionsAndFiltersAtStart();
                m_view.applicationCreated();
                m_Binder = new CBinder(m_viewModel, m_view, m_DataSourceUtils);
                m_Binder.start();
            } else {
                alert(msg);
            }
            if (typeof p_callback === "function") {
                p_callback();
            }
        }

        var taskList = new SyncTaskList([new GenericTask(m_viewModel.init), new GenericTask(m_view.init)], callback);

        taskList.taskOperation();
    }

    function destroy() {
        m_view.destroy();

        m_Binder.stop();
        m_Binder = undefined;

        m_view = undefined;
        m_viewModel = undefined;
    }
}

function CBinder(aViewModel, aView, aDataSourceUtils)
{
    "use strict";

    /** constructor and private members */
    var mOutstandingRequests = 0;
    var mLastResponse;
    var mViewModel = aViewModel;
    var mView = aView;
    var ERunningState = {EStopped:0, ERunning:1, EStopping:2};
    var mRunning = ERunningState.EStopped;
    var mThis = this;
    var mRefreshInterval = 3000;
    if (mViewModel.getUIConfig().refreshInterval != undefined) {
        //mRefreshInterval = mViewModel.getUIConfig().refreshInterval;
    }
    var mDataSourceUtils = aDataSourceUtils;

    aViewModel.setBinder(this);
    var mInterval;

    /** public functions */
    this.start = function()
    {
        if (mRunning !== ERunningState.ERunning)
        {
            mRunning = ERunningState.ERunning;
            mThis.mainLoop();
            if (mRefreshInterval != -1) {
                mInterval = setInterval(mThis.mainLoop, mRefreshInterval);
            }
        }
    };

    this.stop = function()
    {
        mRunning = ERunningState.EStopping;
        if (mInterval != undefined)
            clearInterval(mInterval);
        mInterval = undefined;
    };

    function searchObjecForValue(obj, query)
    {
        for (var key in obj) 
        {
            var value = obj[key];
            if (typeof value === 'object')
                return searchObjecForValue(value, query);
            if (typeof value == "string" && value.startsWith(query)) 
                return value;
        }
        return false;
    }

    this.notifyChange = function(aDisableViews, aFiredFromLoop, aResponseInCaseOfSetData)
    {
        if (aResponseInCaseOfSetData)
        {
            var val = searchObjecForValue(aResponseInCaseOfSetData, "GoodbyeMessage:");
            if (val)
            {
                mView.destroy();
                mView.displayGoodbyeMessage(val.substring(val.indexOf(':') + 1));
                mThis.stop();
            }
        }
        function dataArrived(aData) {
            if (aDisableViews && mViewModel.getUIConfig().overlayEnabledOnSelect) {
                mView.enableViews();
            }
            responseArrived(aData)
        }

        if (mOutstandingRequests == 0 || !aFiredFromLoop)
        {
            if (aDisableViews && mViewModel.getUIConfig().overlayEnabledOnSelect) {
                mView.disableViews();
            }
            mViewModel.getChangedData(dataArrived);
            mOutstandingRequests++;
        }
    };

    this.mainLoop = function()
    {
        if (mRunning === ERunningState.ERunning)
        {
            mThis.notifyChange(false, true);
        }
    };

    this.reloadSetup = function()
    {
        mViewModel.getChangedData(function(aResponse) {
            mView.destroySetupOnly();
            mView.reInitSetup();
            mViewModel.applicationCreated();
            mView.applicationCreated();
            mView.refreshView(true);
            mLastResponse = null;
            mThis.enableViews();
            mThis.start();
        });
    }

    this.enableViews = function() {
        mView.enableViews();
    };

    this.disableViews = function(aDoNotShowSpinningCircle) {
        if (mRunning == ERunningState.ERunning)
            mView.disableViews(aDoNotShowSpinningCircle);
    };

    this.isRunning = function() {
        return mRunning === ERunningState.ERunning;
    }

    /** private functions */
    function responseArrived(aData)
    {
        mOutstandingRequests--;
        if (aData)
        {
            if (mRunning === ERunningState.ERunning)
            {
                if (!mDataSourceUtils.dataSkeletonEquals(mLastResponse, aData))
                {
                    if (mDataSourceUtils.isResponseToRequest(aData, mViewModel.getRequest()))
                    {
                        mLastResponse = aData;
                        mViewModel.initRequestSelectionsAndFilters();
                        mView.refreshView(true);
                    }
                }
                else
                {
                    mView.refreshView(false);
                }

            }
            else if (mRunning === ERunningState.EStopping)
            {
                mRunning = ERunningState.EStopped;
            }
        }
    }
}
//# sourceURL=CustomizableApp\Main.js
