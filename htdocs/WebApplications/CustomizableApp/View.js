// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView(a_viewModel, a_ID, a_parentID)
{
    "use strict";

    /** private members and constructor */
    var HTML = "WebApplications/CustomizableApp/View.html";
    var CSS = "WebApplications/CustomizableApp/View.css";
    var CSSNodeId = "WebAppStyle";
    var CSSNodeIdSetup = "SetupStyle";
    var CustomAppSetupID = "CustomAppSetup";
    var m_parentDiv = document.getElementById(a_parentID);
    var m_ID = a_ID;
    var m_viewModel = a_viewModel;
    var m_this = this;
    var m_views = [];

    /** public functions */
    this.reInitSetup = function(a_callback)
    {
        $("#" + CSSNodeIdSetup).html(m_viewModel.getSetupStyle());
        $("#" + CustomAppSetupID).html(m_viewModel.getSetupHtml());
    }

    this.init = function(a_callback)
    {
        var mainDiv = document.createElement("div");
        mainDiv.setAttribute("id", m_ID);
        m_parentDiv.appendChild(mainDiv);

        function htmlLoaded(ok, data) {
            if (ok) {
                $("#" + m_ID).append(data);
                a_viewModel.getFileHandler().loadCss(CSSNodeId, CSS);
                m_this.reInitSetup();
                a_callback(true);
            } else {
                a_callback(false, "Error loading " + HTML);
            }
        }

        m_viewModel.loadFile(HTML, htmlLoaded);
    };

    this.destroy = function() {
        $("#" + m_ID).remove();
        $(window).off("resize", onWindowResize);
    };

    this.destroySetupOnly = function() {
        $(".ui-tabs").tabs("option", "disabled", true);
        $("#" + CustomAppSetupID).empty();
        $("#" + CSSNodeIdSetup).empty();        
        m_views = [];
    };

    this.applicationCreated = function()
    {
        m_views = [];
        var viewDescriptors = m_viewModel.getViewDescriptors();
        var viewCount = viewDescriptors.length;
        var initedViewIDs = {}; /** stores inited info for parentIDs */
        var idDependencyMap = {}; /** maps creatingIDs to parentIDs */
        for (var i = 0; i < viewCount; ++i)
        {
            viewDescriptors[i].customData.parentID = viewDescriptors[i].parentID;
            if (viewDescriptors[i].idsCreating)
            {
                viewDescriptors[i].customData.idsCreating = viewDescriptors[i].idsCreating;
                for (var j = 0; j < viewDescriptors[i].customData.idsCreating.length; ++j)
                    if (!idDependencyMap[viewDescriptors[i].customData.idsCreating[j]])
                        idDependencyMap[viewDescriptors[i].customData.idsCreating[j]] = viewDescriptors[i].customData.parentID;
                    else
                        alert ("The same ID is created by multiple views. ID " + viewDescriptors[i].customData.idsCreating[j] + " is created by at least these 2 views: " + idDependencyMap[viewDescriptors[i].customData.idsCreating[j]] + " and " + viewDescriptors[i].customData.parentID);
            }
        }
        //TODO: check if such a check is needed: alert ("Multiple views placed on the same ID. On ID " + viewDescriptors[i].customData.idsCreating[j] + " are placed more at least these 2 IDs: " + idDependencyMap[viewDescriptors[i].customData.idsCreating[j]] + " and " + viewDescriptors[i].customData.parentID);
        var alertIndxs = [];
        var lastNrOfInitedViews = 0;
        while (Object.keys(initedViewIDs).length < viewCount)
        {
            var lastTryingID = "noID";
            var lastTryingParentID = "noID";
            for (var i = 0; i < viewCount; ++i)
            {
                if (window[viewDescriptors[i].class] && viewDescriptors[i].viewModelIndexes)
                {
                    /** if view is not inited yet AND (it has no parent dependency that needs to be created, OR it has dependency, but the dependency is already inited) */
                    if (initedViewIDs[viewDescriptors[i].customData.parentID] != "inited" && (!idDependencyMap[viewDescriptors[i].customData.parentID] || initedViewIDs[idDependencyMap[viewDescriptors[i].customData.parentID]] == "inited"))
                    {
                        var lVMs = m_viewModel.getSubViewModels(viewDescriptors[i].viewModelIndexes);
                        if (lVMs.length == 0)
                          lVMs = [m_viewModel];
                        
                        var lId = "subView" + i;
                        m_views[i] = new window[viewDescriptors[i].class](lVMs, lId, viewDescriptors[i].customData.parentID, viewDescriptors[i].customData);
                        if (m_views[i].applicationCreated) {
                            m_views[i].applicationCreated();
                            ViewUtils.applyCss(viewDescriptors[i].customData, lId);
                            ViewUtils.processCss(viewDescriptors[i].customData, viewDescriptors[i].customData.parentID);
                        }
                        initedViewIDs[viewDescriptors[i].customData.parentID] = "inited";
                        if (!$("#" + viewDescriptors[i].customData.parentID).length)
                        {
                            if (alertIndxs[i] != "alertDisplayed")
                            {
                                alert ("No parent found for a view of class '"+viewDescriptors[i].class+"'. Missing object's ID is: " + viewDescriptors[i].customData.parentID);
                                alertIndxs[i] = "alertDisplayed";
                            }
                        }
                    }
                    else
                    {
                        if (initedViewIDs[idDependencyMap[viewDescriptors[i].customData.parentID]] != "inited")
                        {
                            lastTryingID = viewDescriptors[i].customData.parentID;
                            lastTryingParentID = idDependencyMap[viewDescriptors[i].customData.parentID];
                        }
                    }
                }
                else if (viewDescriptors[i].viewModelIndexes)
                {
                    alert("View class " + viewDescriptors[i].class + " does not exist!");
                }
            }
            if (lastNrOfInitedViews < Object.keys(initedViewIDs).length)
                lastNrOfInitedViews = Object.keys(initedViewIDs).length;
            else
            {
                alert("Parent dependency error with the view with ID \" " + lastTryingID + "\". Its parent view with ID \"" + lastTryingParentID + "\" cannot be found.");
                break;
            }
        }
        
        $(".ui-tabs").tabs("option", "disabled", false);
        
        $(window).on("resize", onWindowResize);
        $("#" + CustomAppSetupID).height(ViewUtils.getSuggestedHeight(CustomAppSetupID));
    };
    
    function onWindowResize(event) {
        if (event.target == window) {
            $("#" + CustomAppSetupID).height(ViewUtils.getSuggestedHeight(CustomAppSetupID));
        }
    }

    this.refreshView = function(aFullRefresh)
    {
        /* aFullRefresh is True if the dataset's structure is changed. */
        var viewCount = m_views.length;
        for (var i = 0; i < viewCount; i++)
            if (m_views[i] && m_views[i].refresh)
                m_views[i].refresh(aFullRefresh);
    };
    
    this.disableViews = function(aDoNotShowSpinningCircle) {
        var opacity = m_viewModel.getUIConfig().overlayOpacity;
        if (opacity == undefined) {
            opacity = 0.0;
        }
        if ($(".ui-widget-overlay")[0] == undefined) {
            var html;
            if (aDoNotShowSpinningCircle) {
                html = '<div class="ui-widget-overlay ui-front" style="z-index: 1000; opacity: ' + opacity + ';"></div>';
            } else {
                html = '<div class="ui-widget-overlay ui-front" style="z-index: 1000; opacity: ' + opacity + ';"></div><center><img src="WebApplicationFramework/Res/waiting.gif" class="waitingImage"></center>';
            }
            $(m_parentDiv).append(html);
        }
    };
    
    this.enableViews = function() {
        $(".ui-widget-overlay").remove();
        $(".waitingImage").remove();
    };
    
    this.displayGoodbyeMessage = function(aMessage) {
        $(m_parentDiv).html('<div style="height: 100px;"></div> <div style="height: 100px; line-height: 100px; text-align: center; font-size: 20px;background-color: #6494e9;">' + aMessage + '</div>');
    };

    /** private functions */
}
//# sourceURL=CustomizableApp\Views\View.js
