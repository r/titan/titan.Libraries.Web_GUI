// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function TitansimConfiguration_View(p_viewmodel, p_parentId, p_mainId) {
    "use strict";

    var EDITOROPTIONS = {
        mode: "javascript",
        lineNumbers: true,
        smartIndent: true,
        indentUnit: 4,
        lineWrapping: true,
        foldGutter: true,
        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
        matchBrackets: true,
        autoCloseBrackets: true,
        highlightSelectionMatches: true,
        styleActiveLine: true
    };

    var HTML = "WebApplications/TitansimConfiguration/View.html";

    var v_parentId = p_parentId;
    var v_mainId = p_mainId;

    var v_viewmodel = p_viewmodel;
    var v_editor;
    var v_this = this;

    this.init = function(p_callback) {
        var mainDiv = document.createElement("div");
        mainDiv.setAttribute("id", v_mainId);
        $("#" + v_parentId).append(mainDiv);

        function htmlLoaded(ok, data) {
            if (ok) {
                $('#' + v_mainId).append(data);
                v_viewmodel.loadCss("TitansimConfiguration_WebAppStyle", "WebApplications/TitansimConfiguration/View.css");
                $("#TitansimConfiguration_Save").prop("disabled", "false");
                v_editor = CodeMirror(document.getElementById("TitansimConfiguration_Editor"), EDITOROPTIONS);
                v_editor.setSize("100%", "100%");

                p_callback(true);
            } else {
                p_callback(false, "Error loading " + HTML);
            }
        }

        v_viewmodel.loadFile(HTML, htmlLoaded);
    };

    this.applicationCreated = function() {
        setupCallbacks();
    };

    function refresh(ok, data) {
        if (ok) {
            v_editor.setValue(data);
            $("#TitansimConfiguration_Save").prop("disabled", false);
        } else {
            v_editor.setValue("");
            alert("Failed to load file");
            $("#TitansimConfiguration_Save").prop("disabled", true);
            $("#TitansimConfiguration_Title").html("Currently editing:");
        }
    }

    function load() {
        function fileSelected(selected) {
            $("#TitansimConfiguration_Title").html("Currently editing: " + selected[0].text);
            v_viewmodel.load(selected[0].value, refresh);
        }

        var dialog = FileDialog(v_viewmodel.getFileHandler(), v_mainId, v_mainId + "_LoadFile", {
            "header": "Select a file to load",
            "text": "Please select a file to load",
            "fileTypeList": ["*.cfg"],
            "callback": fileSelected
        });

        dialog.open();
    }

    function save() {
        v_viewmodel.save(v_editor.getValue());
    }

    function restart() {
        var div = document.createElement("div");
        div.setAttribute("class", "ui-widget-overlay ui-front");
        div.setAttribute("style", "z-index: 100;");
        $("#" + v_parentId).append(div);

        function callback() {
            $(div).remove();
        }

        v_viewmodel.restart(callback);
    }

    function setupCallbacks() {
        $("#TitansimConfiguration_Load").click(load);
        $("#TitansimConfiguration_Save").click(save);
        $("#TitansimConfiguration_Restart").click(restart);
    }

}
//# sourceURL=TitansimConfiguration\View.js
