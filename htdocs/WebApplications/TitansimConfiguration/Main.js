// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
var WebApplications = WebApplications || [];

WebApplications.push({'application': new TitansimConfigurationApp()});

function TitansimConfigurationApp() {
    "use strict";
    var main = new WebAppBase();
    var webAppModel;
    
    var jsfiles = [
        "WebApplications/TitansimConfiguration/ViewModel.js",
        "WebApplications/TitansimConfiguration/View.js"
    ];
    
    this.info = function() {
        return {
            defaultIcon: "WebApplications/TitansimConfiguration/main_icon.png",
            defaultName: "Titansim Configuration"
        };
    };
    
    this.load = function(p_webAppModel) {
        webAppModel = p_webAppModel;
        main.load(jsfiles, [], start, webAppModel.getFileHandler());
    };
    
    function destroy() {
        $("#ConfigEditor_MainView").remove();
    }

    this.unload = function(webappUnloaded) {
        main.unload(destroy);
        webappUnloaded(true);
    };
    
    function start(p_callback) {
        var viewmodel = new TitansimConfiguration_ViewModel(webAppModel.getFileHandler());
        var view = new TitansimConfiguration_View(viewmodel, "TSGuiFrameworkMain", "ConfigEditor_MainView");
        
        function initDone(ok, msg) {
            if (ok) {
                view.applicationCreated();
            } else {
                alert("Initializing config editor failed");
            }
            
            if (typeof p_callback === "function") {
                p_callback();
            }
        }
        
        view.init(initDone);
    }
}

//# sourceURL=TitansimConfiguration\Main.js
