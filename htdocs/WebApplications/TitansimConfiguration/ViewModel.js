// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function TitansimConfiguration_ViewModel(p_fileHandler) {
    "use strict";

    var v_fileHandler = p_fileHandler;
    var v_loader;

    this.loadFile = v_fileHandler.loadFile;
    this.loadCss = v_fileHandler.loadCss;
    
    this.load = function(url, refresh) {
        v_loader = new FileLoader(url, v_fileHandler);
        v_loader.taskOperation(refresh);
    }
    
    this.save = function(data) {
        v_loader.setData(data);
        v_loader.save();
    };
    
    this.restart = function(callback) {
        v_fileHandler.setData(function() {}, "ExecCtrl", "Restart", "0", 0);
        
        function checkOffline() {
            ping(false, callback);
        }
        setTimeout(function() {
            ping(callback);
        }, 10000);
    };
    
    function ping(callback) {
        function gotAnswer(ans) {
            if (ans) {
                callback();
            } else {
                setTimeout(function() {
                    ping(callback);
                }, 1000);
            }
        }
        
        v_fileHandler.loadFile("Main.html", gotAnswer);
    }
    
    this.getFileHandler = function() {
        return v_fileHandler;
    };
}
//# sourceURL=TitansimConfiguration\ViewModel.js
