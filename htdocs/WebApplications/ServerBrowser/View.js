// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function ServerBrowser_View(p_viewmodel) {
    "use strict";
    
    var HTML = "WebApplications/ServerBrowser/View.html";
    var CSS = "WebApplications/ServerBrowser/View.css";
    var CSSNodeId = "WebAppStyle";
    var v_mainId = "servercontainer";
    var v_mainSelector = '#' + v_mainId;
    
    var v_viewmodel = p_viewmodel;
    var v_this = this;
    
    this.init = function(p_callback) {
        var mainDiv = document.createElement("div");
        mainDiv.setAttribute("id", v_mainId);
        $("#TSGuiFrameworkMain").append(mainDiv);
        
        function htmlLoaded(ok, data) {
            if (ok) {
                $(v_mainSelector).append(data);
                // View's HTML fragment loaded, now go for the CSS.
                v_viewmodel.loadCss(CSSNodeId, CSS);
                // Load files of the root dir
                fillPanel('/', 'leftlist');
                fillPanel('/', 'rightlist');
                
                p_callback(true);
            } else {
                p_callback(false, "Error loading " + HTML);
            }
        }
        
        v_viewmodel.loadFile(HTML, htmlLoaded);
    };
    
    this.destroy = function() {
        $(v_mainSelector).remove();
    };

    this.applicationCreated = function(request) {
    };
    
    function fillPanel(path, p_id) {
        v_viewmodel.ls(path, function(d) {
            var dirs = d.sort(sortLs);
            var v_ul = document.createElement('ul');
            var v_idSelector = '#' + p_id;
            if (path !== "/") {
                var v_liup = document.createElement('li');
                v_liup.setAttribute('path', pathOneUp(path));
                v_liup.setAttribute('filetype', "-d");
                v_liup.innerHTML = "..";
                v_ul.appendChild(v_liup);
            }
            for (var k in dirs) {
                if (dirs.hasOwnProperty(k)) {
                    var v_li = document.createElement('li');
                    var v_value = dirs[k];
                    if (v_value.fileName[0] != "/")
                        v_value.fileName = "/" + v_value.fileName;
                    if (v_value.fileName.endsWith("/"))
                        v_value.fileName = v_value.fileName.slice(0, -1);
                    var v_displayname = v_value.fileName.substr(v_value.fileName.lastIndexOf('/') + 1);
                    v_li.setAttribute('path', v_value.fileName);
                    v_li.setAttribute('filetype', v_value.contentType);
                    if (v_value.contentType.endsWith("d"))
                        v_li.innerHTML = "<span class='name'>" + v_displayname + "</span>";
                    else if (v_value.contentType.endsWith("f"))
                        v_li.innerHTML = "<span class='name'>" + v_displayname + "</span><span class='size'>" + v_value.size + "</span><span class='time'>" + (new Date(v_value.timestamp * 1000)).toISOString() + "</span>";
                    else
                        v_li.innerHTML = v_value.contentType + ": <span class='name'>" + v_displayname + "</span><span class='size'>" + v_value.size + "</span><span class='time'>" + (new Date(v_value.timestamp * 1000)).toISOString() + "</span>";
                    v_ul.appendChild(v_li);
                }
            }
            $('ul', v_idSelector).remove();
            $(v_idSelector, v_mainSelector).siblings(".bread").text(path);
            $(v_idSelector, v_mainSelector).append(v_ul);
            $('ul', v_idSelector).off('click', 'li', onItemClick);
            $('ul', v_idSelector).on('click', 'li', onItemClick);
        });
    }
    
    function onItemClick(event) {
        var v_parentId = event.currentTarget.parentElement.parentElement.id;
        var v_newpath = event.currentTarget.getAttribute('path');
        var v_filetype = event.currentTarget.getAttribute('filetype');
        if (v_filetype.endsWith("d")) {
            fillPanel(v_newpath, v_parentId);
        } else {
            download(v_newpath.substr(v_newpath.lastIndexOf("/")+1), v_newpath);
        }
        return false;
    }
    
    function sortLs(a, b) {
        var isDirA = (a.contentType.endsWith("d")), isDirB = (b.contentType.endsWith("d"));
        // Directories first.
        if (isDirA && !isDirB) {
            return -1;
        } else if (isDirB && !isDirA) {
            return 1;
        } else {
            return a.fileName.localeCompare(b.fileName);
        }
    }
    
    function pathOneUp(path) {
        if (path.slice(-1) == "/")
            path = path.slice(0, -1);
        return path.substr(0, path.lastIndexOf("/") + 1);
    }
    
    function download(filename, path) {
        var element = document.createElement('a');
        element.setAttribute('href', path);
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }
}
//# sourceURL=ServerBrowser\View.js
