// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
var WebApplications = WebApplications || [];

WebApplications.push({'application': new ServerBrowserApp()});

function ServerBrowserApp() {
    "use strict";

    var main = new WebAppBase();
    var webAppModel;
    var view;
    var viewModel;
    
    var jsfiles = [
        "WebApplications/ServerBrowser/ViewModel.js",
        "WebApplications/ServerBrowser/View.js"
    ];
    
    this.info = function() {
        return {
            defaultIcon: "WebApplications/ServerBrowser/images/main_icon.png",
            defaultName: "Server Browser"
        };
    };
    
    this.load = function(p_webAppModel)  {
        webAppModel = p_webAppModel;
        main.load(jsfiles, [], start, webAppModel.getFileHandler());
    };
    
    function destroy() {
        view.destroy();
        viewModel.destroy();
        
        view = undefined;
        viewModel = undefined;
    }
    
    this.unload = function(unloadFinished) {
        main.unload(destroy);
        unloadFinished(true);
    };
    
    function start(p_callback) {
    viewModel = new ServerBrowser_ViewModel(webAppModel.getFileHandler());
    view = new ServerBrowser_View(viewModel);
    view.init(function noop(){});
    if(p_callback)
        p_callback();
    view.applicationCreated();
    }
}
//# sourceURL=ServerBrowser\Main.js
