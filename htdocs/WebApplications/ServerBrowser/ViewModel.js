// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function ServerBrowser_ViewModel(p_fileHandler) {
    "use strict";

    var v_fileHandler = p_fileHandler;

    this.init = function(p_callback) {
        p_callback(true);
    };

    this.destroy = function() {};

    this.loadFile = v_fileHandler.loadFile;
    this.loadCss = v_fileHandler.loadCss;

    this.ls = v_fileHandler.getDirectory;
}
//# sourceURL=ServerBrowser\ViewModel.js
