#!/bin/sh
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

CURRENT_DIR=`pwd`
#---COPY_TO="."
COPY_TO="./WebGUI"
#---CLL_DIR=`readlink -f $0 | sed 's/\/test.*//g'`
CLL_DIR="../"
DO_ZIP=0

showUsage()
{
    echo "Usage: $0 <options>"
    echo "The script will create the offline version of WebGUI."
    echo "Options:"
    echo "  -t [directory]: target directory to create the package in, default: $COPY_TO"
    echo "  -c [directory]: the CLL respository, default: $CLL_DIR"
    echo "  -z : enable zip output, default: disabled"
}

while getopts t:c:z o
do
    case "$o" in 
        t) COPY_TO=$OPTARG;;
        c) CLL_DIR=$OPTARG;;
        z) DO_ZIP=1;;
        [?]) showUsage; exit 1;;
    esac
done

WEBGUI_SRC=$CLL_DIR/htdocs
API_DIR=$CLL_DIR/htdocs/Utils/DsRestAPI/
CUST_DIR=$CLL_DIR/test/TestContent
TESTCASES_DIR=$CLL_DIR/test/DsRestAPI/JSON_reqResp

MOCK_SERVER_DIR=$CLL_DIR/test/MockedServer
MOCKTEST_DIR=$CLL_DIR/test/MockTest
MOCKTEST_DSSERVERDESCRIPTOR=$CLL_DIR/test/DsRestAPI/DsRestAPITestConfig.json
SELENIUM_DIR=$CLL_DIR/test/Selenium

API_LOC=$COPY_TO/htdocs/Utils/DsRestAPI
CUST_LOC=$COPY_TO/htdocs/CustomizableContent
WEBAPP_LOC=$COPY_TO/htdocs/WebApplications
TESTCASES_LOC=$WEBAPP_LOC/MockTest/Testcases
SELENIUM_LOC=$COPY_TO/htdocs/Selenium

MAIN_HTML=$COPY_TO/htdocs/Main.html
TEST_HETML=$COPY_TO/htdocs/Test.html

# create target directory if it does not exist
if [ ! -d $COPY_TO ]; then
    mkdir $COPY_TO
fi

# check if offline WebGui already exists and delete it if it does
if [ -e $COPY_TO/deleteOfflineWebGUI.sh ]; then
    cd $COPY_TO
    ./deleteOfflineWebGUI.sh
    cd $CURRENT_DIR
fi

cp -arL $WEBGUI_SRC $COPY_TO

# check if the symbolic link to the js API exists and delete it if necessary
if [ -e $API_LOC ]; then
    rm -rf $API_LOC
fi
cp -arL $API_DIR $API_LOC

# check if the directory exsits (it usually does, not only when the server is running) and delete it
if [ -e $COPY_TO/htdocs/CustomizableContent ]; then
    rm -rf $COPY_TO/htdocs/CustomizableContent
fi
cp -arL $CUST_DIR $CUST_LOC

cp -arL $MOCK_SERVER_DIR $API_LOC
cp -arL $MOCKTEST_DIR $WEBAPP_LOC

cp -arL $SELENIUM_DIR $SELENIUM_LOC

cp -arL $TESTCASES_DIR/* $TESTCASES_LOC

cp -arL $MOCKTEST_DSSERVERDESCRIPTOR $WEBAPP_LOC/MockTest/DsRestAPITestConfig.js

awk '{
    if ($0 ~ /FileHandler.js/) {
        sub("FileHandler.js", "DsRestAPI/MockedServer/FileHandler.js");
        print $0;
    } else {
        print $0
    }
}' $MAIN_HTML > $TEST_HETML

cd $COPY_TO/htdocs
listOfFiles=`find | cut -b3- | awk '{if(NR>1){if(NR!=2){print ", "}print "\""$0"\""}}'`
# this will replace \n-s with spaces, so sed will work
listOfFiles=`echo $listOfFiles` 
cd $CURRENT_DIR

sed -i "s|REPLACETHIS|$listOfFiles|g" $API_LOC/MockedServer/FileHandler.js

if [ $DO_ZIP == 1 ]; then
    cd $COPY_TO
    tar -czvf WebGUI.tgz htdocs >> /dev/null
    rm -rf WebGUI
    cd $CURRENT_DIR
fi

cd $CURRENT_DIR

echo "rm -rf htdocs; rm -f WebGUI.tgz; rm -f deleteOfflineWebGUI.sh" > $COPY_TO/deleteOfflineWebGUI.sh
chmod u+x $COPY_TO/deleteOfflineWebGUI.sh
