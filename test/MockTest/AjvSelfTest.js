///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
function AjvSelfTest() {
    "use strict";

    var ajv = new Ajv({
            allErrors : true,
            format : "full",
            verbose : true,
            inlineRefs : false
        });
    /* selftest:
    The selftest.pass[].inputs are known good.
    The selftest.fail[].inputs are known bad.

    A selftest step fails if the validator's result does not match the expected result.

    This is for semantically checking our JSON schema.
     */

    var selftest = {
        "pass" : {
            "expects" : true,
            "inputs" : []
        },
        "fail" : {
            "expects" : false,
            "inputs" : []
        }
    };

    selftest.pass.inputs.push({
        "requests" : [{
                "getData" : {
                    "source" : "DsRestAPI_Test_DataSource",
                    "element" : "EntityGroups",
                    "params" : [],
                    "children" : [{
                            "getData" : {
                                "source" : "DsRestAPI_Test_DataSource2",
                                "element" : "EntityGroups2"
                            }
                        }
                    ]
                }
            }
        ]
    });
    selftest.pass.inputs.push({
        "requests" : [{
                "getData" : {
                    "source" : "DsRestAPI_Test_DataSource",
                    "element" : "EntityGroups"
                }
            }
        ]
    });
    selftest.pass.inputs.push({
        "requests" : [{
                "getData" : {
                    "source" : "DsRestAPI_Test_DataSource",
                    "element" : "EntityGroups",
                    "children" : [{
                            "getData" : {
                                "source" : "DsRestAPI_Test_DataSource",
                                "element" : "Scenarios",
                                "params" : [{
                                        "paramName" : "EntityGroup",
                                        "paramValue" : "%Parent0%"
                                    }
                                ],
                                "children" : [{
                                        "getData" : {
                                            "source" : "DsRestAPI_Test_DataSource",
                                            "element" : "TrafficCases",
                                            "params" : [{
                                                    "paramName" : "EntityGroup",
                                                    "paramValue" : "%Parent0%"
                                                }, {
                                                    "paramName" : "Scenario",
                                                    "paramValue" : "%Parent1%"
                                                }
                                            ],
                                            "children" : [{
                                                    "getData" : {
                                                        "source" : "DsRestAPI_Test_DataSource",
                                                        "element" : "TcStart",
                                                        "params" : [{
                                                                "paramName" : "EntityGroup",
                                                                "paramValue" : "%Parent0%"
                                                            }, {
                                                                "paramName" : "Scenario",
                                                                "paramValue" : "%Parent1%"
                                                            }, {
                                                                "paramName" : "TrafficCase",
                                                                "paramValue" : "%Parent2%"
                                                            }
                                                        ]
                                                    }
                                                }, {
                                                    "getData" : {
                                                        "source" : "DsRestAPI_Test_DataSource",
                                                        "element" : "TcStop",
                                                        "params" : [{
                                                                "paramName" : "EntityGroup",
                                                                "paramValue" : "%Parent0%"
                                                            }, {
                                                                "paramName" : "Scenario",
                                                                "paramValue" : "%Parent1%"
                                                            }, {
                                                                "paramName" : "TrafficCase",
                                                                "paramValue" : "%Parent2%"
                                                            }
                                                        ]
                                                    }
                                                }
                                            ]
                                        }
                                    }, {
                                        "getData" : {
                                            "source" : "DsRestAPI_Test_DataSource",
                                            "element" : "Dummy",
                                            "params" : [{
                                                    "paramName" : "EntityGroup",
                                                    "paramValue" : "%Parent0%"
                                                }, {
                                                    "paramName" : "Scenario",
                                                    "paramValue" : "%Parent1%"
                                                }
                                            ]
                                        }
                                    }, {
                                        "getData" : {
                                            "source" : "DsRestAPI_Test_DataSource",
                                            "element" : "ScStart",
                                            "params" : [{
                                                    "paramName" : "EntityGroup",
                                                    "paramValue" : "%Parent0%"
                                                }, {
                                                    "paramName" : "Scenario",
                                                    "paramValue" : "%Parent1%"
                                                }
                                            ]
                                        }
                                    }, {
                                        "getData" : {
                                            "source" : "DsRestAPI_Test_DataSource",
                                            "element" : "ScStop",
                                            "params" : [{
                                                    "paramName" : "EntityGroup",
                                                    "paramValue" : "%Parent0%"
                                                }, {
                                                    "paramName" : "Scenario",
                                                    "paramValue" : "%Parent1%"
                                                }
                                            ]
                                        }
                                    }
                                ]
                            }
                        }, {
                            "getData" : {
                                "source" : "DsRestAPI_Test_DataSource",
                                "element" : "EGrpType",
                                "params" : [{
                                        "paramName" : "EntityGroup",
                                        "paramValue" : "%Parent0%"
                                    }
                                ]
                            }
                        }, {
                            "getData" : {
                                "source" : "DsRestAPI_Test_DataSource",
                                "element" : "EGrpSize",
                                "params" : [{
                                        "paramName" : "EntityGroup",
                                        "paramValue" : "%Parent0%"
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        ]
    });

    selftest.pass.inputs.push({
        "requests" : []
    });

    selftest.fail.inputs.push({
        "requests" : [{
                "getData" : {
                    "source" : "DsRestAPI_Test_DataSource",
                    "element" : "EntityGroups",
                    "params" : [],
                    "children" : [{
                            "apple" : "20"
                        }
                    ]
                }
            }
        ]
    });
    selftest.fail.inputs.push({
        "horgxxp" : [{
                "xxetData" : {
                    "source" : "DsRestAPI_Test_DataSource"
                }
            }
        ]
    });
    selftest.fail.inputs.push({
        "requests" : [{
                "getData" : {
                    "source" : "DsRestAPI_Test_DataSource",
                    "element" : "EntityGroups",
                    "params" : [],
                    "children" : [{
                            "getData" : {
                                "source" : "DsRestAPI_Test_DataSource",
                                "element" : "Ent7658765ityGroups",
                                "children" : [{
                                        "getData" : {
                                            "source" : "DsRestAPI_Test_DataSource",
                                            "element" : "EntityGroug76ps",
                                            "params" : [],
                                            "children" : [{
                                                    "deep_apple" : "20"
                                                }
                                            ]
                                        }
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        ]
    });

    this.validate = function (schema) {
        var validateSchema = ajv.compile({
                "$ref" : "http://json-schema.org/draft-04/schema#"
            });
        var isSchemaValid = validateSchema(schema);
        if (!isSchemaValid) {
            console.error("Our JSON Schema is not syntactically valid per json-schema draft-04! Cannot continue...", validateSchema.errors);
            return false;
        }

        var validator = ajv.compile(schema);
        var validator_result = true;

        for (var key in selftest) {
            if (selftest.hasOwnProperty(key)) {
                for (var i = 0; i < selftest[key].inputs.length; ++i) {
                    var result = validator(selftest[key].inputs[i]);
                    if (result !== selftest[key].expects) {
                        validator_result = false;
                        console.error("Selftest failure: ", key + "[" + (i + 1) + "]: ", result + " returned but expected " + selftest[key].expects, "!!!", validator.errors);
                    }
                }
            }
        }

        return validator_result;
    };
}
