#!/usr/bin/env python
# -*- coding: utf-8 -*-
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

import re

from BaseTestCase import *
from Framework_CommonFunctions import *
from GuiEditor_CommonFunctions import *

from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.color import Color

class GuiEditor_EditorTests(BaseTestCase):

    def test_addEmptyRequest(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        newSetup(self.driver)
        
        addEmptyRequest(self.driver)
        self.assertTrue(editorTypeExists(self.driver, "JSONEditor"), "ElementEditor element not found after adding an empty request")
        
        closeButton = self.driver.wait.until(EC.element_to_be_clickable((By.ID, 'GuiEditor_ElementEditor_Close')))
        closeButton.click()
        
        self.assertTrue(treeExists(self.driver, "GuiEditor_RequestTree"), "There is no request tree, but there should be after adding an empty request")

    # ---------- adding editors ----------

    def test_addViewModel(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        newSetup(self.driver)
        
        addViewModel(self.driver, "CViewModel_DynamicTable")
        self.assertTrue(editorTypeExists(self.driver, "GuiEditor_ViewmodelEditor"), "ViewmodelEditor element not found after adding one")

    def test_addView(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        newSetup(self.driver)
        
        addView(self.driver, "CView_ElementTable")
        self.assertTrue(editorTypeExists(self.driver, "GuiEditor_ViewEditor"), "ViewEditor element not found after adding one")

    def test_addImport(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        newSetup(self.driver)
        
        addImport(self.driver, "Test2")
        self.assertTrue(editorTypeExists(self.driver, "GuiEditor_ImportEditor"), "ImportEditor element not found after adding one")

    # ---------- custom data editors ----------

    def test_validCustomData(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        
        editorId = "GuiEditor_ViewmodelEditor_0"
        openCustomDataEditor(self.driver, editorId)
        
        customData = '{"value":28}'
        script = "a = '" + customData + "';" + "view_editor = $('#GuiEditor_CustomDataEditor .CodeMirror')[0].CodeMirror; view_editor.setValue(a);"
        self.driver.execute_script(script)
        
        closeCustomDataEditor(self.driver)
        openCustomDataEditor(self.driver, editorId)
        
        script = "a = '" + customData + "';" + "view_editor = $('#GuiEditor_CustomDataEditor .CodeMirror')[0].CodeMirror; return view_editor.getValue();"
        textInEditor = self.driver.execute_script(script)
        # the editor formats the code, so we have to remove line breaks and spaces
        textInEditor = re.sub(r"\s", "", textInEditor)
        
        self.assertEqual(customData, textInEditor, "Custom data was not saved after closing the editor")

    def test_invalidCustomData(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        
        editorId = "GuiEditor_ViewmodelEditor_0"
        openCustomDataEditor(self.driver, editorId)
        
        customData = '{"value"28}'
        script = "a = '" + customData + "';" + "view_editor = $('#GuiEditor_CustomDataEditor .CodeMirror')[0].CodeMirror; view_editor.setValue(a);" 
        self.driver.execute_script(script)
        
        editorHeader = self.driver.find_element_by_id("GuiEditor_CustomDataEditor_Header")
        editorHeaderColor = editorHeader.value_of_css_property("color")
        self.assertEqual('#ff0000', str(Color.from_string(editorHeaderColor).hex), 'Custom data editor header should be red, it is ' + editorHeaderColor)

    # ---------- deleting editors ----------

    def test_delViewmodelEditor(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        
        rightClickLabel(self.driver, "GuiEditor_ViewmodelEditor_0")
        if not clickInContextMenu(self.driver, "delete"):
            self.fail("No option for delete in context menu of ViewmodelEditor")
        
        self.assertFalse(editorTypeExists(self.driver, "GuiEditor_ViewmodelEditor"), "ViewmodelEditor element found after it was deleted")

    def test_delViewEditor(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        
        rightClickLabel(self.driver, "GuiEditor_ViewEditor_0")
        if not clickInContextMenu(self.driver, "delete"):
            self.fail("No option for delete in context menu of ViewEditor")
        
        self.assertFalse(editorTypeExists(self.driver, "GuiEditor_ViewEditor"), "ViewEditor element found after it was deleted")

if __name__ == "__main__":
    unittest.main(catchbreak=True, verbosity=2)