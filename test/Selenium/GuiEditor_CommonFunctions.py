#!/usr/bin/env python
# -*- coding: utf-8 -*-
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from selenium.webdriver import ActionChains

# ---------- Handling main buttons ----------

def newSetup(driver):
    button = driver.wait.until(EC.element_to_be_clickable((By.ID, 'GuiEditor_Button_New')))
    button.click()
    time.sleep(0.5)

def loadSetup(driver, setupName):
    radioId = "GuiEditor_Dialog_LoadSetup_RadioButton_" + setupName
    loadButton = driver.wait.until(EC.element_to_be_clickable((By.ID, 'GuiEditor_Button_Load')))
    loadButton.click()
    radioButton = driver.find_element_by_id(radioId)
    radioButton.click()
    dialogClickOk(driver, "LoadSetup")
    time.sleep(1)

def saveSetup(driver):
    button = driver.wait.until(EC.element_to_be_clickable((By.ID, 'GuiEditor_Button_Save')))
    button.click()

def saveSetupAs(driver, setupName, exists = False, overwrite = False):
    button = driver.wait.until(EC.element_to_be_clickable((By.ID, 'GuiEditor_Button_SaveAs')))
    button.click()
    editBox = driver.find_element_by_id("GuiEditor_Dialog_SaveSetupAs_Field")
    ActionChains(driver).move_to_element(editBox).send_keys(Keys.HOME).perform()
    ActionChains(driver).key_down(Keys.SHIFT).send_keys(Keys.END).key_up(Keys.SHIFT).send_keys(Keys.DELETE).perform()
    ActionChains(driver).send_keys(setupName).perform()
    dialogClickOk(driver, "SaveSetupAs")
    try:
        driver.find_element_by_id("GuiEditor_Dialog_OverWrite")
        if overwrite:
            dialogClickOk(driver, "OverWrite")
        else:
            closeButton = driver.find_element_by_class_name("ui-dialog-titlebar").find_element_by_class_name("ui-dialog-titlebar-close")
            closeButton.click()
        return exists
    except NoSuchElementException as e:
        return not exists

# ---------- Handling setup manipulating buttons ----------

def addViewModel(driver, className):
    return addEditor(driver, "GuiEditor_ViewmodelEditor", className, "AddViewmodel")

def addView(driver, className):
    return addEditor(driver, "GuiEditor_ViewEditor", className, "AddView")

def addImport(driver, setupName):
    return addEditor(driver, "GuiEditor_ImportEditor", setupName, "AddImport")

def addEmptyRequest(driver):
    button = driver.wait.until(EC.element_to_be_clickable((By.ID,'GuiEditor_Button_AddRequest')))
    button.click()
    time.sleep(0.25)

# ---------- Editor manipulating functions ----------

def rightClickLabel(driver, editorId):
    element = driver.find_element_by_id(editorId + "_Header")
    actions = ActionChains(driver)
    actions.context_click(element)
    actions.perform()
    time.sleep(0.25)

def clickInContextMenu(driver, text):
    listElements = driver.find_elements_by_css_selector("#GuiEditor_ContextMenu > li")
    for element in listElements:
        if element.text == text:
            element.click()
            time.sleep(0.5)
            return True
    return False

def openCustomDataEditor(driver, editorId):
    button = driver.wait.until(EC.element_to_be_clickable((By.ID, editorId + "_Button_Edit")))
    button.click()
    time.sleep(0.5)

def closeCustomDataEditor(driver):
    button = driver.wait.until(EC.element_to_be_clickable((By.ID, "GuiEditor_CustomDataEditor_Close")))
    button.click()
    time.sleep(0.5)

# ---------- Connection creation ----------

def getNodeFromTreeByElements(driver, treeId, elements):
    children = driver.find_elements_by_css_selector("#" + treeId + " > ul > li")
    nodeToReturn = None
    for i in range(len(elements)):
        missing = True
        for node in children:
            nodeText = node.find_element_by_class_name("jstree-anchor").text
            if nodeText == elements[i]:
                nodeToReturn = node
                missing = False
                break
        if missing: return None
        if i < len(elements) - 1:
            if "jstree-closed" in nodeToReturn.get_attribute("class"):
                nodeToReturn.find_element_by_class_name("jstree-icon").click()
                time.sleep(0.25)
            children = nodeToReturn.find_elements_by_css_selector("ul > li")
    return nodeToReturn

def getNodeFromTreeByPath(driver, treeId, path):
    children = driver.find_elements_by_css_selector("#" + treeId + " > ul > li")
    nodeToReturn = None
    for i in range(len(path)):
        if not path[i] < len(children): return None
        nodeToReturn = children[path[i]]
        if i < len(path) - 1:
            if "jstree-closed" in nodeToReturn.get_attribute("class"):
                nodeToReturn.find_element_by_class_name("jstree-icon").click()
                time.sleep(0.25)
            children = nodeToReturn.find_elements_by_css_selector("ul > li")
    return nodeToReturn

def clickInTreeContextMenu(driver, node, menuText):
    anchor = node.find_element_by_class_name("jstree-anchor")
    actions = ActionChains(driver)
    actions.context_click(anchor)
    actions.perform()
    time.sleep(0.5)
    
    contextMenu = driver.find_element_by_class_name("jstree-default-contextmenu")
    items = contextMenu.find_elements_by_tag_name("li")
    for element in items:
        if menuText in element.text:
            element.click()
            time.sleep(0.5)
            return True
    return False

def pressDeleteOnNode(driver, node):
    node.click()
    node.send_keys(Keys.DELETE)
    time.sleep(0.25)

def dragAndDrop(driver, fromId, toId):
    source = driver.find_element_by_id(fromId)
    target = driver.find_element_by_id(toId)
    target.location_once_scrolled_into_view
    
    action_chains = ActionChains(driver)
    action_chains.drag_and_drop(source, target).perform()
    
    time.sleep(0.25)

# ---------- Utility functions ----------

def addEditor(driver, className, radioButtonToSelect, buttonSuffix):
    existingEditorIds = set()
    existingEditors = driver.find_elements_by_class_name(className)
    for editor in existingEditors: 
        editorId = editor.get_attribute("id")
        existingEditorIds.add(editorId)
    
    button = driver.wait.until(EC.element_to_be_clickable((By.ID, 'GuiEditor_Button_' + buttonSuffix)))
    button.click()
    selectRadioButtonInDialog(driver, buttonSuffix, radioButtonToSelect)
    dialogClickOk(driver, buttonSuffix)
    time.sleep(0.25)
    
    newEditorId = None
    existingEditors = driver.find_elements_by_class_name(className)
    for editor in existingEditors: 
        editorId = editor.get_attribute("id")
        if editorId not in existingEditorIds:
            newEditorId = editorId
            break
    return newEditorId

def selectRadioButtonInDialog(driver, dialogName, className):
    dialog = driver.find_element_by_id("GuiEditor_Dialog_" + dialogName)
    radio_button = dialog.find_element_by_id("GuiEditor_Dialog_" + dialogName + "_RadioButton_" + className)
    radio_button.click()

def dialogClickOk(driver, name):
    buttonId = "GuiEditor_Dialog_" + name + "_OkButton"
    button = driver.wait.until(EC.element_to_be_clickable((By.ID, buttonId)))
    button.click()

def getSetupName(driver):
    label = driver.find_element_by_id("GuiEditor_SetupNameLabel")
    return label.text

def treeExists(driver, id):
    try:
        div = driver.find_element_by_id(id)
        div.find_element_by_class_name("jstree-node")
        return True
    except NoSuchElementException as e:
        return False

def editorTypeExists(driver, className):
    try:
        driver.find_element_by_class_name(className)
        return True
    except NoSuchElementException as e:
        return False

def dialogExists(driver, name):
    dialogId = "GuiEditor_Dialog_" + name
    try:
        driver.find_element_by_id(dialogId)
        return True
    except NoSuchElementException as e:
        return False

def pressDelete(driver):
    actions = ActionChains(driver)
    actions.send_keys(Keys.DELETE).perform()
