#!/usr/bin/env python
# -*- coding: utf-8 -*-
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

def startGuiEditor(driver):
    browserCompatibilityCheck(driver)
    time.sleep(1)
    button = driver.wait.until(EC.element_to_be_clickable((By.ID, 'applications_header_appBtn_1')))
    button.click()
    time.sleep(1)

def startRequestTester(driver):
    browserCompatibilityCheck(driver)
    time.sleep(1)
    button = driver.wait.until(EC.element_to_be_clickable((By.ID, 'applications_header_appBtn_2')))
    button.click()
    time.sleep(1)

def startCustomizableApp(driver):
    browserCompatibilityCheck(driver)
    time.sleep(1)
    button = driver.wait.until(EC.element_to_be_clickable((By.ID, 'applications_header_appBtn_0')))
    button.click()
    time.sleep(1)
    
def browserCompatibilityCheck(driver):
    if (not driver.applicationStarted):
        driver.applicationStarted = True
        time.sleep(1)
        try:
            div = driver.find_element_by_id('UnsupportedBrowser').find_element_by_tag_name('button').click()
            time.sleep(1)
        except NoSuchElementException as e:
            pass