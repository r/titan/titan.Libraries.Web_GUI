#// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
#// All rights reserved. This program and the accompanying materials are made available under the     //
#// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
#/////////////////////////////////////////////////////////////////////////////////////////////////////// 
#!/usr/bin/env python

import sys, os, subprocess

gitPath = os.path.dirname(os.path.abspath(sys.argv[3]))
commandStr = 'cd ' + gitPath + '/test/; ./createOfflineWebGUI.sh'
os.system(commandStr);
commandStr = 'cd ' + gitPath + '/test/WebGUI/htdocs/Selenium; ./runTestsWithXvfb.sh'
result = subprocess.check_output(commandStr, stderr = subprocess.STDOUT, shell=True)
commandStr = 'cd ' + gitPath + '/test/WebGUI/; ./deleteOfflineWebGUI.sh'
os.system(commandStr);

lines = result.splitlines()
failstring = '<failure type="Error">Check console output below for more information.</failure>'
nroffailures = 1;
if 'OK' in lines[-1]:
  failstring = ''
  nroffailures = 0;

with open(sys.argv[3], "w") as text_file:
    text_file.write('''
<testsuite tests="1" failures="''' + str(nroffailures) + '''">
    <testcase classname="test1" name="test1">
    ''' + failstring + '''
    </testcase> 
    <system-out>''' + result + '''</system-out>
    <system-err>''' + lines[-1] + '''</system-err>
</testsuite>
''')

sys.exit(1) if nroffailures else sys.exit(0)